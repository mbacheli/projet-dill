# Projet-dill

[Lien vers la liste de processeurs](https://docs.google.com/spreadsheets/d/18Km-Dbxi9PpgQ6TlBJe9-OFY-vR65e8MMVpEbnPCt_k/edit?usp=sharing)

## Beaglebone

### Test en local

Le programme est fait pour etre compilé sur machine avec un processeur x86_64, ceci afin de tester sur la machine de développement.

```sh
make
# Ou la commande suivante si vous voulez l'affichage de debug complet
make debug
```

Afin de tester le programme il vous faudra simuler des ports série, cela peut etre fait via le programme `socat` et la commande suivante:

```sh
socat -d -d pty,raw,echo=0 pty,raw
```

Vous pouvez ensuite lancer le serveur PUIS le client (dans cet ordre). Entrez sur le client le premier PTY donné par la commande `socat` et dans le serveur le second PTY. Les deux programmes devraient se terminer sur l'affichage de la clé partagée (générée grâce aux échanges sur les ports séries).
