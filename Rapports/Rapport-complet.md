# Rapport complet du projet d'option 2SU, DiLL en partenariat avec le Cresitt

## Sommaire

1. Mise en contexte
2. Etat de l'art des mécanismes d'anti-rejeu de données
3. Description du protocole
4. Plan de validation

## I. Mise en contexte

## II. Etat de l'art des mécanismes d'anti-rejeu de données

### 1. Mécanisme de non rejeu existants

#### A. Timestamp

Le Timestamp est une partie du header d’un paquet qui permet d’ajouter un facteur temporel au paquet. Ainsi une première sécurité pour éviter le rejeu de données est de jeter les paquets dont le timestamp dépasse un certain délai.

#### B. Sequence Number et Sequence Window

Le sequence number est un compteur qui s'incrémente à chaque message envoyé, la sequence window représente les nombres entre le dernier sequence number le plus petit reçu et le plus grand. On peut aussi décider d’avoir une fenêtre fixe qui se déplace en fonction du dernier paquet reçu comme dans le dessin suivant :

![Exemple de Sequence Window](../Images/sequence-window.png)

#### C. Protection du mécanisme de non rejeu

Afin d’être réellement efficace, les deux mécanismes précédents doivent être protégés. Deux protections existent :

- Chiffrement du message
- Calcul d’un code d’authentification du message (HMAC ou CMAC)

Cela permet d’éviter que le champ contenant la donnée nécessaire au non rejeu soit altérée par l’attaquant. Le mécanisme HMAC est expliqué plus bas. De plus afin d’éviter la création d’une connexion légitime par un attaquant, il faut que les deux parties prenantes s’authentifient.

### 2. Explication des principes mis en jeu

#### A. Principe du HMAC

HMAC, pour Hash-based Message Authentication Code, permet de vérifier simultanément l’intégrité et l’authenticité d’un message. Il se base cryptographiquement sur une fonction de hashage couplé à une clé secrète. Le principe du HMAC se définit ainsi :

![Equation HMAC](../Images/equation-HMAC.png)

On a h la fonction de hashage, K la clé secrète et m le message. opad et ipad sont des blocs de padding “inner padding” et “outter padding” ayant pour but d’aligner les différentes clés à la taille d’un bloc. Ils sont de plus nécessaires à la démonstration mathématiques prouvant le mécanisme HMAC. Dans le cas de notre protocole le message m utilisé par le HMAC sera composé des données à envoyer ainsi que du numéro de séquence utilisé pour éviter le rejeu. Intégré le numéro de séquence dans le calcul du HMAC est primordial pour éviter que le numéro de séquence soit altéré par un attaquant. On trouve donc un paquet de la forme suivante :

![Format d'un paquet](../Images/paquet-format.jpg)

#### B. Principe d'authentification mutuelle

L’authentification mutuelle cherche à vérifier l’identité des deux parties prenantes. Elle peut se baser sur des mécanismes de chiffrements asymétriques. Prenons par exemple le cas suivant : Alice cherche à communiquer avec Bob. Alice va alors générer “un challenge” qu’elle enverra à Bob, ce dernier chiffrera ce challenge avec sa clé privée, puis le renverra à Alice. Celle-ci pourra alors déchiffrer le message reçu grâce à la clé publique de Bob et s’assurer que cela correspond bien au “challenge” précédemment envoyé. Pour rendre l’authentification bidirectionnelle, il suffit de procéder de la même manière, mais dans l’autre sens. Une manière simple de condenser ces étapes est de procéder à l’envoi du challenge de Bob vers Alice en même temps que l’envoi de la réponse de Bob au challenge d’Alice.

#### C. Chiffrement Authentifié

Le Chiffrement Authentifié, en anglais Authentication Encryption, est un mode d’opération qui assure la confidentialité et l’authentification des données. Le Chiffrement Authentifié est composé de deux algorithmes, l’un permet l’authentification des données, et l’autre effectue le chiffrement. On distingue 3 types de Chiffrement Authentifié :

- Encrypt-then-MAC (EtM), Chiffrement puis Authentification. Le chiffrement s’effectue dans un premier temps, puis le MAC est calculé à partir du message chiffré.
- Encrypt-and-MAC (EaM), Chiffrement et Authentification en même temps. Le chiffrement et le MAC sont calculés puis concaténés l’un à l’autre.
- MAC-then-Encrypt (MtE), Authentification puis Chiffrement. Le MAC est calculé à partir du message en claire, puis le tout est chiffré par l’algorithme de chiffrement.

Il existe une variant au Chiffrement Authentifié qui est le Chiffrement Authentifié avec Données Associées (AEAD). Cette méthode permet d’assurer l’intégrité des données qui ne font pas partie de la partie chiffrée (Header par exemple).

Exemples de AEAD :

- CCM (Counter with CBC-MAC) :  AEAD du type MtE. Il utilise un CBC-MAC comme algorithme de hashage puis un CTR comme algorithme de chiffrement. son principal avantage est qu’il permet de chiffrer le message en parallèle car il utilise un algorithme de chiffrement de type stream cipher. En revanche c’est l’AEAD le moins robuste par rapport aux deux suivants.
- EAX (Encrypt-then-Authenticate-then-Translate) AEAD du type EtM. Il utilise les mêmes types d’algorithmes que le CCM. La grande différence entre le CCM et l’EAX est l’ordre dans lequel les algorithmes sont utilisés, EAX chiffre d’abord son message puis ensuite applique l’algorithme de hashage pour calculer le MAC. Cela lui permet d’être plus robuste que le précédent et est s'exécute plus rapidement.
- GCM (Galois/Counter Mode) : AEAD du type EtM. Il utilise un CTR comme algorithme de chiffrement puis utilise une multiplication dans le corps de galois afin d’obtenir un MAC. Il est aussi robuste que EAX mais peut être plus performant en utilisant la méthode d’Horner pour paralléliser la fonction de hashage.

#### D. Diagramme de séquence

Le diagramme de séquence suivant est un exemple

![Exemple d'un diagramme de séquence](../Images/diagramme.png)

Ce diagramme présente trois parties. La première est la phase d’authentification mutuelle. Le premier send envoi le challenge du client chiffré par le serveur ainsi que le challenge du serveur vers le client. Le second send n’envoi que le challenge du serveur chiffré par le client. La seconde partie (Acquire Session Token), permet de partager une clé utilisée par la suite pour effectuer le HMAC. Dans la dernière partie (Example Transaction), la fonction calculateIntegrity effectue le calcule du HMAC avec la clé de session (session token) afin d’envoyer un message et son HMAC au serveur. Finalement, la fonction extractPacket s’occupe de séparer le HMAC du message. Tandis que la fonction calculateIntegrity chercher à vérifier qu’elle a bien reçu un message non altéré et provenant du client (et non d’un attaquant).

### 3. Différentes solutions proposés

#### A. Authentification mutuelle + Sequence Number + HMAC

Avantages :

- Protection face au rejeu de données
- Intégrité des messages
- Authentification des parties prenantes au début de la session
Inconvénients :
- Messages en claires
- Couple clé publique/privée pré-installé sur les différents appareils

Solution à préférer dans le cas où les échanges entre les appareils et le serveur sont fréquents et ne sont pas confidentiels.

#### B. Signature + Sequence Number

Avantage :

- Authentification à chaque message
- Protection face au rejeu de données
- Intégrité des messages
Inconvénient :
- Messages en claires
- Couple clé publique/privée pré-installé sur les différents appareils
- Facteur 1000 en temps de calcul pour la signature par rapport au HMAC

Solution à préférer dans le cas où les échanges sont réguliers, peu fréquents et non confidentiels.

#### C. Authentification mutuelle + Chiffrement + Sequence Number + Hash

Avantage :

- Messages chiffrés
- Authentification des parties prenants au début de la session
- Intégrité des messages
- Protection face au rejeu de données
Inconvénient :
- Plus gourmand en énergie
- Nécessite un co-processeur avec un accélérateur pour le chiffrement symétrique
- Couple clé publique/privée pré-installé sur les différents appareils

Solution à préférer dans le cas où les échanges doivent être confidentiels.

### III. Description du protocole

L’utilisation de ce protocole impose à l’utilisateur d’installer à l’avance la clé publique du client sur le serveur et la clé publique du serveur sur le client afin d'empêcher une attaque man in the middle durant l’échange de clé lors de l’authentification.

### 1. Schéma d'un datagramme

![Schéma d'ub datagramme](/Images/paquet.png)

**Le header :** Il sera simplement composé d’un octet de message type pour distinguer les différents types de messages soit 255 types différents. 2 octets afin d’avoir la taille des données qui suivent soit 64ko de données par paquet au maximum. Le sequence number sur 2 octets qui permet l’envoi d'un peu plus de 65000 messages. 16 octets pour le nonce qui permet d’initialiser le chiffrement chaîné. Le hash du header et du body sur 16 octets pour authentifier et vérifier l’intégrité du message.

**Le body :**  Contient les data.

### 2. Types de messages

#### A. Authentification

Lors de l’authentification le client va émettre un premier paquet avec le type of message à 0 (CLIENT_HELLO) ainsi qu’un nonce et une signature généré à partir du hash de l’intégralité du paquet et de sa clé privée. Le premier paquet contient l’identifiant de session. Ensuite une clé publique sera généré par le serveur afin de générer un secret partagé grâce à Diffie-Hellman. Cette clé publique sera envoyée au client (dans un paquet de type PKEY_DH = 1) qui à son tour enverra une clé publique qu’il aura généré de son côté. Ces deux clés publiques permettront d’établir une connexion sécurisée en créant un secret partagé. Durant ces échanges de messages les signatures seront calculées à partir des hash de chaque message et de la clé privée de l’émetteur afin que le récepteur puisse vérifier l’authenticité du message grâce à la clé publique qui est installée de son côté.

![Diagramme d'authentification](../Images/diagramme-authentification.png)

#### B. Données

Lors de la phase d’échange des données les paquets auront comme type DATA. Chaque paquet possédera un sequence number. Si ce sequence number n’est pas dans la sequence window alors il sera jeté. Pour ce protocole la sequence window est de taille 128. Afin de garantir l’intégrité, l’authenticité et la confidentialité des données le chiffrement utilisé sera AES en mode GCM qui garantit aussi l’intégrité du header.

#### C. Fin de communication

Lorsque la communication entre les deux appareils est terminée un paquet avec END_COMMUNICATION comme type de message sera envoyé pour que la communication soit fermée correctement.
signe

### 3. Machine à états du protocole

![Diagramme de la machine à états](../Images/client-state-machine.png)

## IV. Plan de validation

### 1. Définition des objectifs

Le but de ce plan de validation est de mettre en concurrence différents systèmes et implémentations de notre protocole de communication par ondes radios.
Ce protocole se base sur le modèle AEAD  et assure ainsi la confidentialité, l’intégrité des paquets et le non rejeu. Toutes ces fonctionnalités nécessitent plusieurs mécanismes : authentification, algorithme de chiffrement et de hashage. Tous ces dispositifs provoquent un surcoût pouvant provoquer des baisses de disponibilités et une surconsommation. Ce sont majoritairement ces deux propriétés que nous allons mesurer.
Cette mise en concurrence se fera donc sur la base de différentes métriques. Ce présent document cherchera donc à présenter ces différentes métriques, ainsi que les manières de les mesurer (configurations et scénarios).

### 2. Définition des métriques

Comme dit dans la partie d’introduction sur les objectifs, il est nécessaire de définir les différents points que nous allons mesurer. Nous nous efforcerons de préciser à chaque fois la méthode utilisée pour mesurer chaque paramètre.

#### A. Consommation Électrique

Bien que cela ne soit pas une contrainte forte du projet, il peut être intéressant et simple d’étudier la consommation électrique des différentes configurations que nous mettrons en place.
Pour cela, il nous suffira d’utiliser un simple wattmètre branché sur l’appareil (émetteur ou récepteur) que nous étudierons lors d’un scénario précis.

#### B. Temps moyen de chiffrement et déchiffrement d’un paquet

Une des contraintes fortes du projet réside dans le temps mis par le système pour effectuer les tâches de chiffrement et déchiffrement. Ces tâches demandent en effet du temps de calcul pouvant retarder le système et impacter d’autres paramètres comme le débit de transmission. Il faudra donc tester ces deux métriques :

- temps moyen de chiffrement et déchiffrement d’un paquet
- débit de transmission et réception du trafic

Pour mesurer ces caractéristiques nous avons décidé de mettre en place la phase de test représentée par le tableau suivant :

| Taille des données (octets) | 1   | 16  | 32  | 128 | 512  |
| --------------------------- | --- | --- | --- | --- | ---- |
| Avec accélaration           |     |     |     |     |      |
| Sans accélaration           |     |     |     |     |      |

#### C. Utilisation du CPU et de l’espace mémoire

Un dernier test serait d'observer l'évolution de l'utilisation de la mémoire notamment lors de l'échange de clé qui est assez gourmand car il demande de stocker les clés publiques et privées. L'utilisation du CPU nous permettrait de savoir si l'utilisation d'un accélérateur physique pour le chiffrement et le déchiffrement permet de libérer du temps de calcul pour que le CPU effectue d'autres tâches. 

### III. Définition des configurations de tests

Comme dit dans la partie objectifs, notre protocole se base sur la méthode AEAD. Il existe plusieurs configurations (mettant en jeu différents algorithmes), il est donc intéressant de tester ces différentes combinaisons. Dans notre cas, la définition d’une configuration de test se définit ainsi :

- support physique utilisé (micro proc / contrôleur)
- algorithme de chiffrement utilisé
- algorithme de hashage
- activation ou non des différentes accélérations matérielles
- mécanisme de non rejeu utilisé

Afin d’avoir un point de comparaison quelle que soit la configuration utilisée nous avons décidé d’utiliser une configuration “témoin” sur laquelle l’implémentation de notre protocole est considérée comme simple. Cela nous permettra aussi de tester les différents scénarios imaginés. Nous avons donc choisi d’utiliser un Beagle Bone Black, muni d’un crypto shield, utilisant AES 128, SHA256 (AES en mode EAX) et le mécanisme de sequence window.

### IV. Scénarios de tests fonctionnels

Afin de déterminer si le système supporte les cas d’utilisation que nous avons défini nous avons choisi plusieurs situations :

- Un test témoin va mettre en évidence le bon fonctionnement du protocole pour une connexion simple entre les deux émetteurs avec une transaction d’authentification puis un échange de données.
- Un test de montée en charge qui va nous permettre d’estimer les limites de notre protocole. Une fois le test de montée en charge effectué nous pourrons réaliser un test de charge pour mesurer le bon fonctionnement du système lorsqu’il est dans un état de fonctionnement intensif. Ces deux tests seront réalisés en simulant des connexions entre l'émetteur et le récepteur.
- Un test de performance sera réalisé pour évaluer les capacités du système à maintenir un service satisfaisant tout en augmentant sa charge au cours du temps. Ce test permettra aussi de déterminer la dégradation du système en fonction de la charge.

### V. Scénarios d'attaques

Soit A un émetteur, B un récepteur et P un attaquant qui écoute les transmissions entre A et B.

- Capture et rejeu instantané d’un paquet : A envoie un paquet chiffré et authentifié à B, P le réceptionne et le renvoie instantanément sur B.
- Capture et rejeu différé d’un paquet : A envoie un paquet chiffré et authentifié à B, P le réceptionne et le renvoie en différé sur B.
- Capture et rejeu d’un paquet à plusieurs reprises (spam) : A envoie un paquet chiffré et authentifié à B, P le réceptionne et le renvoie n fois sur B.
- Capture, modification puis rejeu d’un paquet : A envoie un paquet chiffré et authentifié à B, P modifie l’entête et le renvoie sur B.
- Man in the middle pendant l’authentification : A envoie un paquet d’authentification à B, P le réceptionne et envoie un paquet d’authentification forgé à B. P envoie aussi un paquet d'acquittement à A et s’authentifie auprès de A comme étant B. P termine de s’authentifier auprès de B.
