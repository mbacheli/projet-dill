# Plan de validation

## I. Définition des objectifs

Le but de ce plan de validation est de mettre en concurrence différents systèmes et implémentations de notre protocole de communication par ondes radios.
Ce protocole se base sur le modèle AEAD  et assure ainsi la confidentialité, l’intégrité des paquets et le non rejeu. Toutes ces fonctionnalités nécessitent plusieurs mécanismes : authentification, algorithme de chiffrement et de hashage. Tous ces dispositifs provoquent un surcoût pouvant provoquer des baisses de disponibilités et une surconsommation. Ce sont majoritairement ces deux propriétés que nous allons mesurer.
Cette mise en concurrence se fera donc sur la base de différentes métriques. Ce présent document cherchera donc à présenter ces différentes métriques, ainsi que les manières de les mesurer (configurations et scénarios).

## II. Définition des métriques

Comme dit dans la partie d’introduction sur les objectifs, il est nécessaire de définir les différents points que nous allons mesurer. Nous nous efforcerons de préciser à chaque fois la méthode utilisée pour mesurer chaque paramètre.

### A. Consommation Électrique

Bien que cela ne soit pas une contrainte forte du projet, il peut être intéressant et simple d’étudier la consommation électrique des différentes configurations que nous mettrons en place.
Pour cela, il nous suffira d’utiliser un simple wattmètre branché sur l’appareil (émetteur ou récepteur) que nous étudierons lors d’un scénario précis.

### B. Temps moyen de chiffrement et déchiffrement d’un paquet

Une des contraintes fortes du projet réside dans le temps mis par le système pour effectuer les tâches de chiffrement et déchiffrement. Ces tâches demandent en effet du temps de calcul pouvant retarder le système et impacter d’autres paramètres comme le débit de transmission. Il faudra donc tester ces deux métriques :

- temps moyen de chiffrement et déchiffrement d’un paquet
- débit de transmission et réception du trafic

Pour mesurer ces caractéristiques nous avons décidé de mettre en place la phase de test représentée par le tableau suivant :

| Taille des données (octets) | 1   | 16  | 32  | 128 | 512  |
| --------------------------- | --- | --- | --- | --- | ---- |
| Avec accélaration           |     |     |     |     |      |
| Sans accélaration           |     |     |     |     |      |

### C. Utilisation du CPU et de l’espace mémoire

Un dernier test serait d'observer l'évolution de l'utilisation de la mémoire notamment lors de l'échange de clé qui est assez gourmand car il demande de stocker les clés publiques et privées. L'utilisation du CPU nous permettrait de savoir si l'utilisation d'un accélérateur physique pour le chiffrement et le déchiffrement permet de libérer du temps de calcul pour que le CPU effectue d'autres tâches. 

## III. Définition des configurations de tests

Comme dit dans la partie objectifs, notre protocole se base sur la méthode AEAD. Il existe plusieurs configurations (mettant en jeu différents algorithmes), il est donc intéressant de tester ces différentes combinaisons. Dans notre cas, la définition d’une configuration de test se définit ainsi :

- support physique utilisé (micro proc / contrôleur)
- algorithme de chiffrement utilisé
- algorithme de hashage
- activation ou non des différentes accélérations matérielles
- mécanisme de non rejeu utilisé

Afin d’avoir un point de comparaison quelle que soit la configuration utilisée nous avons décidé d’utiliser une configuration “témoin” sur laquelle l’implémentation de notre protocole est considérée comme simple. Cela nous permettra aussi de tester les différents scénarios imaginés. Nous avons donc choisi d’utiliser un Beagle Bone Black, muni d’un crypto shield, utilisant AES 128, SHA256 (AES en mode EAX) et le mécanisme de sequence window.

## IV. Scénarios de tests fonctionnels

Afin de déterminer si le système supporte les cas d’utilisation que nous avons défini nous avons choisi plusieurs situations :

- Un test témoin va mettre en évidence le bon fonctionnement du protocole pour une connexion simple entre les deux émetteurs avec une transaction d’authentification puis un échange de données.
- Un test de montée en charge qui va nous permettre d’estimer les limites de notre protocole. Une fois le test de montée en charge effectué nous pourrons réaliser un test de charge pour mesurer le bon fonctionnement du système lorsqu’il est dans un état de fonctionnement intensif. Ces deux tests seront réalisés en simulant des connexions entre l'émetteur et le récepteur.
- Un test de performance sera réalisé pour évaluer les capacités du système à maintenir un service satisfaisant tout en augmentant sa charge au cours du temps. Ce test permettra aussi de déterminer la dégradation du système en fonction de la charge.

## V. Scénarios d'attaques

Soit A un émetteur, B un récepteur et P un attaquant qui écoute les transmissions entre A et B.

- Capture et rejeu instantané d’un paquet : A envoie un paquet chiffré et authentifié à B, P le réceptionne et le renvoie instantanément sur B.
- Capture et rejeu différé d’un paquet : A envoie un paquet chiffré et authentifié à B, P le réceptionne et le renvoie en différé sur B.
- Capture et rejeu d’un paquet à plusieurs reprises (spam) : A envoie un paquet chiffré et authentifié à B, P le réceptionne et le renvoie n fois sur B.
- Capture, modification puis rejeu d’un paquet : A envoie un paquet chiffré et authentifié à B, P modifie l’entête et le renvoie sur B.
- Man in the middle pendant l’authentification : A envoie un paquet d’authentification à B, P le réceptionne et envoie un paquet d’authentification forgé à B. P envoie aussi un paquet d'acquittement à A et s’authentifie auprès de A comme étant B. P termine de s’authentifier auprès de B.
