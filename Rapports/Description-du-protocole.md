# Description du protocole

L’utilisation de ce protocole impose à l’utilisateur d’installer à l’avance la clé publique du client sur le serveur et la clé publique du serveur sur le client afin d'empêcher une attaque man in the middle durant l’échange de clé lors de l’authentification. 

## 1. Schéma d'un datagramme

![Schéma d'ub datagramme](/Images/paquet.png)

**Le header :** Il sera simplement composé d’un octet de message type pour distinguer les différents types de messages soit 255 types différents. 2 octets afin d’avoir la taille des données qui suivent soit 64ko de données par paquet au maximum. Le sequence number sur 2 octets qui permet l’envoi d'un peu plus de 65000 messages. 16 octets pour le nonce qui permet d’initialiser le chiffrement chaîné. Le hash du header et du body sur 16 octets pour authentifier et vérifier l’intégrité du message.

**Le body :**  Contient les data.

## 2. Types de messages

### A. Authentification

Lors de l’authentification le client va émettre un premier paquet avec le type of message à 0 (CLIENT_HELLO) ainsi qu’un nonce et une signature généré à partir du hash de l’intégralité du paquet et de sa clé privée. Le premier paquet contient l’identifiant de session. Ensuite une clé publique sera généré par le serveur afin de générer un secret partagé grâce à Diffie-Hellman. Cette clé publique sera envoyée au client (dans un paquet de type PKEY_DH = 1) qui à son tour enverra une clé publique qu’il aura généré de son côté. Ces deux clés publiques permettront d’établir une connexion sécurisée en créant un secret partagé. Durant ces échanges de messages les signatures seront calculées à partir des hash de chaque message et de la clé privée de l’émetteur afin que le récepteur puisse vérifier l’authenticité du message grâce à la clé publique qui est installée de son côté.

![Diagramme d'authentification](../Images/diagramme-authentification.png)

### B. Données

Lors de la phase d’échange des données les paquets auront comme type DATA. Chaque paquet possédera un sequence number. Si ce sequence number n’est pas dans la sequence window alors il sera jeté. Pour ce protocole la sequence window est de taille 128. Afin de garantir l’intégrité, l’authenticité et la confidentialité des données le chiffrement utilisé sera AES en mode GCM qui garantit aussi l’intégrité du header.

### C. Fin de communication

Lorsque la communication entre les deux appareils est terminée un paquet avec END_COMMUNICATION comme type de message sera envoyé pour que la communication soit fermée correctement.
signe

## 3. Machine à états du protocole

![Diagramme de la machine à états](../Images/client-state-machine.png)