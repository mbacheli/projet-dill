# Rapport sur la phase de recherche du projet

## 1. Mécanisme de non rejeu existants

### A. Timestamp

Le Timestamp est une partie du header d’un paquet qui permet d’ajouter un facteur temporel au paquet. Ainsi une première sécurité pour éviter le rejeu de données est de jeter les paquets dont le timestamp dépasse un certain délai.

### B. Sequence Number et Sequence Window

Le sequence number est un compteur qui s'incrémente à chaque message envoyé, la sequence window représente les nombres entre le dernier sequence number le plus petit reçu et le plus grand. On peut aussi décider d’avoir une fenêtre fixe qui se déplace en fonction du dernier paquet reçu comme dans le dessin suivant :

![Exemple de Sequence Window](../Images/sequence-window.png)

### C. Protection du mécanisme de non rejeu

Afin d’être réellement efficace, les deux mécanismes précédents doivent être protégés. Deux protections existent :

- Chiffrement du message
- Calcul d’un code d’authentification du message (HMAC ou CMAC)

Cela permet d’éviter que le champ contenant la donnée nécessaire au non rejeu soit altérée par l’attaquant. Le mécanisme HMAC est expliqué plus bas. De plus afin d’éviter la création d’une connexion légitime par un attaquant, il faut que les deux parties prenantes s’authentifient.

## II. Explication des principes mis en jeu

### A. Principe du HMAC

HMAC, pour Hash-based Message Authentication Code, permet de vérifier simultanément l’intégrité et l’authenticité d’un message. Il se base cryptographiquement sur une fonction de hashage couplé à une clé secrète. Le principe du HMAC se définit ainsi :

![Equation HMAC](../Images/equation-HMAC.png)

On a h la fonction de hashage, K la clé secrète et m le message. opad et ipad sont des blocs de padding “inner padding” et “outter padding” ayant pour but d’aligner les différentes clés à la taille d’un bloc. Ils sont de plus nécessaires à la démonstration mathématiques prouvant le mécanisme HMAC. Dans le cas de notre protocole le message m utilisé par le HMAC sera composé des données à envoyer ainsi que du numéro de séquence utilisé pour éviter le rejeu. Intégré le numéro de séquence dans le calcul du HMAC est primordial pour éviter que le numéro de séquence soit altéré par un attaquant. On trouve donc un paquet de la forme suivante :

![Format d'un paquet](../Images/paquet-format.jpg)

### B. Principe d'authentification mutuelle

L’authentification mutuelle cherche à vérifier l’identité des deux parties prenantes. Elle peut se baser sur des mécanismes de chiffrements asymétriques. Prenons par exemple le cas suivant : Alice cherche à communiquer avec Bob. Alice va alors générer “un challenge” qu’elle enverra à Bob, ce dernier chiffrera ce challenge avec sa clé privée, puis le renverra à Alice. Celle-ci pourra alors déchiffrer le message reçu grâce à la clé publique de Bob et s’assurer que cela correspond bien au “challenge” précédemment envoyé. Pour rendre l’authentification bidirectionnelle, il suffit de procéder de la même manière, mais dans l’autre sens. Une manière simple de condenser ces étapes est de procéder à l’envoi du challenge de Bob vers Alice en même temps que l’envoi de la réponse de Bob au challenge d’Alice.

### C. Chiffrement Authentifié

Le Chiffrement Authentifié, en anglais Authentication Encryption, est un mode d’opération qui assure la confidentialité et l’authentification des données. Le Chiffrement Authentifié est composé de deux algorithmes, l’un permet l’authentification des données, et l’autre effectue le chiffrement. On distingue 3 types de Chiffrement Authentifié :

- Encrypt-then-MAC (EtM), Chiffrement puis Authentification. Le chiffrement s’effectue dans un premier temps, puis le MAC est calculé à partir du message chiffré.
- Encrypt-and-MAC (EaM), Chiffrement et Authentification en même temps. Le chiffrement et le MAC sont calculés puis concaténés l’un à l’autre.
- MAC-then-Encrypt (MtE), Authentification puis Chiffrement. Le MAC est calculé à partir du message en claire, puis le tout est chiffré par l’algorithme de chiffrement.

Il existe une variant au Chiffrement Authentifié qui est le Chiffrement Authentifié avec Données Associées (AEAD). Cette méthode permet d’assurer l’intégrité des données qui ne font pas partie de la partie chiffrée (Header par exemple).

Exemples de AEAD :

- CCM (Counter with CBC-MAC) :  AEAD du type MtE. Il utilise un CBC-MAC comme algorithme de hashage puis un CTR comme algorithme de chiffrement. son principal avantage est qu’il permet de chiffrer le message en parallèle car il utilise un algorithme de chiffrement de type stream cipher. En revanche c’est l’AEAD le moins robuste par rapport aux deux suivants.
- EAX (Encrypt-then-Authenticate-then-Translate) AEAD du type EtM. Il utilise les mêmes types d’algorithmes que le CCM. La grande différence entre le CCM et l’EAX est l’ordre dans lequel les algorithmes sont utilisés, EAX chiffre d’abord son message puis ensuite applique l’algorithme de hashage pour calculer le MAC. Cela lui permet d’être plus robuste que le précédent et est s'exécute plus rapidement.
- GCM (Galois/Counter Mode) : AEAD du type EtM. Il utilise un CTR comme algorithme de chiffrement puis utilise une multiplication dans le corps de galois afin d’obtenir un MAC. Il est aussi robuste que EAX mais peut être plus performant en utilisant la méthode d’Horner pour paralléliser la fonction de hashage.

### D. Diagramme de séquence

Le diagramme de séquence suivant est un exemple

![Exemple d'un diagramme de séquence](../Images/diagramme.png)

Ce diagramme présente trois parties. La première est la phase d’authentification mutuelle. Le premier send envoi le challenge du client chiffré par le serveur ainsi que le challenge du serveur vers le client. Le second send n’envoi que le challenge du serveur chiffré par le client. La seconde partie (Acquire Session Token), permet de partager une clé utilisée par la suite pour effectuer le HMAC. Dans la dernière partie (Example Transaction), la fonction calculateIntegrity effectue le calcule du HMAC avec la clé de session (session token) afin d’envoyer un message et son HMAC au serveur. Finalement, la fonction extractPacket s’occupe de séparer le HMAC du message. Tandis que la fonction calculateIntegrity chercher à vérifier qu’elle a bien reçu un message non altéré et provenant du client (et non d’un attaquant).

## III. Différentes solutions proposés

### A. Authentification mutuelle + Sequence Number + HMAC

Avantages :

- Protection face au rejeu de données
- Intégrité des messages
- Authentification des parties prenantes au début de la session
Inconvénients :
- Messages en claires
- Couple clé publique/privée pré-installé sur les différents appareils

Solution à préférer dans le cas où les échanges entre les appareils et le serveur sont fréquents et ne sont pas confidentiels.

### B. Signature + Sequence Number

Avantage :

- Authentification à chaque message
- Protection face au rejeu de données
- Intégrité des messages
Inconvénient :
- Messages en claires
- Couple clé publique/privée pré-installé sur les différents appareils
- Facteur 1000 en temps de calcul pour la signature par rapport au HMAC

Solution à préférer dans le cas où les échanges sont réguliers, peu fréquents et non confidentiels.

### C. Authentification mutuelle + Chiffrement + Sequence Number + Hash

Avantage :

- Messages chiffrés
- Authentification des parties prenants au début de la session
- Intégrité des messages
- Protection face au rejeu de données
Inconvénient :
- Plus gourmand en énergie
- Nécessite un co-processeur avec un accélérateur pour le chiffrement symétrique
- Couple clé publique/privée pré-installé sur les différents appareils

Solution à préférer dans le cas où les échanges doivent être confidentiels.
