#ifndef __UTILS_H__
#define __UTILS_H__

#include <stdio.h>

/*
 * NOTE: Pour l'instant on garde en macro pour éviter la surcharge du code
 * Mais on pourra surement passer ça en fonction pour avoir plus de fonctionnalités
 * de logs
 */
#ifdef DEBUG
    #define PRINT_DEBUG(message) printf("%s: %s\n", __FUNCTION__, message)
#else
    // le do while sert à l'utilisation de la macro dans 
    #define PRINT_DEBUG(message) (void) 0
#endif

#ifndef ERASE_MEMORY_FOR_REUSE
    /**
     * @brief permet de faire des memset('\0') pour vider les zones mémoires
     *        dynamique qu'on libère. Les tableaux ayant une taille déterminée
     *        sont forcément remis à zéro.
     */
    #define ERASE_MEMORY_FOR_REUSE 1
#endif

#ifdef DEBUG
    #define PRINT_DEBUG_ENTER_FCT() printf("Entering %s function\n", __FUNCTION__)
#else
    // le do while sert à l'utilisation de la macro dans 
    #define PRINT_DEBUG_ENTER_FCT() (void) 0
#endif

#ifdef DEBUG
    #define PRINT_DEBUG_EXIT_FCT() printf("Exiting %s function\n", __FUNCTION__)
#else
    // le do while sert à l'utilisation de la macro dans 
    #define PRINT_DEBUG_EXIT_FCT() (void) 0
#endif

#define RAISE_ERROR(errnum, message) \
    do {                             \
        PRINT_DEBUG(message);        \
        return errnum;               \
    } while(0)

void print_hex(const unsigned char *s, size_t size);
uint16_t get_short(unsigned char* array, int offset);

#endif // __UTILS_H__
