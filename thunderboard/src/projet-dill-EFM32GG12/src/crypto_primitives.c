#include <mbedtls/entropy.h>
#include <mbedtls/gcm.h>
#include <mbedtls/sha256.h>
#include <mbedtls/ctr_drbg.h>
#include <mbedtls/ecdh.h>
#include <mbedtls/rsa.h>
#include <string.h>

#include <stdbool.h>

#include "utils.h"
#include "crypto_primitives.h"

/**
 * @brief Fichier rassemblant les primitives de chiffrement de notre application
 * 
 * Pour activer le debug dans ce fichier, ajouter la variable DEBUG à la compilation.
 */

/**
 * @brief Fonction permettant de déterminer la taille d'un chiffré à partir du plaintext
 */
size_t determine_cipher_size(size_t plaintext_size) {
    // int block_size = EVP_CIPHER_block_size(EVP_aes_256_gcm());
    // return ((plaintext_size + block_size) / block_size) * block_size;
    // dans le cas de GCM c'est la meme taille: https://crypto.stackexchange.com/a/26787
    return plaintext_size;
}

CRYPTO_RETURN_TYPE generate_random_bytes(unsigned char *bytes, size_t length) {
    // 0 = false, 1= true
    static int is_random_seeded = 0;
    
    if (!bytes)
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Vous devez allouer l'espace nécessaire");

    if (!is_random_seeded) {
        PRINT_DEBUG("RAND_bytes n'était pas initialisé... initialisation");
        is_random_seeded = RAND_poll();
    }

    if (0 >= RAND_bytes(bytes, length))
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible de générer les octets aléatoire");

    return CRYPTO_RETURN_OK;
}

/**
 * @brief génère le session ID avec le random de OPENSSL
 * 
 * @param session_id tableau de 32 octets minimum
 * @return int -1 si erreur, 0 sinon
 */
inline CRYPTO_RETURN_TYPE generate_session_id(unsigned char *session_id)
{
    PRINT_DEBUG("Génération aléatoire de l'ID de session");
    return generate_random_bytes(session_id, SESSION_ID_DIGITS);
}

inline CRYPTO_RETURN_TYPE generate_aes_iv(unsigned char *iv) {
    PRINT_DEBUG("Génération aléatoire du vecteur d'initialisation");
    return generate_random_bytes(iv, IV_SIZE);
}

void handle_errors(void) {
    printf("error\n");
}

CRYPTO_RETURN_TYPE encrypt(const unsigned char *plaintext, int plaintext_len,
                           const unsigned char *aad, int aad_len,
                           const unsigned char *iv, int iv_len,
                           const unsigned char *key,
                           unsigned char *ciphertext, int *ciphertext_len,
                           unsigned char *tag)
{
	mbedtls_gcm_context gcm_ctx;
	mbedtls_gcm_init(&gcm_ctx);
	mbedtls_gcm_setkey(&gcm_ctx, MBEDTLS_CIPHER_ID_AES , key, AES_KEY_SIZE);
	mbedtls_gcm_starts(&gcm_ctx, MBEDTLS_GCM_ENCRYPT, iv, iv_len, aad, aad_len);
	mbedtls_gcm_update(&gcm_ctx, plaintext_len, plaintext, ciphertext);
	mbedtls_gcm_finish(&gcm_ctx, tag, TAG_SIZE);

    return CRYPTO_RETURN_OK;
}

CRYPTO_RETURN_TYPE decrypt(const unsigned char *ciphertext, int ciphertext_len, 
                           const unsigned char *aad, int aad_len, 
                           const unsigned char *tag,
                           const unsigned char *key,
                           const unsigned char *iv, int iv_len,
                           unsigned char *plaintext, int *plaintext_len)
{
	mbedtls_gcm_context gcm_ctx;
	mbedtls_gcm_init(&gcm_ctx);
	mbedtls_gcm_setkey(&gcm_ctx, MBEDTLS_CIPHER_ID_AES , key, AES_KEY_SIZE);
	mbedtls_gcm_starts(&gcm_ctx, MBEDTLS_GCM_DECRYPT, iv, iv_len, aad, aad_len);
	mbedtls_gcm_update(&gcm_ctx, ciphertext_len, ciphertext, plaintext);
	mbedtls_gcm_finish(&gcm_ctx, tag, TAG_SIZE);

    return CRYPTO_RETURN_OK;
}

CRYPTO_RETURN_TYPE hash(const unsigned char *text, int text_len, unsigned char *hash, unsigned int *hash_len)
{

	mbedtls_sha256(text, text_len, hash, 0);

    return CRYPTO_RETURN_OK;
}

#define FREE_AND_QUIT_CREATE()                           							\
    do {                                               								\
    	if( fpub  != NULL )															\
		 fclose( fpub );															\
		if( fpriv != NULL )															\
		 fclose( fpriv );															\
		mbedtls_mpi_free( &N ); mbedtls_mpi_free( &P ); mbedtls_mpi_free( &Q );		\
		mbedtls_mpi_free( &D ); mbedtls_mpi_free( &E ); mbedtls_mpi_free( &DP );	\
		mbedtls_mpi_free( &DQ ); mbedtls_mpi_free( &QP );							\
		mbedtls_rsa_free( &rsa );													\
		mbedtls_ctr_drbg_free( &ctr_drbg );											\
    	mbedtls_entropy_free( &entropy );											\
        return CRYPTO_RETURN_ERROR;                    								\
    } while (0)

CRYPTO_RETURN_TYPE create_RSA() {
	 mbedtls_rsa_context rsa;
	 mbedtls_entropy_context entropy;
	 mbedtls_ctr_drbg_context ctr_drbg;
	 mbedtls_mpi N, P, Q, D, E, DP, DQ, QP;
	 FILE *fpub  = NULL;
	 FILE *fpriv = NULL;
	 const char *pers = "rsa_genkey";

	 mbedtls_ctr_drbg_init( &ctr_drbg );
	 mbedtls_rsa_init( &rsa, MBEDTLS_RSA_PKCS_V15, 0 );
	 mbedtls_mpi_init( &N ); mbedtls_mpi_init( &P ); mbedtls_mpi_init( &Q );
	 mbedtls_mpi_init( &D ); mbedtls_mpi_init( &E ); mbedtls_mpi_init( &DP );
	 mbedtls_mpi_init( &DQ ); mbedtls_mpi_init( &QP );

	 printf( "\n  . Seeding the random number generator..." );
	 fflush( stdout );

	 mbedtls_entropy_init( &entropy );
	 if( mbedtls_ctr_drbg_seed( &ctr_drbg, mbedtls_entropy_func, &entropy,
								(const unsigned char *) pers,
								strlen( pers ) )  != 0 )
	 {
		 printf( " failed\n  ! mbedtls_ctr_drbg_seed returned an error\n");
		 FREE_AND_QUIT_CREATE();
	 }

	 printf( " ok\n  . Generating the RSA key [ %d-bit ]...", ASYM_KEY_BITS_LEN );
	 fflush( stdout );

	 if( mbedtls_rsa_gen_key( &rsa, mbedtls_ctr_drbg_random, &ctr_drbg, ASYM_KEY_BITS_LEN,
									  EXPONENT )  != 0 )
	 {
		 printf( " failed\n  ! mbedtls_rsa_gen_key returned an error\n" );
		 FREE_AND_QUIT_CREATE();
	 }

	 printf( " ok\n  . Exporting the public  key in rsa_pub.txt...." );
	 fflush( stdout );

	 if( mbedtls_rsa_export    ( &rsa, &N, &P, &Q, &D, &E ) != 0 ||
	         mbedtls_rsa_export_crt( &rsa, &DP, &DQ, &QP ) != 0 )
	     {
	         printf( " failed\n  ! could not export RSA parameters\n\n" );
	         FREE_AND_QUIT_CREATE();
	     }

	     if( ( fpub = fopen( "rsa_pub.txt", "wb+" ) ) == NULL )
	     {
	         printf( " failed\n  ! could not open rsa_pub.txt for writing\n\n" );
	         FREE_AND_QUIT_CREATE();
	     }

	     if( (mbedtls_mpi_write_file( "N = ", &N, 16, fpub ) ) != 0 ||
	         (mbedtls_mpi_write_file( "E = ", &E, 16, fpub ) ) != 0 )
	     {
	         printf( " failed\n  ! mbedtls_mpi_write_file returned an error\n\n");
	         FREE_AND_QUIT_CREATE();
	     }

	     printf( " ok\n  . Exporting the private key in rsa_priv.txt..." );
	     fflush( stdout );

	     if( ( fpriv = fopen( "rsa_priv.txt", "wb+" ) ) == NULL )
	     {
	         printf( " failed\n  ! could not open rsa_priv.txt for writing\n" );
	         FREE_AND_QUIT_CREATE();
	     }

	     if( ( mbedtls_mpi_write_file( "N = " , &N , 16, fpriv ) ) != 0 ||
	         ( mbedtls_mpi_write_file( "E = " , &E , 16, fpriv ) ) != 0 ||
	         ( mbedtls_mpi_write_file( "D = " , &D , 16, fpriv ) ) != 0 ||
	         ( mbedtls_mpi_write_file( "P = " , &P , 16, fpriv ) ) != 0 ||
	         ( mbedtls_mpi_write_file( "Q = " , &Q , 16, fpriv ) ) != 0 ||
	         ( mbedtls_mpi_write_file( "DP = ", &DP, 16, fpriv ) ) != 0 ||
	         ( mbedtls_mpi_write_file( "DQ = ", &DQ, 16, fpriv ) ) != 0 ||
	         ( mbedtls_mpi_write_file( "QP = ", &QP, 16, fpriv ) ) != 0 )
	     {
	         printf( " failed\n  ! mbedtls_mpi_write_file returned an error\n\n");
	         FREE_AND_QUIT_CREATE();
	     }

	     printf( " ok\n\n" );

	     if( fpub  != NULL )
			 fclose( fpub );

		 if( fpriv != NULL )
			 fclose( fpriv );

		 mbedtls_mpi_free( &N ); mbedtls_mpi_free( &P ); mbedtls_mpi_free( &Q );
		 mbedtls_mpi_free( &D ); mbedtls_mpi_free( &E ); mbedtls_mpi_free( &DP );
		 mbedtls_mpi_free( &DQ ); mbedtls_mpi_free( &QP );
		 mbedtls_rsa_free( &rsa );
		 mbedtls_ctr_drbg_free( &ctr_drbg );
		 mbedtls_entropy_free( &entropy );

		 return CRYPTO_RETURN_OK;

}

#define FREE_AND_QUIT_SIGN()                           									\
    do {                                               									\
    	mbedtls_mpi_free( &N ); mbedtls_mpi_free( &P ); mbedtls_mpi_free( &Q );			\
    	mbedtls_mpi_free( &D ); mbedtls_mpi_free( &E ); mbedtls_mpi_free( &DP );		\
    	mbedtls_mpi_free( &DQ ); mbedtls_mpi_free( &QP );								\
    	mbedtls_rsa_free(rsa);															\
        return CRYPTO_RETURN_ERROR;                    									\
    } while (0)

CRYPTO_RETURN_TYPE sign(const unsigned char* message, size_t message_len, unsigned char * signature)
{
	FILE *f;
	unsigned char hash[32];
	mbedtls_rsa_context* rsa;
	mbedtls_mpi N, P, Q, D, E, DP, DQ, QP;
	mbedtls_mpi_init( &N ); mbedtls_mpi_init( &P ); mbedtls_mpi_init( &Q );
	mbedtls_mpi_init( &D ); mbedtls_mpi_init( &E ); mbedtls_mpi_init( &DP );
	mbedtls_mpi_init( &DQ ); mbedtls_mpi_init( &QP );

	mbedtls_rsa_init(rsa, MBEDTLS_RSA_PKCS_V15, 0);

    printf( "\n  . Reading private key from rsa_priv.txt" );
    fflush( stdout );

    if( ( f = fopen( "rsa_priv.txt", "rb" ) ) == NULL )
	{
    	printf( " failed\n  ! Could not open rsa_priv.txt\n" \
    	                "  ! Please run rsa_genkey first\n\n" );
    	FREE_AND_QUIT_SIGN();
	}

    if( ( mbedtls_mpi_read_file( &N , 16, f ) ) != 0 ||
		( mbedtls_mpi_read_file( &E , 16, f ) ) != 0 ||
		( mbedtls_mpi_read_file( &D , 16, f ) ) != 0 ||
		( mbedtls_mpi_read_file( &P , 16, f ) ) != 0 ||
		( mbedtls_mpi_read_file( &Q , 16, f ) ) != 0 ||
		( mbedtls_mpi_read_file( &DP , 16, f ) ) != 0 ||
		( mbedtls_mpi_read_file( &DQ , 16, f ) ) != 0 ||
		( mbedtls_mpi_read_file( &QP , 16, f ) ) != 0 )
	{
    	printf( " failed\n  ! mbedtls_mpi_read_file returned an error\n\n");
		fclose( f );
		FREE_AND_QUIT_SIGN();
	}
    fclose(f);

    if(  mbedtls_rsa_import( rsa, &N, &P, &Q, &D, &E )  != 0 )
        {
    	printf( " failed\n  ! mbedtls_rsa_import returned an error\n" );
		FREE_AND_QUIT_SIGN();
	}

	if(  mbedtls_rsa_complete( rsa ) != 0 )
	{
		printf( " failed\n  ! mbedtls_rsa_complete returned an error\n" );
		FREE_AND_QUIT_SIGN();
	}

	printf( "\n  . Checking the private key" );
	fflush( stdout );
	if( mbedtls_rsa_check_privkey( rsa ) != 0 )
	{
		printf( " failed\n  ! mbedtls_rsa_check_privkey failed with an error\n");
		FREE_AND_QUIT_SIGN();
	}

	printf( "\n  . Generating the RSA/SHA-256 signature" );
	fflush( stdout );

	if(  mbedtls_md(
					mbedtls_md_info_from_type( MBEDTLS_MD_SHA256 ),
					message, message_len, hash )  != 0 )
	{
		printf( " failed\n  ! mbedtls_md return an error\n\n" );
		FREE_AND_QUIT_SIGN();
	}

    if( mbedtls_rsa_pkcs1_sign( rsa, NULL, NULL, MBEDTLS_RSA_PRIVATE, MBEDTLS_MD_SHA256,
                                20, hash, signature ) != 0 )
    {
    	printf( " failed\n  ! mbedtls_rsa_pkcs1_sign returned an error\n\n");
        FREE_AND_QUIT_SIGN();
    }

    printf
	( "\n  . Done\n\n");

    mbedtls_mpi_free( &N ); mbedtls_mpi_free( &P ); mbedtls_mpi_free( &Q );
    mbedtls_mpi_free( &D ); mbedtls_mpi_free( &E ); mbedtls_mpi_free( &DP );
	mbedtls_mpi_free( &DQ ); mbedtls_mpi_free( &QP );
	mbedtls_rsa_free(rsa);

	return CRYPTO_RETURN_OK;
}

CRYPTO_RETURN_TYPE verify(const unsigned char* message, size_t message_len, const unsigned char *signature, size_t signed_len)
{
	FILE *f;
	unsigned char hash[32];
	mbedtls_rsa_context *rsa;

	mbedtls_rsa_init(rsa, MBEDTLS_RSA_PKCS_V15, 0);

	printf( "\n  . Reading public key from rsa_pub.txt" );
	fflush( stdout );

	if( ( f = fopen( "rsa_pub.txt", "rb" ) ) == NULL )
	{
		printf( " failed\n  ! Could not open rsa_pub.txt\n" \
				"  ! Please run rsa_genkey first\n\n" );
		mbedtls_rsa_free(rsa);
		return CRYPTO_RETURN_ERROR;
	}

	if( mbedtls_mpi_read_file( &(rsa->N), 16, f ) != 0 ||
	        mbedtls_mpi_read_file( &(rsa->E), 16, f ) != 0 )
	{
		printf( " failed\n  ! mbedtls_mpi_read_file returned an error\n\n");
	    fclose( f );
	    mbedtls_rsa_free(rsa);
	    return CRYPTO_RETURN_ERROR;
	}

	rsa->len = ( mbedtls_mpi_bitlen( &(rsa->N) ) + 7 ) >> 3;

	fclose( f );

	if(rsa->len != signed_len){
		printf( "\n  ! Invalid RSA signature format\n\n" );
		mbedtls_rsa_free(rsa);
		return CRYPTO_RETURN_ERROR;
	}

	printf( "\n  . Verifying the RSA/SHA-256 signature" );
	fflush( stdout );

	if( ( mbedtls_md(
	                    mbedtls_md_info_from_type( MBEDTLS_MD_SHA256 ),
	                    message, message_len, hash ) ) != 0 )
	    {
	        printf( " failed\n  ! Could not hash message\n\n");
	        mbedtls_rsa_free(rsa);
	        return CRYPTO_RETURN_ERROR;
	    }

	if(mbedtls_rsa_pkcs1_verify(rsa, NULL, NULL, MBEDTLS_RSA_PUBLIC,
            MBEDTLS_MD_SHA256, 20, hash, signature) != 0){
		printf( " failed\n  ! mbedtls_rsa_pkcs1_verify returned an error\n\n");
		mbedtls_rsa_free(rsa);
		return CRYPTO_RETURN_ERROR;
	}

	printf( "\n  . OK (the signature is valid)\n\n" );

	mbedtls_rsa_free(rsa);

	return CRYPTO_RETURN_OK;
}

CRYPTO_RETURN_TYPE generate_dh_parameters(mbedtls_ctr_drbg_context  *ctr_drbg, mbedtls_ecdh_context *ecdh_ctx )
{
	int ret = 1;
    mbedtls_entropy_context entropy;

    PRINT_DEBUG("Initialize ecdh and drbg context");
    mbedtls_ecdh_init(ecdh_ctx);
    mbedtls_ctr_drbg_init(ctr_drbg);

    PRINT_DEBUG("Create the random generator for generating the parameters");
    mbedtls_entropy_init(&entropy);
    if(!mbedtls_ctr_drbg_seed(ctr_drbg, mbedtls_entropy_func, &entropy, NULL, sizeof(NULL))){
    	RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible de cr�er la seed pour DH");
        mbedtls_entropy_free(&entropy);
    }

    PRINT_DEBUG("Initializing the group");
    if(!mbedtls_ecp_group_load(&ecdh_ctx->grp, MBEDTLS_ECP_DP_CURVE25519 )) {
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible d'initialiser le groupe");
        mbedtls_entropy_free(&entropy);
    }

    if(!mbedtls_ecdh_gen_public( &(ecdh_ctx->grp), &(ecdh_ctx->d), &(ecdh_ctx->Q), mbedtls_ctr_drbg_random, ctr_drbg)){
    	RAISE_ERROR(CRYPTO_RETURN_ERROR, "Erreur lors de la creation de la cle publique");
    	mbedtls_entropy_free(&entropy);
    }


    mbedtls_entropy_free(&entropy);
    return CRYPTO_RETURN_OK;
}

CRYPTO_RETURN_TYPE generate_shared_secret(mbedtls_ctr_drbg_context  *ctr_drbg, mbedtls_ecdh_context *ecdh_ctx) {

	if(mbedtls_ecdh_compute_shared( &(ecdh_ctx->grp), &(ecdh_ctx->z),
			&(ecdh_ctx->Qp), &(ecdh_ctx->d), mbedtls_ctr_drbg_random, ctr_drbg )){
		RAISE_ERROR(CRYPTO_RETURN_ERROR, "Echec du calcul du secret partage");
	}

	return CRYPTO_RETURN_OK;
}

//
//CRYPTO_RETURN_TYPE write_dhparams_to_file(const char* filename, EVP_PKEY* dhparam)
//{
//    FILE* fp = NULL;
//    DH *params = NULL;
//
//    if (NULL == filename)
//        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Vous devez spécifier le chemin vers un fichier");
//
//    if (NULL == dhparam)
//        RAISE_ERROR(CRYPTO_RETURN_ERROR, "La structure EVP_PKEY ne doit pas être NULL");
//
//    fp = fopen(filename, "w");
//    if (NULL == fp)
//        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible de lire le fichier demandé");
//
//    params = EVP_PKEY_get0_DH(dhparam);
//    if (NULL == params) {
//        fclose(fp);
//        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible de convertir EVP_PKEY* en DH*");
//    }
//
//    if (1 != PEM_write_DHparams(fp, params)) {
//        DH_free(params);
//        fclose(fp);
//        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible d'écrire les paramètres DH dans le fichier");
//    }
//
//    DH_free(params);
//    fclose(fp);
//
//    return CRYPTO_RETURN_OK;
//}

// NOTE: Il faudra free dhparams avec EVP_PKEY_free
//CRYPTO_RETURN_TYPE read_dhparams_from_file(const char* filename, EVP_PKEY **dhparams)
//{
//    FILE* fp = NULL;
//    DH *params = NULL;
//
//    if (NULL == filename)
//        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Vous devez spécifier le chemin vers un fichier");
//
//    fp = fopen(filename, "r");
//    if (NULL == fp)
//        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible de lire le fichier demandé");
//
//    params = PEM_read_DHparams(fp, NULL, 0, NULL);
//    if (NULL == params) {
//        fclose(fp);
//        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible de lire les paramètres depuis le fichier");
//    }
//
//    *dhparams = EVP_PKEY_new();
//    if (NULL == *dhparams) {
//        fclose(fp);
//        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible d'allouer la structure EVP_PKEY");
//    }
//
//    if (1 != EVP_PKEY_set1_DH(*dhparams, params)) {
//        fclose(fp);
//        DH_free(params);
//        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible de convertir la structure DH en EVP_PKEY");
//    }
//
//    DH_free(params);
//    fclose(fp);
//
//    return CRYPTO_RETURN_OK;
//}

//CRYPTO_RETURN_TYPE convert_from_evp_pkey(EVP_PKEY* pkey, unsigned char* converted, size_t* max_len) {
//    BIO* bio_buffer = NULL;
//    int bio_written = 0;
//    size_t bio_len = 0;
//    int bio_read_len = 0;
//
//    // merci https://stackoverflow.com/a/56798741
//
//    if (NULL == converted)
//        PRINT_DEBUG("'converted' est à NULL, on place la taille de la pkey dans 'max_len'");
//
//    bio_buffer = BIO_new(BIO_s_mem());
//    if (NULL == bio_buffer)
//        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Error while allocating creating the BIO Buffer");
//
//    PRINT_DEBUG("converting pubkey to BIO structure");
//    bio_written = PEM_write_bio_PUBKEY(bio_buffer, pkey);
//    if (0 == bio_written) {
//        BIO_free(bio_buffer);
//        RAISE_ERROR(CRYPTO_RETURN_ERROR, "problem while converting pubkey to BIO structure");
//    }
//
//    /*
//     * BIO_pending est censé retourner le nombre exacte de caractères qu'il reste à lire
//     * c'est utile pour pouvoir allouer un buffer dynamiquement
//     */
//    bio_len = BIO_pending(bio_buffer);
//    if (0 == bio_len) {
//        BIO_free(bio_buffer);
//        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible de récupérer la taille de la structure BIO");
//    }
//
//    if(NULL == converted) {
//        PRINT_DEBUG("on place la taille du buffer BIO dans le paramètres 'max_len'");
//        *max_len = bio_len;
//    } else {
//        if (*max_len < bio_len) {
//            BIO_free(bio_buffer);
//            RAISE_ERROR(CRYPTO_RETURN_ERROR, "La taille du buffer passé en paramètre est trop faible");
//        }
//
//        bio_read_len = BIO_read(bio_buffer, converted, bio_len);
//        if (-1 == bio_read_len || 0 == bio_read_len) {
//            BIO_free(bio_buffer);
//            RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible d'allouer l'espace pour transformer la structure BIO");
//        } else if (-2 == bio_read_len) {
//            BIO_free(bio_buffer);
//            RAISE_ERROR(CRYPTO_RETURN_ERROR, "BIO_read n'est pas implémenté pour la structure BIO demandée");
//        }
//    }
//
//    BIO_free(bio_buffer);
//    return CRYPTO_RETURN_OK;
//}

// NOTE: dest n'a pas besoin d'etre alloué, mais devra etre libéré avec EVP_PKEY_free
//CRYPTO_RETURN_TYPE convert_to_evp_pkey(EVP_PKEY **dest, unsigned char *source, size_t key_size) {
//    BIO* bio_buffer = NULL;
//    int bio_written = 0;
//
//    if (NULL == source)
//        PRINT_DEBUG("'converted' est à NULL, aucune informations à prendre");
//
//    bio_buffer = BIO_new(BIO_s_mem());
//    if (NULL == bio_buffer)
//        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible d'allouer l'espace pour la structure BIO");
//
//    bio_written = BIO_write(bio_buffer, source, key_size);
//    if (0 >= bio_written) {
//        BIO_free(bio_buffer);
//        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible de transformer la chaine de charactère source en structure BIO");
//    }
//
//    PRINT_DEBUG("Convertion du format BIO vers le format EVP_PKEY");
//    if (NULL == PEM_read_bio_PUBKEY(bio_buffer, dest, NULL, NULL)) {
//        BIO_free(bio_buffer);
//        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible de convertir la structure BIO en EVP_PKEY");
//    }
//
//    BIO_free(bio_buffer);
//    return CRYPTO_RETURN_OK;
//}
