#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#include "utils.h"
#include "state_machine.h"
#include "crypto_primitives.h"
#include "protocol.h"

state_function_t client_states[] = {
  disconnected_state,
  client_start_connection_state,
  receive_public_key_from_peer_state,
  send_public_key_to_peer_state,
  gen_shared_secret_state,
  connected_state,
  send_encrypted_data_state,
  receive_encrypted_data_state
};

state_function_t server_states[] = {
  disconnected_state,
  server_start_connection_state,
  receive_public_key_from_peer_state,
  send_public_key_to_peer_state,
  gen_shared_secret_state,
  connected_state,
  send_encrypted_data_state,
  receive_encrypted_data_state
};

transition_t client_state_transitions[] = {
  {DISCONNECTED,                 REPEAT,  DISCONNECTED},
  {DISCONNECTED,                 ERROR,   DISCONNECTED},
  {DISCONNECTED,                 OK,      START_CONNECTION},

  {START_CONNECTION,             ERROR,   DISCONNECTED},
  {START_CONNECTION,             OK,      RECEIVE_PUBLIC_KEY_FROM_PEER},

  {RECEIVE_PUBLIC_KEY_FROM_PEER, REPEAT,  RECEIVE_PUBLIC_KEY_FROM_PEER},
  {RECEIVE_PUBLIC_KEY_FROM_PEER, ERROR,   DISCONNECTED},
  {RECEIVE_PUBLIC_KEY_FROM_PEER, OK,      SEND_PUBLIC_KEY_TO_PEER},

  {SEND_PUBLIC_KEY_TO_PEER,      OK,      GEN_SHARED_SECRET},
  {SEND_PUBLIC_KEY_TO_PEER,      ERROR,   DISCONNECTED},
  {SEND_PUBLIC_KEY_TO_PEER,      REPEAT,  SEND_PUBLIC_KEY_TO_PEER},

  {GEN_SHARED_SECRET,            OK,      CONNECTED},
  {GEN_SHARED_SECRET,            REPEAT,  GEN_SHARED_SECRET},
  {GEN_SHARED_SECRET,            ERROR,   DISCONNECTED},

  {CONNECTED,                    STOP,    DISCONNECTED},
  {CONNECTED,                    SEND,    SEND_ENCRYPTED_DATA},
  {CONNECTED,                    RECEIVE, RECEIVE_ENCRYPTED_DATA},

  {SEND_ENCRYPTED_DATA,          OK,      CONNECTED},
  {SEND_ENCRYPTED_DATA,          ERROR,   CONNECTED},
  {RECEIVE_ENCRYPTED_DATA,       OK,      CONNECTED},
  {RECEIVE_ENCRYPTED_DATA,       ERROR,   CONNECTED}
};
size_t client_transitions_amount = sizeof(client_state_transitions) / sizeof(transition_t);

transition_t server_state_transitions[] = {
  {DISCONNECTED,                 REPEAT,  DISCONNECTED},
  {DISCONNECTED,                 ERROR,   DISCONNECTED},
  {DISCONNECTED,                 OK,      START_CONNECTION},

  {START_CONNECTION,             ERROR,   DISCONNECTED},
  {START_CONNECTION,             OK,      SEND_PUBLIC_KEY_TO_PEER},

  {SEND_PUBLIC_KEY_TO_PEER,      OK,      RECEIVE_PUBLIC_KEY_FROM_PEER},

  {RECEIVE_PUBLIC_KEY_FROM_PEER, REPEAT,  RECEIVE_PUBLIC_KEY_FROM_PEER},
  {RECEIVE_PUBLIC_KEY_FROM_PEER, ERROR,   DISCONNECTED},
  {RECEIVE_PUBLIC_KEY_FROM_PEER, OK,      GEN_SHARED_SECRET},

  {GEN_SHARED_SECRET,            OK,      CONNECTED},
  {GEN_SHARED_SECRET,            REPEAT,  GEN_SHARED_SECRET},
  {GEN_SHARED_SECRET,            ERROR,   DISCONNECTED},

  {CONNECTED,                    STOP,    DISCONNECTED},
  {CONNECTED,                    SEND,    SEND_ENCRYPTED_DATA},
  {CONNECTED,                    RECEIVE, RECEIVE_ENCRYPTED_DATA},

  {SEND_ENCRYPTED_DATA,          OK,      CONNECTED},
  {SEND_ENCRYPTED_DATA,          ERROR,   CONNECTED},
  {RECEIVE_ENCRYPTED_DATA,       OK,      CONNECTED},
  {RECEIVE_ENCRYPTED_DATA,       ERROR,   CONNECTED}
};
size_t server_transitions_amount = sizeof(server_state_transitions) / sizeof(transition_t);

//int configure_serial_port(state_attr_t *sm_attr) {
//  struct termios oldtio, newtio;
//  int fd = -1;
//
//  // Ouverture d'un port série
//  fd = open(sm_attr->device_name, O_RDWR | O_NOCTTY | O_NDELAY);
//
//  if (fd == -1) {
//    PRINT_DEBUG("Impossible d'ouvrir le port serie demande");
//    PRINT_DEBUG_EXIT_FCT();
//    return fd;
//  } else {
//    PRINT_DEBUG("port successfully opened");
//    fcntl(fd, F_SETFL, 0);
//  }
//
//  tcgetattr(fd,&oldtio); /* save current port settings */
//
//  memset(&newtio, '\0', sizeof(newtio));
//  newtio.c_cflag = BAUDRATE | CS8 | CLOCAL | CREAD;
//  newtio.c_iflag = IGNPAR;
//  newtio.c_oflag = 0;
//
//  /* set input mode (non-canonical, no echo,...) */
//  newtio.c_lflag = 0;
//
//  newtio.c_cc[VTIME]    = 5;   /* inter-character timer = 5 * 0.1s */
//  newtio.c_cc[VMIN]     = 5;   /* blocking read until 5 chars received */
//
//  tcflush(fd, TCIFLUSH);
//  tcsetattr(fd,TCSANOW,&newtio);
//
//  PRINT_DEBUG("configuration du port serie termine");
//  return fd;
//}

STATE_MACHINE_RETURN_TYPE init_state_attributes(state_attr_t *sm_attr) {
  memset(sm_attr->device_name, '\0', DEVICE_NAME_LENGTH);
  sm_attr->serial_fd = -1;
  sm_attr->drbg_ctx = NULL;
  sm_attr->ecdh_ctx = NULL;
  sm_attr->secret = NULL;
  sm_attr->secret_len = 0;

  return STATE_MACHINE_OK;
}

STATE_MACHINE_RETURN_TYPE free_state_attributes(state_attr_t *sm_attr) {
  mbedtls_ecdh_free(sm_attr->ecdh_ctx);
  mbedtls_ctr_drbg_free(sm_attr->drbg_ctx);
  free(sm_attr->secret);

  return init_state_attributes(sm_attr);
}

state_t lookup_transitions(state_machine_type_t sm_type, state_t current_state, transition_code_t return_code)
{
  PRINT_DEBUG_ENTER_FCT();
  transition_t *state_transitions;
  size_t transitions_amount = 0;
  transition_t transition;
  size_t i = 0;

  switch (sm_type)
  {
    case SERVER_SM:
      PRINT_DEBUG("On effectue le lookup sur la machine à état du serveur");
      state_transitions = server_state_transitions;
      transitions_amount = server_transitions_amount;
      break;
    case CLIENT_SM:
      PRINT_DEBUG("On effectue le lookup sur la machine à état du client");
      state_transitions = client_state_transitions;
      transitions_amount = client_transitions_amount;
      break;
  }

  PRINT_DEBUG("Recherche de l'état suivant");
  for (i = 0; i < transitions_amount; i++) {
    transition = state_transitions[i];

    if (current_state == transition.from && return_code == transition.ret_code) {
      PRINT_DEBUG_EXIT_FCT();
      return transition.to;
    }
  }

  // TODO: Trouver le moyen de faire proprement un état "LOOKUP_ERROR"
  PRINT_DEBUG("aucun état suivant n'a été trouvé");
  PRINT_DEBUG_EXIT_FCT();
  return DISCONNECTED;
}

transition_code_t disconnected_state(state_attr_t *sm_attr)
{
  PRINT_DEBUG_ENTER_FCT();

  PRINT_DEBUG("Génération des paramètres pour le diffie hellman");

  if(!generate_dh_parameters(sm_attr->ecdh_ctx, sm_attr->drbg_ctx))return ERROR;
  if (sm_attr->ecdh_ctx == NULL || sm_attr->drbg_ctx == NULL) return ERROR;

  if (0 == strlen(sm_attr->device_name)) {
    printf("Veuillez entrer le chemin vers le port série que le programme utilisera\n");
    scanf("%254s", sm_attr->device_name);
    printf("port série utilisé pour la communication : %s\n", sm_attr->device_name);
  }
  
  // TODO: Faire de la vérification d'entrée pour éviter d'aller dans la 
  // fonction state_connection_state inutilement

  PRINT_DEBUG_EXIT_FCT();
  return OK;
}

transition_code_t server_start_connection_state(state_attr_t *sm_attr)
{
  packet_t hello_message;
  PRINT_DEBUG_ENTER_FCT();

  sm_attr->serial_fd = configure_serial_port(sm_attr);
  if (sm_attr->serial_fd == -1) return ERROR;

  if (PROTOCOL_ERROR == receive_packet(sm_attr->serial_fd, sm_attr->secret, &hello_message)) {
    PRINT_DEBUG("Une erreur lors de la reception du paquet s'est produite");
    PRINT_DEBUG_EXIT_FCT();
    return ERROR;
  }

  PRINT_DEBUG_EXIT_FCT();
  return OK;
}

transition_code_t client_start_connection_state(state_attr_t *sm_attr)
{
  packet_t hello_message;
  PRINT_DEBUG_ENTER_FCT();

  sm_attr->serial_fd = configure_serial_port(sm_attr);
  if (sm_attr->serial_fd == -1) return ERROR;

  PRINT_DEBUG("Création du message CLIENT_HELLO");
  if (PROTOCOL_ERROR == craft_client_hello(&hello_message)) {
    PRINT_DEBUG("Problème lors de la construction du paquet CLIENT_HELLO");
    PRINT_DEBUG_EXIT_FCT();
    return ERROR;
  }
  
  PRINT_DEBUG("Envoie du paquet");
  if (PROTOCOL_ERROR == send_packet(sm_attr->serial_fd, &hello_message)) {
    PRINT_DEBUG("Problème lors de l'envoi du paquet de bonjour");
    PRINT_DEBUG_EXIT_FCT();
    return ERROR;
  }

  PRINT_DEBUG_EXIT_FCT();
  return OK;
}

transition_code_t receive_public_key_from_peer_state(state_attr_t *sm_attr)
{
  PRINT_DEBUG_ENTER_FCT();
  packet_t message;

  if (PROTOCOL_ERROR == receive_packet(sm_attr->serial_fd, sm_attr->secret, &message)) {
    PRINT_DEBUG("Erreur lors de la réception d'un paquet");
    PRINT_DEBUG_EXIT_FCT();
    return ERROR;
  }

  if (PUBLIC_KEY_DH == message.header.message_type) {
    PRINT_DEBUG(message.body.pkey_dh.public_key);
    if (CRYPTO_RETURN_ERROR == mbedtls_mpi_write_binary(&sm_attr->ecdh_ctx->Q.X, message.body.pkey_dh.public_key, message.body.pkey_dh.public_key_len)) {
      PRINT_DEBUG("Le paquet contient une clé publique erronée");
      return ERROR;
    }
  } else {
    PRINT_DEBUG("Le paquet reçu n'est pas du bon type");
    return ERROR;
  }

  PRINT_DEBUG_EXIT_FCT();
  return OK;
}

transition_code_t send_public_key_to_peer_state(state_attr_t *sm_attr)
{
  PRINT_DEBUG_ENTER_FCT();
  packet_t message;

  // TODO: Vérifier si il y a besoin d'envoyer les paramètres diffie hellman !!!
  PRINT_DEBUG("Création du PUBLIC_KEY8DH qui sera envoyé à l'appareil distant");
  if (PROTOCOL_ERROR == craft_public_key_dh(&message, sm_attr->ecdh_ctx)) {
    PRINT_DEBUG("Impossible de forger le paquet PUBLIC_KEY_DH pour l'envoie de la clé local");
    PRINT_DEBUG_EXIT_FCT();
    return ERROR;
  }

  if (PROTOCOL_ERROR == send_packet(sm_attr->serial_fd, &message)) {
    PRINT_DEBUG("Impossible d'envoyer le paquet PUBLIC_KEY_DH sur le port série'");
    PRINT_DEBUG_EXIT_FCT();
    return ERROR;
  }

  PRINT_DEBUG_EXIT_FCT();
  return OK;
}

transition_code_t gen_shared_secret_state(state_attr_t *sm_attr) {

  PRINT_DEBUG("Génération de la clé partagée");
  if(CRYPTO_RETURN_ERROR == generate_shared_secret(sm_attr->drbg_ctx, sm_attr->ecdh_ctx)) {
    PRINT_DEBUG("Erreur lors de la génération du secret partagé");
    return ERROR;
  }

  // TODO: Faire un print spécial pour le mode debug de données sensible
  print_hex(sm_attr->secret, sm_attr->secret_len);

  return OK;
}

transition_code_t connected_state(state_attr_t *sm_attr)
{
  PRINT_DEBUG_ENTER_FCT();
  int choice = 0;

  printf("Que voulez-vous faire ? \n");
  printf("\t1 - recevoir des données\n");
  printf("\t2 - envoyer un message\n");
  printf("\t3 - stopper le programme\n");
  scanf("%d", &choice);

  switch (choice) {
  case 1:
    PRINT_DEBUG("Receiving RECEIVE order");
    return RECEIVE;
  case 2:
    PRINT_DEBUG("Receiving SEND order");
    return SEND;
  case 3:
    PRINT_DEBUG("Receiving STOP order");
    return STOP;
  
  default:
    printf("Une erreur s'est produite\n");
    return REPEAT;
    break;
  }

  PRINT_DEBUG_EXIT_FCT();
  return STOP;
}

transition_code_t send_encrypted_data_state(state_attr_t *sm_attr)
{
  PRINT_DEBUG_ENTER_FCT();
  packet_t message;

  if (PROTOCOL_ERROR == craft_data(&message, sm_attr->secret, (unsigned char *) "toto")) {
    PRINT_DEBUG("Impossible de construire le paquet à envoyer");
    return ERROR;
  }

  if (PROTOCOL_ERROR == send_packet(sm_attr->serial_fd, &message)) {
    PRINT_DEBUG("Impossible d'envoyer le paquet");
    return ERROR;
  }

  PRINT_DEBUG_EXIT_FCT();
  return OK;
}

transition_code_t receive_encrypted_data_state(state_attr_t *sm_attr)
{
  PRINT_DEBUG_ENTER_FCT();
  packet_t message;

  if (PROTOCOL_ERROR == receive_packet(sm_attr->serial_fd, sm_attr->secret, &message)) {
    PRINT_DEBUG("Impossible d'envoyer le paquet");
    return ERROR;
  }

  printf("Le message reçu est le suivant: %s\n", message.body.datas.datas);

  PRINT_DEBUG_EXIT_FCT();
  return OK;
}
