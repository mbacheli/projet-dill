#ifndef __STATE_MACHINE_H__
#define __STATE_MACHINE_H__

#define STATE_MACHINE_RETURN_TYPE size_t
#define STATE_MACHINE_OK          1
#define STATE_MACHINE_ERROR       0

#define DEVICE_NAME_LENGTH        255

#include <mbedtls/ecdh.h>
#include <mbedtls/rsa.h>
#include <mbedtls/ctr_drbg.h>

// Exemple de state machine propre : https://stackoverflow.com/a/1371654
/**
 * Les numéros à chaque éléments de l'enum doivent correspondre
 * aux index de chaque fonction associée dans le tableau 'states'.
 */
typedef enum {
  DISCONNECTED                 = 0,
  START_CONNECTION             = 1,
  RECEIVE_PUBLIC_KEY_FROM_PEER = 2,
  SEND_PUBLIC_KEY_TO_PEER      = 3,
  GEN_SHARED_SECRET            = 4,
  CONNECTED                    = 5,
  SEND_ENCRYPTED_DATA          = 6,
  RECEIVE_ENCRYPTED_DATA       = 7
} state_t;

typedef struct state_attr_t {
  char device_name[DEVICE_NAME_LENGTH];
  int serial_fd;
  mbedtls_ecdh_context* ecdh_ctx;
  mbedtls_ctr_drbg_context* drbg_ctx;
  unsigned char* secret;
  size_t secret_len;
} state_attr_t;

#define ENTRY_STATE        DISCONNECTED
#define BAUDRATE           B38400
#define DH_PARAMS_FILENAME "dhparams.txt"

typedef enum {
  OK      = 0,
  ERROR   = 1, // pour l'instant erreur = "timeout" ou "wrong packet"
  REPEAT  = 2,

  // Transition pour l'état connecté
  SEND    = 3,
  RECEIVE = 4,
  STOP    = 5
} transition_code_t;

typedef transition_code_t (*state_function_t)(state_attr_t*);
extern state_function_t client_states[];
extern state_function_t server_states[];

typedef struct transition_t {
  state_t           from;
  transition_code_t ret_code;
  state_t           to;
} transition_t;

typedef enum {
  SERVER_SM,
  CLIENT_SM
} state_machine_type_t;

STATE_MACHINE_RETURN_TYPE init_state_attributes(state_attr_t *sm_attr);
STATE_MACHINE_RETURN_TYPE free_state_attributes(state_attr_t *sm_attr);

transition_code_t disconnected_state(state_attr_t *sm_attr);
transition_code_t server_start_connection_state(state_attr_t *sm_attr);
transition_code_t client_start_connection_state(state_attr_t *sm_attr);
transition_code_t receive_public_key_from_peer_state(state_attr_t *sm_attr);
transition_code_t send_public_key_to_peer_state(state_attr_t *sm_attr);
transition_code_t gen_shared_secret_state(state_attr_t *sm_attr);
transition_code_t connected_state(state_attr_t *sm_attr);
transition_code_t send_encrypted_data_state(state_attr_t *sm_attr);
transition_code_t receive_encrypted_data_state(state_attr_t *sm_attr);

state_t lookup_transitions(state_machine_type_t sm_type, state_t current_state, transition_code_t return_code);

#endif // __STATE_MACHINE_H__
