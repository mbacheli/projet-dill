#include <string.h>
#include <stdbool.h>
#include <unistd.h>

#include "utils.h"
#include "crypto_primitives.h"
#include "protocol.h"

// TODO: A charger dynamiquement
const unsigned char *private_key = (unsigned char *) "-----BEGIN RSA PRIVATE KEY-----\n" \
"MIIEowIBAAKCAQEA4hjuuZ3BXO6xBJ0qRwDqtycZV38xvAbtvhRbYpHXFQBVEr+H\n" \
"EwirGyvBGKjeobvpwdTkdk45f5kn83ODpZ+3TZ8sqoRNaRtun3wH10vUoaShVy1e\n" \
"jyEqyFmqyrTe7B/au+USuTuueGKbTbavKt9yelWy+ceUguTuJKQl4w23sprSYbHm\n" \
"inVRFpABfXN/9cIhLfVszjMj+cJi8nQqBHEeonh4qVhtszMEPz/9i1RnMALSbQy0\n" \
"Mo6Sxq5szMJA2ZUdFpGTbtjvYwJbTz4VtyCO+hfPA1EosZ5M4x715DcHpbMrjgIe\n" \
"ciapERK8xkNdZe/B870g6ZXJ61r6IxyaYMX7ewIDAQABAoIBABhN/9OaPsADtKm/\n" \
"T10knxajMFk0hh8lS4dEXvKApPNP/tUuG5ITO3mHQZGqDYyrS5yXixugmxO5Xg8Q\n" \
"mtVCDw1cEAwgfsXOuhyQ07L0Jt5deS6K5QBT22qAIRj7Sn9tiTDrlhCtdrrgNruq\n" \
"6LeeH4tBDSVGrBNKEsODlcMLpXSgdkCcuddS6NDdRTKGLqc8zKwrivlualVk5qD5\n" \
"vWYQHhgflx/f5NhhenVMHrlJ4nWFRLvEpZko3hf7IeN28hnO5FNAptjgin+Wo59o\n" \
"9zk3P8HFVgU79uIo1+oQGGovGJf3JQ3MJn5AKZELbqBxRIP9EW9+IMyb31NRGb9c\n" \
"bDY5W5kCgYEA90Y462w2ZH92SJyk8+APgmaHNWcOWc6z6472QbtwE5Wj/4svifFq\n" \
"RJYayr9MShlG4ET43GcFJ1a40xlX1gpK1tF2wiUn6S1UwqPsLGXq0Q3Hp9x6fgQy\n" \
"dmym+kPRJ4tnM5xJPYA5Dpm4+rnhK8W1skgju+8VHhbWg5xU05MFgl8CgYEA6hNo\n" \
"B/GPMHo+v4kS0c1CjsFRZTCDh+5mcH0rVY9odHUYO/HBFSqbZ6idlbrk++8ZeGMc\n" \
"12cCalpLN5Ent33SLFHJmU7yRfpVXEYiUnjo9gcp0vj7Eqv6R4NeRlO04cWjnVsq\n" \
"yYdl7LDyNQacFN+UKeLGn9r5K3piVuVTQbYJ9GUCgYAL29NI/xSQt0gt7U0/4OP9\n" \
"hftABWQ7zFz2lBGdT2btmYSW8c7i17AX//bi+E+pUhMDGGuy7kHiBBezEAkVW8MB\n" \
"EGSYpP2IAkOJXiEqc+zR/84ub7V0K7Fb2pQbdnsmYVoesIUWrPqPh7HGXHm/BqlT\n" \
"FoCYN7wUiFXbU/kBeJCQEwKBgGjaapZsWZUAnY0U7O44QSFLbZejZLgGTbMEDt5T\n" \
"0BmivklT4QcHP/fuKzEAOES2dTVdSHUg+o8DhYANsLncTcJ2nJTlLVe1bHlaHVPO\n" \
"P6S9zrlnuKhDi2hva1KbeoJMEx+Q0BjYYwrktIqWPGKJSIh+wjSnVUhpmRWNwn7D\n" \
"+a+VAoGBAKSGi+HNv4NsbBD9oJk1OvFS0KWxrjwlOFjvno56TN0gWH/IypZMuPsx\n" \
"13+0HB1e7DO0zZgzC+oIg0xW/I1ylBemoTFld3QnVycLUo0+OGejaDcH7Ps9H/Sr\n" \
"A+lhJDL6MLRX9F1T6X9eddmsvHdUzwGMGsZ37XTLWqYSJSnOkLqk\n" \
"-----END RSA PRIVATE KEY-----\n\0";

const unsigned char *peer_sign_pubkey = (unsigned char *) "-----BEGIN PUBLIC KEY-----\n"\
"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4hjuuZ3BXO6xBJ0qRwDq\n"\
"tycZV38xvAbtvhRbYpHXFQBVEr+HEwirGyvBGKjeobvpwdTkdk45f5kn83ODpZ+3\n"\
"TZ8sqoRNaRtun3wH10vUoaShVy1ejyEqyFmqyrTe7B/au+USuTuueGKbTbavKt9y\n"\
"elWy+ceUguTuJKQl4w23sprSYbHminVRFpABfXN/9cIhLfVszjMj+cJi8nQqBHEe\n"\
"onh4qVhtszMEPz/9i1RnMALSbQy0Mo6Sxq5szMJA2ZUdFpGTbtjvYwJbTz4VtyCO\n"\
"+hfPA1EosZ5M4x715DcHpbMrjgIeciapERK8xkNdZe/B870g6ZXJ61r6IxyaYMX7\n"\
"ewIDAQAB\n"\
"-----END PUBLIC KEY-----\n\0";

bool messate_type_exist(message_type_t message_type) {
    return message_type < MESSAGE_TYPE_AMOUNT;
}

size_t message_full_size(const packet_t *message) {
    return message->header.length + sizeof(header_t);
}

// RECEIVE PART

/**
 * @brief Permet de recevoir un paquet sur le port série 'serial_fd'
 * 
 * @param serial_fd le numéro de descripteur du port série
 * @param message la structure à remplir pendant la réception
 * 
 * @return PROTOCOL_RETURN_TYPE PROTOCOL_OK si tout s'est bien passé
 */
PROTOCOL_RETURN_TYPE receive_packet(int serial_fd, const unsigned char *secret, packet_t *message) {
    if (!message)
        RAISE_ERROR(PROTOCOL_ERROR, "Vous devez allouer la structure du paquet");
    
    if (PROTOCOL_ERROR == receive_packet_header(serial_fd, message))
        RAISE_ERROR(PROTOCOL_ERROR, "Erreur lors de la réception du header");

    if (PROTOCOL_ERROR == receive_packet_body(serial_fd, secret, message))
        RAISE_ERROR(PROTOCOL_ERROR, "Erreur lors de la réception du body");

    return PROTOCOL_OK;
}

/**
 * @brief Permet de récupérer la partie header d'un paquet
 * 
 * @param serial_fd le numéro de descripteur du port série
 * @param message la structure à remplir pendant la réception
 * 
 * @return PROTOCOL_RETURN_TYPE PROTOCOL_OK si tout s'est bien passé
 */
PROTOCOL_RETURN_TYPE receive_packet_header(int serial_fd, packet_t *message) {
    unsigned char header_buffer[5] = {'\0'};
    int n = 0;

    PRINT_DEBUG("Receiving header part");
    n = read(serial_fd, header_buffer, sizeof(header_t));
    if (n < 0)
        RAISE_ERROR(PROTOCOL_ERROR, "Erreur lors de la lecture du header");

    if (PROTOCOL_ERROR == parse_header(&message->header, header_buffer))
        RAISE_ERROR(PROTOCOL_ERROR, "Erreur lors du parsing du header");

    return PROTOCOL_OK;
}

/**
 * @brief Permet de récupérer la partie body d'un paquet
 * 
 * @param serial_fd le numéro de descripteur du port série
 * @param message la structure à remplir pendant la réception
 * 
 * @return PROTOCOL_RETURN_TYPE PROTOCOL_OK si tout s'est bien passé
 */
PROTOCOL_RETURN_TYPE receive_packet_body(int serial_fd, const unsigned char *secret, packet_t *message) {
    unsigned char *body_buffer = NULL;
    int n = 0;

    PRINT_DEBUG("Allocating body part");
    body_buffer = calloc(message->header.length, sizeof(unsigned char));
    if (NULL == body_buffer)
        RAISE_ERROR(PROTOCOL_ERROR, "Impossible d'allouer l'espace pour récupérer le body");

    PRINT_DEBUG("Receiving body part");
    n = read(serial_fd, body_buffer, message->header.length);
    if (n < 0) {
        free(body_buffer);
        RAISE_ERROR(PROTOCOL_ERROR, "Erreur lors de la lecture du body");
    }

    if (PROTOCOL_ERROR == parse_body(message, secret, body_buffer)) {
        free(body_buffer);
        RAISE_ERROR(PROTOCOL_ERROR, "Erreur lors du parsing du body");
    }

    free(body_buffer);
    return PROTOCOL_OK;
}

// SEND PART

/**
 * @brief Permet d'envoyer un packet 'message' sur le port série 'serial_fd'
 * 
 * @param serial_fd le numéro de descripteur du port série
 * @param message le packet_t à envoyer
 * 
 * @return PROTOCOL_RETURN_TYPE PROTOCOL_OK si tout s'est bien passé
 */
PROTOCOL_RETURN_TYPE send_packet(int serial_fd, const packet_t *message) {
    int n;
    unsigned char* raw_message = NULL;

    PRINT_DEBUG("Allocation du buffer pour le message brut");
    raw_message = calloc(message_full_size(message), sizeof(unsigned char));
    if (NULL == raw_message)
        RAISE_ERROR(PROTOCOL_ERROR, "Erreur d'allocation du buffer pour la serialisation");

    PRINT_DEBUG("Serialisation du message dans le buffer");
    if (PROTOCOL_ERROR == serialize(message, raw_message)) {
        free(raw_message);
        RAISE_ERROR(PROTOCOL_ERROR, "Erreur d'allocation du buffer pour la serialisation");
    }

    PRINT_DEBUG("Ecriture sur le port série");
    n = write(serial_fd, raw_message, message_full_size(message));
    if (n < 0) {
        free(raw_message);
        RAISE_ERROR(PROTOCOL_ERROR, "Une erreur est survenue lors de l'envoie du paquet");
    }
    // QUESTION: Est-ce qu'il sera nécessaire de vérifier la taille envoyée ?

//    if (-1 == tcflush(serial_fd, TCIFLUSH)) {
//        free(raw_message);
//        RAISE_ERROR(PROTOCOL_ERROR, "Une erreur est survenue lors du flush du buffer du port série");
//    }

    free(raw_message);
    return PROTOCOL_OK;
}

// SERIALIZE PART

/**
 * @brief Permet de transformer un packet_t en une chaine d'octets (pas en string !)
 * 
 * @param message le packet_t à transformer
 * @param buffer le buffer dans lequel on placera le packet_t. Il doit être assez 
 *               grand pour tout contenir !
 * 
 * @return PROTOCOL_RETURN_TYPE PROTOCOL_OK si tout s'est bien passé
 */
PROTOCOL_RETURN_TYPE serialize(const packet_t *message, unsigned char *buffer) {
    if (!buffer)
        RAISE_ERROR(PROTOCOL_ERROR, "Vous devez allouer le buffer avant d'utiliser la fonction");

    if (PROTOCOL_ERROR == serialize_header(&message->header, buffer))
        RAISE_ERROR(PROTOCOL_ERROR, "Impossible de sérialiser le header");

    if (PROTOCOL_ERROR == serialize_body(message, buffer))
        RAISE_ERROR(PROTOCOL_ERROR, "Impossible de sérialiser le body");

    return PROTOCOL_OK;
}

/**
 * @brief Permet de transformer le header 'header' en une chaine d'octets
 * 
 * @param header le header_t à transformer
 * @param buffer le buffer dans lequel placer le header sérialisé. Il doit
 *               avoir une taille de 5 minimum et n'est pas vérifié.
 * 
 * @return PROTOCOL_RETURN_TYPE PROTOCOL_OK si tout s'est bien passé
 */
PROTOCOL_RETURN_TYPE serialize_header(const header_t *header, char unsigned *buffer) {
    // IMPORTANT: Il faudra faire attention à ce que les deux systèmes soit dans le meme endian
    memcpy(buffer, header, sizeof(header_t));

    return PROTOCOL_OK;
}

/**
 * @brief Permet de transformer le body 'body' en une chaine d'octets.
 * 
 * @param message le packet_t à transformer
 * @param buffer le buffer dans lequel placer le body sérialisé. Il doit
 *               avoir une taille suffisante pour contenir tout le packet_t.
 * 
 * @return PROTOCOL_RETURN_TYPE PROTOCOL_OK si tout s'est bien passé
 */
PROTOCOL_RETURN_TYPE serialize_body(const packet_t *message, unsigned char *buffer) {
    const packet_body_t *body = NULL;
    body = &message->body;
    buffer += sizeof(header_t);

    switch (message->header.message_type) {
        case CLIENT_HELLO:
            memcpy(buffer, body->client_hello.session_id, SESSION_ID_DIGITS);
            buffer += SESSION_ID_DIGITS;
            memcpy(buffer, body->client_hello.signature, body->client_hello.signature_length);
            break;
        
        case PUBLIC_KEY_DH:
            memcpy(buffer, &body->pkey_dh.public_key_len, sizeof(uint16_t));
            buffer += sizeof(uint16_t);
            memcpy(buffer, body->pkey_dh.public_key, body->pkey_dh.public_key_len);
            buffer += body->pkey_dh.public_key_len;
            memcpy(buffer, body->pkey_dh.signature, body->pkey_dh.signature_length);
            break;

        case DATA:
            memcpy(buffer, body->datas.iv, IV_SIZE);
            buffer += IV_SIZE;
            memcpy(buffer, body->datas.tag, TAG_SIZE);
            buffer += TAG_SIZE;
            memcpy(buffer, body->datas.enc_datas, body->datas.datas_length);
            break;

        default:
            return PROTOCOL_ERROR;
    }

    return PROTOCOL_OK;
}

// PARSE PART

/**
 * @brief Remplit la structure header_t 'header' avec le contenu de 'message'
 * 
 * @param header la structure header_t à remplir
 * @param char la chaine d'octets utilisés pour remplir header_t, aucune
 *             vérification n'est faite dessus.
 * 
 * @return PROTOCOL_RETURN_TYPE PROTOCOL_OK si tout s'est bien passé
 */
PROTOCOL_RETURN_TYPE parse_header(header_t *header, const unsigned char* message) {
    memcpy(header, message, sizeof(header_t));

    // Petit check pour voir si le type de message est ok
    if (!messate_type_exist(header->message_type))
        RAISE_ERROR(PROTOCOL_ERROR, "Le type du message reçu n'existe pas !");

    return PROTOCOL_OK;
}

/**
 * @brief Complète la partie body de packet grace au contenu de message
 * 
 * @param packet la structure packet à remplir
 * @param message le corps du message (sans le header donc)
 * 
 * @return PROTOCOL_RETURN_TYPE PROTOCOL_OK si tout s'est bien passé
 */
PROTOCOL_RETURN_TYPE parse_body(packet_t *packet, const unsigned char *secret, const unsigned char *message) {
    switch (packet->header.message_type) {
        case CLIENT_HELLO:
            parse_body_client_hello(packet, message);
            break;
        case PUBLIC_KEY_DH:
            parse_body_public_key_dh(packet, message);
            break;
        case DATA:
            parse_body_data(packet, secret, message);
            break;
        case END_COMMUNICATION:
            break;
        default:
            PRINT_DEBUG("Type de paquet non reconnu");
            return PROTOCOL_ERROR;
            break;
    }
    
    return PROTOCOL_OK;
}

/**
 * @brief Complète la partie body de packet grace au contenu de message
 * 
 * @param packet la structure packet à remplir
 * @param message le corps du message (sans le header donc)
 * 
 * @return PROTOCOL_RETURN_TYPE PROTOCOL_OK si tout s'est bien passé
 */
PROTOCOL_RETURN_TYPE parse_body_client_hello(packet_t *packet, const unsigned char *body_buffer) {
    client_hello_body_t *body = &(packet->body.client_hello);
    size_t is_valid = 0;

    PRINT_DEBUG("On traite un paquet de type CLIENT_HELLO");

    /* QUESTION: A voir si on change la manière de faire en :
          1. remplir la structure
          2. vérifier la signature
          3. la vider si nécessaire
    */

    // On recopie dans le verify ce qui est passé dans la structure avant de la remplir pour
    // éviter de rendre à l'utilisateur une structure avec des informations non signées.
    is_valid = verify(
        body_buffer,
        SESSION_ID_DIGITS,
        body_buffer + SESSION_ID_DIGITS,
        packet->header.length - SESSION_ID_DIGITS
	);
    if (CRYPTO_RETURN_OK == is_valid)
        PRINT_DEBUG("La signature du paquet CLIENT_HELLO est valide");
    else
        RAISE_ERROR(PROTOCOL_ERROR, "La signature du paquet est non valide");
    
    memcpy(body->session_id, body_buffer, SESSION_ID_DIGITS);
    body->signature_length = packet->header.length - SESSION_ID_DIGITS;
    body->signature = calloc(body->signature_length, sizeof(unsigned char));
    memcpy(body->signature, body_buffer + SESSION_ID_DIGITS, body->signature_length);

    return PROTOCOL_OK;
}

/**
 * @brief 
 * 
 * @param packet 
 * @param body_buffer 
 * 
 * @return PROTOCOL_RETURN_TYPE PROTOCOL_OK si tout s'est bien passé
 */
PROTOCOL_RETURN_TYPE parse_body_public_key_dh(packet_t *packet, const unsigned char *body_buffer) {
    public_key_dh_body_t *body = &(packet->body.pkey_dh);
    size_t offset = 0;
    size_t is_valid = 0;
    uint16_t pkey_len = 0;

    PRINT_DEBUG("On traite un paquet de type PUBLIC_KEY_DH");

    memcpy(&pkey_len, body_buffer, sizeof(uint16_t));

    // On recopie dans le verify ce qui est passé dans la structure avant de la remplir pour
    // éviter de rendre à l'utilisateur une structure avec des informations non signées.
    is_valid = verify(
        body_buffer,
        sizeof(uint16_t) + pkey_len,
        body_buffer + sizeof(uint16_t) + pkey_len,
        packet->header.length - sizeof(uint16_t) - pkey_len
    );
    if (CRYPTO_RETURN_OK == is_valid)
        PRINT_DEBUG("La signature du paquet CLIENT_HELLO est valide");
    else
        RAISE_ERROR(PROTOCOL_ERROR, "La signature du paquet est non valide");

    body->public_key_len = pkey_len;
    offset += sizeof(uint16_t);

    body->public_key = calloc(body->public_key_len, sizeof(unsigned char));
    memcpy(body->public_key, body_buffer + offset, body->public_key_len);
    offset += body->public_key_len;
    
    body->signature_length = packet->header.length - sizeof(uint16_t) - body->public_key_len;
    body->signature = calloc(body->signature_length, sizeof(unsigned char));
    memcpy(body->signature, body_buffer + offset, body->signature_length);

    return PROTOCOL_OK;
}

PROTOCOL_RETURN_TYPE parse_body_data(packet_t *packet, const unsigned char *secret, const unsigned char *body_buffer) {
    unsigned char header_buffer[sizeof(header_t)] = {'\0'};
    data_body_t *body = &(packet->body.datas);
    CRYPTO_RETURN_TYPE decryption_result;
    int decrypted_length = 0;

    body->datas_length = packet->header.length - IV_SIZE - TAG_SIZE;

    memcpy(body->iv, body_buffer, IV_SIZE);
    body_buffer += IV_SIZE;

    memcpy(body->tag, body_buffer, TAG_SIZE);
    body_buffer += TAG_SIZE;

    body->enc_datas = calloc(body->datas_length, sizeof(unsigned char));
    if (NULL == body->enc_datas) 
        RAISE_ERROR(PROTOCOL_ERROR, "Impossible d'allouer l'espace pour les données chiffrés");

    memcpy(body->enc_datas, body_buffer, body->datas_length);

    body->datas = calloc(body->datas_length, sizeof(unsigned char));
    if (NULL == body->enc_datas) 
        RAISE_ERROR(PROTOCOL_ERROR, "Impossible d'allouer l'espace pour les données chiffrés");

    if (PROTOCOL_ERROR == serialize_header(&packet->header, header_buffer))
        RAISE_ERROR(PROTOCOL_ERROR, "Impossible de récupérer le header depuis la structure packet_t");

    decryption_result = decrypt(
        body->enc_datas, body->datas_length,
        header_buffer, sizeof(header_t),
        body->tag,
        secret,
        body->iv, IV_SIZE,
        body->datas, &decrypted_length
    );
    if (CRYPTO_RETURN_ERROR == decryption_result)
        RAISE_ERROR(PROTOCOL_ERROR, "Impossible de déchiffre le message");

    if ((size_t) decrypted_length != body->datas_length)
        RAISE_ERROR(PROTOCOL_ERROR, "La taille des données déchiffrés ne correspond pas à la taille attendue");

    return PROTOCOL_OK;
}

// CRAFT PART

PROTOCOL_RETURN_TYPE free_packet(packet_t *message) {
    switch (message->header.message_type) {
        case CLIENT_HELLO:
            memset(message->body.client_hello.session_id, '\0', SESSION_ID_DIGITS);
            if (ERASE_MEMORY_FOR_REUSE)
                memset(message->body.client_hello.signature, '\0', message->body.client_hello.signature_length);
            free(message->body.client_hello.signature);
            message->body.client_hello.signature = NULL;
            message->body.client_hello.signature_length = 0;
            break;
        
        case PUBLIC_KEY_DH:
            if (ERASE_MEMORY_FOR_REUSE) {
                memset(message->body.pkey_dh.public_key, '\0', message->body.pkey_dh.public_key_len);
                memset(message->body.pkey_dh.signature, '\0', message->body.pkey_dh.signature_length);
            }
            free(message->body.pkey_dh.public_key);
            message->body.pkey_dh.public_key = NULL;
            message->body.pkey_dh.public_key_len = 0;
            free(message->body.pkey_dh.signature);
            message->body.pkey_dh.signature = NULL;
            message->body.pkey_dh.signature_length = 0;
            break;

        case DATA:
            memset(message->body.datas.iv, '\0', IV_SIZE);
            memset(message->body.datas.tag, '\0', TAG_SIZE);
            if (ERASE_MEMORY_FOR_REUSE) {
                memset(message->body.datas.datas, '\0', message->body.datas.datas_length);
                memset(message->body.datas.enc_datas, '\0', message->body.datas.datas_length);
            }
            free(message->body.datas.datas);
            message->body.datas.datas = NULL;
            free(message->body.datas.enc_datas);
            message->body.datas.enc_datas = NULL;
            message->body.datas.datas_length = 0;
            break;

        default:
            RAISE_ERROR(PROTOCOL_ERROR, "On a reçu un type de message inconnu");
            break;
    }

    message->header.length = 0;
    message->header.sequence_number = 0;
    message->header.message_type = CLIENT_HELLO;

    return PROTOCOL_OK;
}

/**
 * @brief Permet de construire un paquet de type CLIENT_HELLO
 * 
 * @param message La structure qui sera rempli par la fonction
 * 
 * @return PROTOCOL_RETURN_TYPE PROTOCOL_OK si tout s'est bien passé
 */
PROTOCOL_RETURN_TYPE craft_client_hello(packet_t *message) {
    unsigned char* message_signature = NULL;
    size_t signed_len = 0;
    unsigned char session_id[SESSION_ID_DIGITS] = {'\0'};
 
    if (!message)
        RAISE_ERROR(PROTOCOL_ERROR, "Received message buffer was NULL");

    if (CRYPTO_RETURN_ERROR == generate_session_id(session_id))
        RAISE_ERROR(PROTOCOL_ERROR, "Error while generating session ID");

    if (CRYPTO_RETURN_ERROR == sign(session_id, SESSION_ID_DIGITS , message_signature))
        RAISE_ERROR(PROTOCOL_ERROR, "Error while signing message body");

    if (SESSION_ID_DIGITS + signed_len > UINT16_MAX) 
        RAISE_ERROR(PROTOCOL_ERROR, "Message is too long for length packet_t's attribute");

    client_hello_body_t *body = &message->body.client_hello;

    // FIXME: Peut être vérifier les erreurs à ce niveau là
    body->signature_length = signed_len;
    body->signature = calloc(signed_len, sizeof(unsigned char));
    memcpy(body->signature, message_signature, signed_len);
    memcpy(body->session_id, session_id, SESSION_ID_DIGITS);

    message->header.message_type = CLIENT_HELLO;
    message->header.sequence_number = 0;
    message->header.length = SESSION_ID_DIGITS + signed_len;

    free(message_signature);
    return PROTOCOL_OK;
}

/**
 * @brief Créer un packet_t de type PUBLIC_KEY_DH, en utilisant une clé
 *        Diffie Hellman
 * 
 * @param message La structure qui sera rempli par la fonction
 * @param diffie_key La clé diffie hellman qui sera placée dans le paquet
 * 
 * @return PROTOCOL_RETURN_TYPE PROTOCOL_OK si tout s'est bien passé
 */
PROTOCOL_RETURN_TYPE craft_public_key_dh(packet_t *message, mbedtls_ecdh_context *ecdh_ctx) {
    unsigned char* message_signature = NULL;
    size_t signed_len = 0;
    unsigned char *key_buffer = NULL;
    size_t key_bio_len = 0;

    if (!message)
        RAISE_ERROR(PROTOCOL_ERROR, "Vous devez d'abord allouer l'espace nécessaire pour le paquet");

    key_bio_len = mbedtls_mpi_size(&(ecdh_ctx->Q));

    key_buffer = calloc(key_bio_len + sizeof(uint16_t), sizeof(unsigned char));
    if (NULL == key_buffer)
        RAISE_ERROR(PROTOCOL_ERROR, "Erreur lors de l'allocation du buffer de charactères pour la clé");

    memcpy(key_buffer, &key_bio_len, sizeof(uint16_t));

    if (mbedtls_mpi_write_string(&(ecdh_ctx->Q), 16, key_buffer, key_bio_len, &key_bio_len ) != 0) {
        free(key_buffer);
        RAISE_ERROR(PROTOCOL_ERROR, "Erreur lors de la conversion de la clé en chaine de cahractères");
    }

    if (CRYPTO_RETURN_OK != sign(key_buffer, key_bio_len + sizeof(uint16_t), message_signature)) {
        free(key_buffer);
        RAISE_ERROR(PROTOCOL_ERROR, "Error while signing message body");
    }

    public_key_dh_body_t *body = &message->body.pkey_dh;

    body->public_key = calloc(key_bio_len, sizeof(unsigned char));
    memcpy(body->public_key, key_buffer + sizeof(uint16_t), key_bio_len);
    body->public_key_len = key_bio_len;

    body->signature = calloc(signed_len, sizeof(unsigned char));
    memcpy(body->signature, message_signature, signed_len);
    body->signature_length = signed_len;

    message->header.message_type = PUBLIC_KEY_DH;
    message->header.sequence_number = 0;
    message->header.length = sizeof(uint16_t) + key_bio_len + signed_len;

    OPENSSL_free(message_signature);
    free(key_buffer);
    return PROTOCOL_OK;
}

/**
 * @brief Créer un paquet de type DATA avec les données contenus dans 'datas'
 * 
 * @param message La structure qui sera rempli par la fonction
 * @param secret La clé partagée utilisée pour chiffrer les données
 * @param datas Les données brutes à utilisées. Pour l'instant ne peut être 
 *              qu'une chaine de caractères.
 * 
 * TODO: A implémenter
 * 
 * @return PROTOCOL_RETURN_TYPE PROTOCOL_OK si tout s'est bien passé
 */
PROTOCOL_RETURN_TYPE craft_data(packet_t *message, const unsigned char *secret, const unsigned char *datas) {
    unsigned char header_buffer[sizeof(header_t)] = {'\0'};
    size_t cipher_size = determine_cipher_size(strlen((char *) datas));
    data_body_t *body = NULL;
    CRYPTO_RETURN_TYPE encryption_result;

    if (!message)
        RAISE_ERROR(PROTOCOL_ERROR, "Vous devez d'abord allouer l'espace nécessaire pour le paquet");

    message->header.message_type = DATA;
    message->header.sequence_number = 0; // TODO: Bien implémenter le sequence number
    message->header.length = cipher_size + IV_SIZE + TAG_SIZE;
    /* QUESTION: Est-ce qu'on serais capable de prédire la taille du paquet ?
     * voir ici: https://www.quora.com/Information-Security-How-does-encryption-affect-a-file-size
     * 
     * TODO: Trouver la taille à allouer avec https://crypto.stackexchange.com/a/54022
     *       Trouver aussi la taille du tag pour AES-GCM
     */

    body = &message->body.datas;
    body->enc_datas = calloc(cipher_size, sizeof(unsigned char));

    if (PROTOCOL_ERROR == serialize_header(&message->header, header_buffer))
        RAISE_ERROR(PROTOCOL_ERROR, "Impossible de sérialiser le header");

    if (CRYPTO_RETURN_ERROR == generate_aes_iv(body->iv))
        RAISE_ERROR(PROTOCOL_ERROR, "Impossible de générer le vecteur d'initialisation");

    encryption_result = encrypt(
        datas, strlen((char *) datas),
        header_buffer, sizeof(header_t),
        body->iv, IV_SIZE,
        secret, 
        body->enc_datas, (int *) &body->datas_length, body->tag
    );
    if (CRYPTO_RETURN_ERROR == encryption_result)
        RAISE_ERROR(PROTOCOL_ERROR, "Impossible de chiffrer le message");

    if (body->datas_length != cipher_size)
        RAISE_ERROR(PROTOCOL_ERROR, "La taille du chiffré est différente de celle prévue");

    return PROTOCOL_OK;
}

/**
 * @brief Permet de construire un paquet de terminaison de connexion
 *        (END_COMMUNICATION).
 * 
 * @param message La structure qui sera rempli par la fonction
 * 
 * TODO: A implémenter
 * 
 * @return PROTOCOL_RETURN_TYPE PROTOCOL_OK si tout s'est bien passé
 */
PROTOCOL_RETURN_TYPE craft_end_communication(packet_t *message) {
    return PROTOCOL_OK;
}
