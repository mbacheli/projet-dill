#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#include "crypto_primitives.h"
#include "protocol.h"
#include "state_machine.h"
#include "utils.h"

void test_aes_gcm(void)
{
    /*
     * Set up the key and iv. Do I need to say to not hard code these in a
     * real application? :-)
     */

    /* A 256 bit key */
    unsigned char *key = (unsigned char *) "01234567890123456789012345678901";

    /* A 128 bit IV */
    unsigned char *iv = (unsigned char *) "0123456789012345";

    /* Message to be encrypted */
    unsigned char *plaintext =
        (unsigned char *)"The quick brown fox jumps over the lazy dog";
    unsigned char *aad =
        (unsigned char *)"The quick brown fox jumps over the lazy dog";

    /*
     * Buffer for ciphertext. Ensure the buffer is long enough for the
     * ciphertext which may be longer than the plaintext, depending on the
     * algorithm and mode.
     */
    unsigned char ciphertext[128 * 4];
    unsigned char tag[TAG_SIZE];
    /* Buffer for the decrypted text */
    unsigned char decryptedtext[128 * 4];
    int decryptedtext_len, ciphertext_len;

    /* Encrypt the plaintext */
    if (CRYPTO_RETURN_ERROR == encrypt(plaintext, strlen((char*) plaintext),
                                       aad, strlen((char*) aad),
                                       iv, strlen((char*) iv),
                                       key,
                                       ciphertext, &ciphertext_len, tag))
    {
        printf("erreur lors du déchiffrement\n");
    }
    printf("encrypted : %d bytes\n", ciphertext_len);

    /* Do something useful with the ciphertext here */
    printf("Ciphertext is:\n");
    BIO_dump_fp (stdout, (const char *)ciphertext, ciphertext_len);

    /* Decrypt the ciphertext */
    if (CRYPTO_RETURN_ERROR == decrypt(ciphertext, ciphertext_len,
                                aad, strlen((char *) aad),
                                tag,
                                key, iv, strlen((char *) iv),
                                decryptedtext, &decryptedtext_len))
        printf("erreur lors du déchiffrement");

    printf("decrypted %d bytes\n", decryptedtext_len);
    /* Add a NULL terminator. We are expecting printable text */
    decryptedtext[decryptedtext_len] = '\0';

    /* Show the decrypted text */
    printf("Decrypted text is:\n");
    printf("%s\n", decryptedtext);
}

void test_state_machine(void)
{
    state_t current_state = ENTRY_STATE;
    // Peut etre changer de terminologie, return_code_t serait surement mieux
    transition_code_t return_code;
    state_function_t state_function;
    state_attr_t sm_attributes;
    
    init_state_attributes(&sm_attributes);

    while (1) {
        printf("current state number : %d\n", current_state);
        state_function = client_states[current_state];
        return_code = state_function(&sm_attributes);
        current_state = lookup_transitions(CLIENT_SM, current_state, return_code);

        if (current_state == DISCONNECTED)
            break;
    }
    
}

void print_hash(unsigned char *hash){
    int i, hash_len = strlen((char *) hash);

    for(i = 0; i < hash_len; i++)
        printf("%02x", hash[i]);

    printf("\n");
}

void test_sha256() {
    unsigned char hashed_word[33];
    unsigned int hash_len = 0;

    hash((unsigned char*) "bonjour", strlen("bonjour"), hashed_word, &hash_len);
    printf("-----\nreceived : ");
    print_hash(hashed_word);
    printf("should be: 2cb4b1431b84ec15d35ed83bb927e27e8967d75f4bcd9cc4b25c8d879ae23e18\n");
    printf("-----\n");
}

void test_signature() {

	CRYPTO_RETURN_TYPE create_RSA();
	unsigned char signature[32];
	const unsigned char *message = (unsigned char *) "toto";

	sign(message, strlen((char*) message), signature);

	verify(message, strlen((char*) message),signature, 32);
}

//void test_serial() {
//    int fd;
//
//    // Ouverture d'un port série
//    fd = open("/dev/pts/4", O_RDWR | O_NOCTTY | O_NDELAY);
//    if (fd == -1)
//        perror("open_port: Unable to open /dev/ttyf1 - ");
//    else {
//        printf("port successfully opened\n");
//        fcntl(fd, F_SETFL, 0);
//    }
//
//    // Ecriture
//    int n = write(fd, "ATZ\r", 4);
//    if (n < 0)
//        fputs("write() of 4 bytes failed!\n", stderr);
//
//    // Lecture (l'appel est bloquant)
//    char buffer[255] = {'\0'};
//    n = read(fd, buffer, 255);
//    if (n < 0) {
//        fputs("read() failed!\n", stderr);
//    } else {
//        printf("read %d bytes\n", n);
//        buffer[n] = '\0';
//        printf("%s", buffer);
//    }
//
//    // fermure du port série
//    close(fd);
//
//    return;
//}

void test_messate_type_exist(void) {
    if (messate_type_exist(6))
        printf("error while trying messate_type_exist function\n");
    else
        printf("OK\n");

    if(!messate_type_exist(CLIENT_HELLO))
        printf("error while trying messate_type_exist function\n");
    else
        printf("OK\n");
}

void test_parse_header(void) {
    header_t source_header, final_header;
    unsigned char buffer[255] = {'\0'};

    source_header.length = 10;
    source_header.message_type = CLIENT_HELLO;
    source_header.sequence_number = 2;

    printf("sizeof(header_t) = %ld (should be 5)\n", sizeof(header_t));
    printf("sizeof(message_type_t) = %ld (should be 1)\n", sizeof(message_type_t));
    serialize_header(&source_header, buffer);
    print_hex(buffer, sizeof(header_t));
    parse_header(&final_header, buffer);

    printf("%d, %d, %d\n", final_header.length, final_header.message_type, final_header.sequence_number);
}

void test_diffie_hellman(void) {
	mbedtls_ctr_drbg_context* drbg_ctx = NULL;
	mbedtls_ecdh_context* ecdh_ctx = NULL;
    unsigned char* secret = NULL;
    char buf[32];

    printf("Generating diffie hellman parameters\n");
    
    if (CRYPTO_RETURN_ERROR == generate_dh_parameters(drbg_ctx, ecdh_ctx))
        printf("error while generating df_parameters\n");

    printf("Generating the shared secret\n");
    if(CRYPTO_RETURN_ERROR == generate_shared_secret(drbg_ctx, ecdh_ctx))
        printf("error while generating the shared secret\n");

    printf("THE SHARED SECRET WAS SUCCESSFULLY GENERATED -->\n");
    // QUESTION: Est-ce que secret_len est la taille en bit ou en octets ?
    mbedtls_mpi_write_string(&ecdh_ctx->z, 16, buf, 32, 0);
    printf("%s\n", buf);
    // XXX: Si c'est la taille en bit alors il faudrait faire l'appel suivant :
    // print_hex(secret, secret_len / 8);


    if (secret) OPENSSL_free(secret);
}

void test_session_id_generator(void) {
    unsigned char buffer[SESSION_ID_DIGITS] = {'\0'};

    generate_session_id(buffer);
    print_hex(buffer, SESSION_ID_DIGITS);
    generate_session_id(buffer);
    print_hex(buffer, SESSION_ID_DIGITS);
    generate_session_id(buffer);
    print_hex(buffer, SESSION_ID_DIGITS);
}

void test_packet_craft_client_hello(void) {
    packet_t message;
    packet_t reception;
    unsigned char serialized_message[1024] = {'\0'};
    unsigned char *secret = (unsigned char *) "01234567890123456789012345678901";

    if (PROTOCOL_ERROR == craft_client_hello(&message)) {
        printf("Erreur lors du craft du paquet CLIENT_HELLO\n");
        return;
    }

    if (PROTOCOL_ERROR == serialize(&message, serialized_message)) {
        printf("Erreur lors de la serialisation du paquet CLIENT_HELLO\n");
        return;
    }

    print_hex(serialized_message, message.header.length + sizeof(header_t));

    if (PROTOCOL_ERROR == parse_header(&reception.header, serialized_message)) {
        printf("Erreur lors du parsing du header\n");
    }
    if (PROTOCOL_ERROR == parse_body(&reception, secret, serialized_message + sizeof(header_t))) {
        printf("Erreur lors du parsing du body\n");
    }
}

void test_packet_craft_public_key(void) {
    mbedtls_ecdh_context *ecdh_ctx = NULL;
    mbedtls_ctr_drbg_context *drbg_ctx = NULL;
    packet_t message;

    printf("Generating diffie hellman parameters\n");
    if (CRYPTO_RETURN_ERROR == generate_dh_parameters(drbg_ctx, ecdh_ctx))
        printf("error while generating df_parameters\n");

    craft_public_key_dh(&message, ecdh_ctx);
}

void test_packet_craft_data(void) {
	mbedtls_ecdh_context* ecdh_serv_ctx;
	mbedtls_ecdh_context* ecdh_cli_ctx;
	mbedtls_ctr_drbg_context* drbg_ctx;
    unsigned char *secret = NULL;
    packet_t message;

    mbedtls_ecdh_init(ecdh_cli_ctx);
    mbedtls_ecdh_init(ecdh_serv_ctx);
    mbedtls_ctr_drbg_init(drbg_ctx);

    printf("Generating diffie hellman parameters\n");
    if (CRYPTO_RETURN_ERROR == generate_dh_parameters(drbg_ctx, ecdh_serv_ctx))
        printf("error while generating df_parameters\n");

    if (CRYPTO_RETURN_ERROR == generate_dh_parameters(drbg_ctx, ecdh_cli_ctx))
            printf("error while generating df_parameters\n");

    ecdh_serv_ctx->Qp = ecdh_cli_ctx->Q;

    printf("Generating the secret\n");
    if (CRYPTO_RETURN_ERROR == generate_shared_secret(drbg_ctx, ecdh_serv_ctx)) {
        printf("error while generating the shared secret\n");
        return;
    }

    unsigned char *raw_datas = (unsigned char *) "totoleplusbo";
    if (PROTOCOL_ERROR == craft_data(&message, secret, raw_datas)) {
        printf("error while generating the DATA packet\n");
        return;
    }

    printf("length:        %d\n", message.header.length);
    printf("datas_length: %ld\n", message.body.datas.datas_length);
    printf("enc_datas:\n");
    print_hex(message.body.datas.enc_datas, message.body.datas.datas_length);
    printf("iv:\n");
    print_hex(message.body.datas.iv, IV_SIZE);
    printf("tag:\n");
    print_hex(message.body.datas.tag, TAG_SIZE);
}

void test_get_short(void) {
    uint16_t value1 = 10;
    uint16_t value2 = 640;
    unsigned char array[4] = {'\0'};

    memcpy (array, &value1, 2);
    memcpy (array + 2, &value2, 2);

    printf("value1 = %d\n", get_short(array, 0));
    printf("value2 = %d\n", get_short(array, 2));
}

int main (void)
{
    printf("Il faut implémenter une vraie suite de test !!!\n");

    // test_signature();
    // test_aes_gcm();
    // test_sha256();
    // test_serial();
    // test_messate_type_exist();
    // test_parse_header();
    // test_session_id_generator();
    // test_packet_craft_client_hello();
    // test_packet_craft_public_key();
    // test_get_short();

    test_packet_craft_data();

    // test_diffie_hellman();
    // test_packet_craft_public_key();

    // test_state_machine();

    return 0;
}
