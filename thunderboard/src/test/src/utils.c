#include <unistd.h>

#include "utils.h"

void print_hex(const unsigned char *s, size_t size) {
	for (size_t i = 0; i < size; i++)
		printf("%02x", (int) s[i]);
	printf("\n");
}

//void print_bio(BIO* bio_buffer) {
//	char buffer[1024] = { '\0' };
//
//	while (BIO_read(bio_buffer, buffer, 1024) > 0)
//		printf("%s", buffer);
//
//	printf("\n");
//}

uint16_t get_short(unsigned char* array, int offset) {
	return (uint16_t) (((uint16_t) array[offset + 1]) << 8) | array[offset];
}
