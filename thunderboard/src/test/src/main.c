/***************************************************************************//**
 * @file
 * @brief Simple LED Blink Demo
 *******************************************************************************
 * # License
 * <b>Copyright 2018 Silicon Laboratories Inc. www.silabs.com</b>
 *******************************************************************************
 *
 * The licensor of this software is Silicon Laboratories Inc. Your use of this
 * software is governed by the terms of Silicon Labs Master Software License
 * Agreement (MSLA) available at
 * www.silabs.com/about-us/legal/master-software-license-agreement. This
 * software is distributed to you in Source Code format and is governed by the
 * sections of the MSLA applicable to Source Code.
 *
 ******************************************************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "em_device.h"
#include "em_chip.h"
#include "em_cmu.h"
#include "em_emu.h"
#include "crypto_primitives.h"

/***************************************************************************//**
 * @brief  Main function
 ******************************************************************************/
int main(void)
{

	setupSWOForPrint();

// TEST ECDH
//	mbedtls_ecdh_context ecdh_srv;
//	mbedtls_ecdh_context ecdh_cli;
//	mbedtls_ctr_drbg_context drbg;
//	unsigned char cli_to_srv[32], srv_to_cli[32];
//
//	mbedtls_ctr_drbg_init(&drbg);
//	mbedtls_ecdh_init(&ecdh_srv);
//	mbedtls_ecdh_init(&ecdh_cli);
//
//	generate_dh_parameters(&drbg, &ecdh_cli);
//	generate_dh_parameters(&drbg, &ecdh_srv);
//
//	mbedtls_mpi_write_binary( &ecdh_cli.Q.X, cli_to_srv, 32 );
//	mbedtls_mpi_write_binary( &ecdh_srv.Q.X, srv_to_cli, 32 );
//
//	mbedtls_mpi_read_binary( &ecdh_cli.Qp.X, srv_to_cli, 32 );
//	mbedtls_mpi_read_binary( &ecdh_srv.Qp.X, cli_to_srv, 32 );
//
//	mbedtls_mpi_lset( &ecdh_cli.Qp.Z, 1 );
//	mbedtls_mpi_lset( &ecdh_srv.Qp.Z, 1 );
//
//	generate_shared_secret(&drbg, &ecdh_cli);
//	generate_shared_secret(&drbg, &ecdh_srv);
//
//	if(!mbedtls_mpi_cmp_mpi(&ecdh_cli.z, &ecdh_srv.z)){
//		printf("Les cles sont identiques");
//	} else {
//		printf("Les deux cles sont differentes");
//	}

// TEST AES_GCM
	unsigned char header[] = "HELLO15002";
	unsigned char init_vector[] = "_initvector_";
	unsigned char key[] = "01234567890123456789012345678901";
	unsigned char plaintext8[] = "fylvRrKJ";
	unsigned char plaintext16[] = "hhl@HEHqij9KKuKk";
	unsigned char plaintext32[] = "yggJ-4qQqJ22CAroSSO@4SvDkoN7m0H0";
	unsigned char plaintext64[] = "w6GYqP%GzQk~ak7io*AtluxPBUMUMgKg28R4pgkcFF2cruBFQ$#HHo9vNvVx~lon";
	unsigned char cipher[65];
	unsigned char hmac[16];
	unsigned char decipher[65];

	encrypt(
			plaintext8,
			sizeof plaintext8,
			header,
			sizeof header,
			init_vector,
			sizeof init_vector,
			key,
			cipher,
			sizeof cipher,
			hmac);

	decrypt(
			cipher,
			strlen(cipher),
			header,
			sizeof header,
			hmac,
			key,
			init_vector,
			sizeof init_vector,
			decipher,
			sizeof decipher);



}
