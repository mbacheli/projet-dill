#ifndef __ATTACK_H__
#define __ATTACK_H__

void simple_replay_attack(char *client_device_name, char *server_device_name);
void spam_replay_attack(char *client_device_name, char *server_device_name);
void smart_replay_attack(char *client_device_name, char *server_device_name);
void unwanted_end_communication(char *client_device_name, char *server_device_name);
void evil_server(char *server_device_name);

#endif // __ATTACK_H__