#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <string.h>
#include <sys/select.h>
#include <openssl/evp.h>
#include <openssl/bio.h>
#include <openssl/pem.h>

#include "utils.h"
#include "state_machine.h"
#include "crypto_primitives.h"
#include "protocol.h"

const char *state_names[] = {
  STRINGIFY(DISCONNECTED),
  STRINGIFY(START_CONNECTION),
  STRINGIFY(RECEIVE_PUBLIC_KEY_FROM_PEER),
  STRINGIFY(SEND_PUBLIC_KEY_TO_PEER),
  STRINGIFY(GEN_SHARED_SECRET),
  STRINGIFY(CONNECTED),
  STRINGIFY(SEND_ENCRYPTED_DATA),
  STRINGIFY(RECEIVE_ENCRYPTED_DATA),
  STRINGIFY(STOP_COMMUNICATION)
};

state_function_t client_states[] = {
  disconnected_state,
  client_start_connection_state,
  receive_public_key_from_peer_state,
  send_public_key_to_peer_state,
  gen_shared_secret_state,
  connected_state,
  send_encrypted_data_state,
  receive_encrypted_data_state,
  stop_communication_state
};

state_function_t server_states[] = {
  disconnected_state,
  server_start_connection_state,
  receive_public_key_from_peer_state,
  send_public_key_to_peer_state,
  gen_shared_secret_state,
  connected_state,
  send_encrypted_data_state,
  receive_encrypted_data_state,
  stop_communication_state
};

transition_t client_state_transitions[] = {
  {DISCONNECTED,                 REPEAT,  DISCONNECTED},
  {DISCONNECTED,                 ERROR,   DISCONNECTED},
  {DISCONNECTED,                 OK,      START_CONNECTION},

  {START_CONNECTION,             ERROR,   DISCONNECTED},
  {START_CONNECTION,             OK,      RECEIVE_PUBLIC_KEY_FROM_PEER},

  {RECEIVE_PUBLIC_KEY_FROM_PEER, REPEAT,  RECEIVE_PUBLIC_KEY_FROM_PEER},
  {RECEIVE_PUBLIC_KEY_FROM_PEER, ERROR,   DISCONNECTED},
  {RECEIVE_PUBLIC_KEY_FROM_PEER, OK,      SEND_PUBLIC_KEY_TO_PEER},

  {SEND_PUBLIC_KEY_TO_PEER,      OK,      GEN_SHARED_SECRET},
  {SEND_PUBLIC_KEY_TO_PEER,      ERROR,   DISCONNECTED},
  {SEND_PUBLIC_KEY_TO_PEER,      REPEAT,  SEND_PUBLIC_KEY_TO_PEER},

  {GEN_SHARED_SECRET,            OK,      CONNECTED},
  {GEN_SHARED_SECRET,            REPEAT,  GEN_SHARED_SECRET},
  {GEN_SHARED_SECRET,            ERROR,   DISCONNECTED},

  {CONNECTED,                    STOP,    STOP_COMMUNICATION},
  {CONNECTED,                    SEND,    SEND_ENCRYPTED_DATA},
  {CONNECTED,                    RECEIVE, RECEIVE_ENCRYPTED_DATA},

  {SEND_ENCRYPTED_DATA,          OK,      CONNECTED},
  {SEND_ENCRYPTED_DATA,          ERROR,   CONNECTED},

  {RECEIVE_ENCRYPTED_DATA,       OK,      CONNECTED},
  {RECEIVE_ENCRYPTED_DATA,       ERROR,   CONNECTED},

  {STOP_COMMUNICATION,           OK,      DISCONNECTED},
  {STOP_COMMUNICATION,           ERROR,   DISCONNECTED}
};
size_t client_transitions_amount = sizeof(client_state_transitions) / sizeof(transition_t);

transition_t server_state_transitions[] = {
  {DISCONNECTED,                 REPEAT,  DISCONNECTED},
  {DISCONNECTED,                 ERROR,   DISCONNECTED},
  {DISCONNECTED,                 OK,      START_CONNECTION},

  {START_CONNECTION,             ERROR,   DISCONNECTED},
  {START_CONNECTION,             OK,      SEND_PUBLIC_KEY_TO_PEER},

  {SEND_PUBLIC_KEY_TO_PEER,      OK,      RECEIVE_PUBLIC_KEY_FROM_PEER},

  {RECEIVE_PUBLIC_KEY_FROM_PEER, REPEAT,  RECEIVE_PUBLIC_KEY_FROM_PEER},
  {RECEIVE_PUBLIC_KEY_FROM_PEER, ERROR,   DISCONNECTED},
  {RECEIVE_PUBLIC_KEY_FROM_PEER, OK,      GEN_SHARED_SECRET},

  {GEN_SHARED_SECRET,            OK,      CONNECTED},
  {GEN_SHARED_SECRET,            REPEAT,  GEN_SHARED_SECRET},
  {GEN_SHARED_SECRET,            ERROR,   DISCONNECTED},

  {CONNECTED,                    STOP,    STOP_COMMUNICATION},
  {CONNECTED,                    SEND,    SEND_ENCRYPTED_DATA},
  {CONNECTED,                    RECEIVE, RECEIVE_ENCRYPTED_DATA},

  {SEND_ENCRYPTED_DATA,          OK,      CONNECTED},
  {SEND_ENCRYPTED_DATA,          ERROR,   CONNECTED},

  {RECEIVE_ENCRYPTED_DATA,       STOP,    START_CONNECTION},
  {RECEIVE_ENCRYPTED_DATA,       OK,      CONNECTED},
  {RECEIVE_ENCRYPTED_DATA,       ERROR,   CONNECTED},

  {STOP_COMMUNICATION,           OK,      DISCONNECTED},
  {STOP_COMMUNICATION,           ERROR,   DISCONNECTED}
};
size_t server_transitions_amount = sizeof(server_state_transitions) / sizeof(transition_t);

int configure_serial_port(state_attr_t *sm_attr) {
  struct termios oldtio, newtio;
  int fd = -1;

  // Ouverture d'un port série
  fd = open(sm_attr->device_name, O_RDWR | O_NOCTTY | O_NDELAY);
  
  if (fd == -1) {
    PRINT_DEBUG("Impossible d'ouvrir le port serie demande");
    PRINT_DEBUG_EXIT_FCT();
    return fd;
  } else {
    PRINT_DEBUG("port successfully opened");
    fcntl(fd, F_SETFL, 0);
  }

  tcgetattr(fd,&oldtio); /* save current port settings */

  memset(&newtio, '\0', sizeof(newtio));
  newtio.c_cflag = BAUDRATE | CS8 | CLOCAL | CREAD;
  newtio.c_iflag = IGNPAR;
  newtio.c_oflag = 0;

  /* set input mode (non-canonical, no echo,...) */
  newtio.c_lflag = 0;

  newtio.c_cc[VTIME]    = 5;   /* inter-character timer = 5 * 0.1s */
  newtio.c_cc[VMIN]     = 5;   /* blocking read until 5 chars received */

  tcflush(fd, TCIFLUSH);
  tcsetattr(fd,TCSANOW,&newtio);

  PRINT_DEBUG("configuration du port serie termine");
  return fd;
}

// QUESTION: A déplacer dans la partie crypto_primitives ?
EVP_PKEY* get_dh_params(char *dh_filename) {
  EVP_PKEY* params = NULL;

  if(access(dh_filename, F_OK) == -1) {
    if (CRYPTO_RETURN_ERROR == generate_dh_parameters(&params)) {
      PRINT_DEBUG("Il y a eu une erreur lors de la génération des paramètres de diffie hellman");
    } else {
      if (CRYPTO_RETURN_ERROR == write_dhparams_to_file(dh_filename, params)) {
        PRINT_DEBUG("Impossible d'écrire les paramètres diffie hellman dans le fichier fourni");
      }
    }
  } else {
    if (CRYPTO_RETURN_ERROR == read_dhparams_from_file(dh_filename, &params)) {
      PRINT_DEBUG("Impossible de lire les paramètres diffie hellman depuis le fichier fourni");
    }
  }

  return params;
}

size_t count_char_in_file(FILE *fp) {
  signed char c;
  size_t char_count = 0;

  for (c = getc(fp); c != EOF; c = getc(fp))
    char_count++;
  rewind(fp);

  return char_count;
}

// TODO: Pourquoi pas mettre dans utils.c
STATE_MACHINE_RETURN_TYPE read_file_content(const char *filename, unsigned char **content) {
  FILE *fp = NULL;
  size_t char_amount = 0;

  if (!filename)
    RAISE_ERROR(STATE_MACHINE_ERROR, "Vous devez spécifier un nom de fichier valide");

  if (-1 == access(filename, F_OK))
    RAISE_ERROR(STATE_MACHINE_ERROR, "Le fichier spécifié n'existe pas");

  fp = fopen(filename, "r"); 
  if (NULL == fp)
    RAISE_ERROR(STATE_MACHINE_ERROR, "Impossible d'ouvrir le fichier demandé");

  char_amount = count_char_in_file(fp);

  *content = calloc(char_amount + 1, sizeof(unsigned char));
  if (NULL == *content)
    RAISE_ERROR(STATE_MACHINE_ERROR, "Impossible d'allouer le buffer pour lire le fichier");

  // les deux 1 suivants sont le nombre de bloc qui seront (ou ont été) lu dans le fichier
  if (1 != fread(*content, char_amount, 1, fp))
    RAISE_ERROR(STATE_MACHINE_ERROR, "Impossible de lire le contenu du fichier");

  // Close the file
  if (-1 == fclose(fp))
    RAISE_ERROR(STATE_MACHINE_ERROR, "Erreur lors de la fermeture du fichier...");

  return STATE_MACHINE_OK;
}

STATE_MACHINE_RETURN_TYPE reset_state_attributes(state_attr_t *sm_attr) {
  /**
   * NOTE: On ne touhce pas aux dh_params, au device_name ni au serial_fd,
   *       car ce sont des données qui servent à nouveau suite à la 
   *       fermeture de connexion d'un client.
   */

  RESET_POINTER(sm_attr->local_key, EVP_PKEY_free);
  RESET_POINTER(sm_attr->peer_key, EVP_PKEY_free);

  if (ERASE_MEMORY_FOR_REUSE && sm_attr->secret)
    memset(sm_attr->secret, '\0', sm_attr->secret_len);
  RESET_POINTER(sm_attr->secret, free);
  sm_attr->secret_len = 0;

  return STATE_MACHINE_OK;
}

STATE_MACHINE_RETURN_TYPE init_state_attributes(state_attr_t *sm_attr) {
  memset(sm_attr->device_name, '\0', DEVICE_NAME_LENGTH);
  strncpy(sm_attr->private_key_filename, PRIVATE_KEY_FILENAME, PEM_FILENAME_LENGTH);
  strncpy(sm_attr->peer_sign_pubkey_filename, PEER_PUBLIC_KEY_FILENAME, PEM_FILENAME_LENGTH);
  strncpy(sm_attr->dh_parameters_filename, DH_PARAMS_FILENAME, PEM_FILENAME_LENGTH);
  sm_attr->serial_fd = -1;
  sm_attr->local_key = NULL;
  sm_attr->dh_params = NULL;
  sm_attr->peer_key = NULL;
  sm_attr->secret = NULL;
  sm_attr->secret_len = 0;

  return STATE_MACHINE_OK;
}

STATE_MACHINE_RETURN_TYPE free_state_attributes(state_attr_t *sm_attr) {
  RESET_POINTER(sm_attr->dh_params, EVP_PKEY_free);
  reset_state_attributes(sm_attr);
  close(sm_attr->serial_fd);
  
  return init_state_attributes(sm_attr);
}

state_t lookup_transitions(state_machine_type_t sm_type, state_t current_state, transition_code_t return_code)
{
  PRINT_DEBUG_ENTER_FCT();
  transition_t *state_transitions;
  size_t transitions_amount = 0;
  transition_t transition;
  size_t i = 0;

  switch (sm_type)
  {
    case SERVER_SM:
      PRINT_DEBUG("On effectue le lookup sur la machine à état du serveur");
      state_transitions = server_state_transitions;
      transitions_amount = server_transitions_amount;
      break;
    case CLIENT_SM:
      PRINT_DEBUG("On effectue le lookup sur la machine à état du client");
      state_transitions = client_state_transitions;
      transitions_amount = client_transitions_amount;
      break;
  }

  PRINT_DEBUG("Recherche de l'état suivant");
  for (i = 0; i < transitions_amount; i++) {
    transition = state_transitions[i];

    if (current_state == transition.from && return_code == transition.ret_code) {
      PRINT_DEBUG_EXIT_FCT();
      return transition.to;
    }
  }

  // TODO: Trouver le moyen de faire proprement un état "LOOKUP_ERROR"
  PRINT_DEBUG("aucun état suivant n'a été trouvé");
  PRINT_DEBUG_EXIT_FCT();
  return DISCONNECTED;
}

transition_code_t disconnected_state(state_attr_t *sm_attr)
{
  PRINT_DEBUG_ENTER_FCT();

  int flag = fcntl(STDIN_FILENO, F_GETFL, 0);
  flag |= O_NONBLOCK;
  fcntl(STDIN_FILENO, F_SETFL, flag);

  PRINT_DEBUG("Récupération de la clé privée");
  if (STATE_MACHINE_ERROR == read_file_content(sm_attr->private_key_filename, &sm_attr->private_key))
    RAISE_ERROR(ERROR, "Impossible de récupérer la clé privée de la machine");

  PRINT_DEBUG("Récupération de la clé publique");
  if (STATE_MACHINE_ERROR == read_file_content(sm_attr->peer_sign_pubkey_filename, &sm_attr->peer_sign_pubkey))
    RAISE_ERROR(ERROR, "Impossible de récupérer la clé privée de la machine");

  PRINT_DEBUG("Génération des paramètres pour le diffie hellman");
  sm_attr->dh_params = get_dh_params(sm_attr->dh_parameters_filename);
  if (sm_attr->dh_params == NULL) return ERROR;
  
  sm_attr->serial_fd = configure_serial_port(sm_attr);
  if (sm_attr->serial_fd == -1) return ERROR;

  PRINT_DEBUG_EXIT_FCT();
  return OK;
}

transition_code_t server_start_connection_state(state_attr_t *sm_attr)
{
  // NOTE: En fait on pourrais plutot appeler cet état "wait_for_connection"
  packet_t hello_message;
  PRINT_DEBUG_ENTER_FCT();

  if (PROTOCOL_ERROR == receive_packet(sm_attr->serial_fd, sm_attr->secret, sm_attr->peer_sign_pubkey, &hello_message)) {
    PRINT_DEBUG("Une erreur lors de la reception du paquet s'est produite");
    PRINT_DEBUG_EXIT_FCT();
    return ERROR;
  }

  PRINT_DEBUG_EXIT_FCT();
  return OK;
}

transition_code_t client_start_connection_state(state_attr_t *sm_attr)
{
  packet_t hello_message;
  PRINT_DEBUG_ENTER_FCT();

  sm_attr->serial_fd = configure_serial_port(sm_attr);
  if (sm_attr->serial_fd == -1) return ERROR;

  PRINT_DEBUG("Création du message CLIENT_HELLO");
  if (PROTOCOL_ERROR == craft_client_hello(&hello_message, sm_attr->private_key)) {
    PRINT_DEBUG("Problème lors de la construction du paquet CLIENT_HELLO");
    PRINT_DEBUG_EXIT_FCT();
    return ERROR;
  }

  memcpy(sm_attr->session_id, hello_message.body.client_hello.session_id, SESSION_ID_DIGITS);
  
  PRINT_DEBUG("Envoie du paquet");
  if (PROTOCOL_ERROR == send_packet(sm_attr->serial_fd, &hello_message)) {
    PRINT_DEBUG("Problème lors de l'envoi du paquet de bonjour");
    PRINT_DEBUG_EXIT_FCT();
    return ERROR;
  }

  PRINT_DEBUG_EXIT_FCT();
  return OK;
}

transition_code_t receive_public_key_from_peer_state(state_attr_t *sm_attr)
{
  PRINT_DEBUG_ENTER_FCT();
  packet_t message;

  if (PROTOCOL_ERROR == receive_packet(sm_attr->serial_fd, sm_attr->secret, sm_attr->peer_sign_pubkey, &message)) {
    PRINT_DEBUG("Erreur lors de la réception d'un paquet");
    PRINT_DEBUG_EXIT_FCT();
    return ERROR;
  }

  if (PUBLIC_KEY_DH == message.header.message_type) {
    PRINT_DEBUG(message.body.pkey_dh.public_key);
    if (CRYPTO_RETURN_ERROR == convert_to_evp_pkey(&sm_attr->peer_key, message.body.pkey_dh.public_key, message.body.pkey_dh.public_key_len)) {
      PRINT_DEBUG("Le paquet contient une clé publique erronée");
      return ERROR;
    }
  } else {
    PRINT_DEBUG("Le paquet reçu n'est pas du bon type");
    return ERROR;
  }

  PRINT_DEBUG_EXIT_FCT();
  return OK;
}

transition_code_t send_public_key_to_peer_state(state_attr_t *sm_attr)
{
  PRINT_DEBUG_ENTER_FCT();
  packet_t message;

  // TODO: Vérifier si il y a besoin d'envoyer les paramètres diffie hellman !!!
  PRINT_DEBUG("Génération des clés asymétriques pour l'échange Diffie Hellman");
  if (CRYPTO_RETURN_ERROR == generate_keys(sm_attr->dh_params, &(sm_attr->local_key))) {
    PRINT_DEBUG("Error while generating the key");
    PRINT_DEBUG_EXIT_FCT();
    return ERROR;
  }
  
  PRINT_DEBUG("Création du PUBLIC_KEY8DH qui sera envoyé à l'appareil distant");
  if (PROTOCOL_ERROR == craft_public_key_dh(&message, sm_attr->local_key, sm_attr->private_key)) {
    PRINT_DEBUG("Impossible de forger le paquet PUBLIC_KEY_DH pour l'envoie de la clé local");
    PRINT_DEBUG_EXIT_FCT();
    return ERROR;
  }

  if (PROTOCOL_ERROR == send_packet(sm_attr->serial_fd, &message)) {
    PRINT_DEBUG("Impossible d'envoyer le paquet PUBLIC_KEY_DH sur le port série'");
    PRINT_DEBUG_EXIT_FCT();
    return ERROR;
  }

  PRINT_DEBUG_EXIT_FCT();
  return OK;
}

transition_code_t gen_shared_secret_state(state_attr_t *sm_attr) {

  PRINT_DEBUG("Génération de la clé partagée");
  if(CRYPTO_RETURN_ERROR == generate_shared_secret(sm_attr->local_key, sm_attr->peer_key, &sm_attr->secret, &sm_attr->secret_len)) {
    PRINT_DEBUG("Erreur lors de la génération du secret partagé");
    return ERROR;
  }

  return OK;
}

transition_code_t connected_state(state_attr_t *sm_attr)
{
  PRINT_DEBUG_ENTER_FCT();
  int activity;
  fd_set read_fds;

  FD_ZERO(&read_fds);
  FD_SET(STDIN_FILENO, &read_fds);
  FD_SET(sm_attr->serial_fd, &read_fds);

  PRINT_DEBUG("En attente d'acitivité au niveau du select");
  printf("Tapez ce que vous voulez envoyer : \n");
  activity = select(sm_attr->serial_fd + 1, &read_fds, NULL, NULL, NULL);
  if (-1 == activity) {
    PRINT_DEBUG("Erreur lors du select");
    perror("select");
    return REPEAT;
  }

  if (FD_ISSET(STDIN_FILENO, &read_fds)) {
    PRINT_DEBUG("received something on stdin");
    read_from_stdin(sm_attr->stdin_datas, STDIN_DATAS_BUFFER_LEGTH);
    printf("message = %s\n", sm_attr->stdin_datas);
    PRINT_DEBUG_EXIT_FCT();
    return SEND;
  }

  if (FD_ISSET(sm_attr->serial_fd, &read_fds)) {
    PRINT_DEBUG("On a reçu quelque chose, on le traite");
    PRINT_DEBUG_EXIT_FCT();
    return RECEIVE;
  }

  PRINT_DEBUG_EXIT_FCT();
  return REPEAT;
}

transition_code_t send_encrypted_data_state(state_attr_t *sm_attr)
{
  PRINT_DEBUG_ENTER_FCT();
  packet_t message;

  if (PROTOCOL_ERROR == craft_data(&message, sm_attr->secret, (unsigned char *) sm_attr->stdin_datas)) {
    PRINT_DEBUG("Impossible de construire le paquet à envoyer");
    return ERROR;
  }

  if (PROTOCOL_ERROR == send_packet(sm_attr->serial_fd, &message)) {
    PRINT_DEBUG("Impossible d'envoyer le paquet");
    return ERROR;
  }

  PRINT_DEBUG_EXIT_FCT();
  return OK;
}

/* NOTE: Ces états devraient juste s'appeler receive_packet_state
 * (les données sont pas forcément chiffré, surtout dans le cas de END_COMMUNICATION)
 */
transition_code_t receive_encrypted_data_state(state_attr_t *sm_attr)
{
  PRINT_DEBUG_ENTER_FCT();
  packet_t message;

  if (PROTOCOL_OK != receive_packet(sm_attr->serial_fd, sm_attr->secret, sm_attr->peer_sign_pubkey, &message)) {
    PRINT_DEBUG("Impossible d'envoyer le paquet");
    return ERROR;
  }

  if (DATA == message.header.message_type) {
    printf("Le message reçu est le suivant: %s\n", message.body.datas.datas);
  } else if (END_COMMUNICATION == message.header.message_type) {
    printf("Le serveur va se stopper\n");
    reset_state_attributes(sm_attr);
    PRINT_DEBUG_EXIT_FCT();
    return STOP;
  }

  PRINT_DEBUG_EXIT_FCT();
  return OK;
}

transition_code_t stop_communication_state(state_attr_t *sm_attr) {
  packet_t message;

  if (PROTOCOL_ERROR == craft_end_communication(&message, sm_attr->session_id, sm_attr->private_key)) {
    PRINT_DEBUG("Impossible de construire le paquet à envoyer");
    return ERROR;
  }

  if (PROTOCOL_ERROR == send_packet(sm_attr->serial_fd, &message)) {
    PRINT_DEBUG("Impossible d'envoyer le paquet");
    return ERROR;
  }

  return OK;
}