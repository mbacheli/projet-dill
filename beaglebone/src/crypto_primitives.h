#ifndef __CRYPTO_PRIMITIVES_H__
#define __CRYPTO_PRIMITIVES_H__

#define SESSION_ID_DIGITS   32
#define IV_SIZE             12
#define TAG_SIZE            16
#define ASYM_KEY_BITS_LEN   2048
#define ASYM_KEY_LEN        (ASYM_KEY_BITS_LEN / 8)

#define CRYPTO_RETURN_TYPE  size_t
#define CRYPTO_RETURN_OK    1
#define CRYPTO_RETURN_ERROR 0

#include <openssl/evp.h>

// Fonctions utilitaires (ne respectent pas le conformisme du reste de la lib)

size_t determine_cipher_size(size_t plaintext_size);


// Fonctions de la lib

CRYPTO_RETURN_TYPE generate_random_bytes(unsigned char *bytes, size_t length);
extern CRYPTO_RETURN_TYPE generate_session_id(unsigned char *session_id);
extern CRYPTO_RETURN_TYPE generate_aes_iv(unsigned char *iv);

CRYPTO_RETURN_TYPE encrypt(
    const unsigned char *plaintext, int plaintext_len,
    const unsigned char *aad, int aad_len,
    const unsigned char *iv, int iv_len,
    const unsigned char *key,
    unsigned char *ciphertext, size_t *ciphertext_len,
    unsigned char *tag
);
            
CRYPTO_RETURN_TYPE decrypt(
    const unsigned char *ciphertext, int ciphertext_len, 
    const unsigned char *aad, int aad_len, 
    const unsigned char *tag,
    const unsigned char *key,
    const unsigned char *iv, int iv_len,
    unsigned char *plaintext, size_t *plaintext_len
);

CRYPTO_RETURN_TYPE hash(const unsigned char *text, int text_len, unsigned char *hash, unsigned int *hash_len);

CRYPTO_RETURN_TYPE sign(const unsigned char* message, size_t message_len, unsigned char **signature, size_t *signed_len, const unsigned char* private_key);
CRYPTO_RETURN_TYPE verify(const unsigned char* message, size_t message_len, const unsigned char *signature, size_t signed_len, const unsigned char* public_key);

CRYPTO_RETURN_TYPE generate_dh_parameters(EVP_PKEY **params);
CRYPTO_RETURN_TYPE generate_keys(EVP_PKEY *params, EVP_PKEY **pkey);
CRYPTO_RETURN_TYPE generate_shared_secret(EVP_PKEY* local_key, EVP_PKEY* peer_key, unsigned char **secret, size_t* secret_len);

CRYPTO_RETURN_TYPE write_dhparams_to_file(const char* filename, EVP_PKEY* dhparam);
CRYPTO_RETURN_TYPE read_dhparams_from_file(const char* filename, EVP_PKEY **dh_params);

CRYPTO_RETURN_TYPE convert_from_evp_pkey(EVP_PKEY* pkey, unsigned char* converted, size_t *max_len);
CRYPTO_RETURN_TYPE convert_to_evp_pkey(EVP_PKEY **dest, unsigned char *source, size_t key_size);

#endif // __CRYPTO_PRIMITIVES_H__