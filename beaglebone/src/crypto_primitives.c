#include <openssl/crypto.h>
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/bio.h>
#include <openssl/pem.h>
#include <openssl/rsa.h>
#include <openssl/rand.h>
#include <openssl/err.h>
#include <stdbool.h>

#include "utils.h"
#include "crypto_primitives.h"

/**
 * @brief Fichier rassemblant les primitives de chiffrement de notre application
 * 
 * Pour activer le debug dans ce fichier, ajouter la variable DEBUG à la compilation.
 */

/**
 * @brief Fonction permettant de déterminer la taille d'un chiffré à partir du plaintext
 */
size_t determine_cipher_size(size_t plaintext_size) {
    // int block_size = EVP_CIPHER_block_size(EVP_aes_256_gcm());
    // return ((plaintext_size + block_size) / block_size) * block_size;
    // dans le cas de GCM c'est la meme taille: https://crypto.stackexchange.com/a/26787
    return plaintext_size;
}

CRYPTO_RETURN_TYPE generate_random_bytes(unsigned char *bytes, size_t length) {
    // 0 = false, 1= true
    static int is_random_seeded = 0;
    
    if (!bytes)
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Vous devez allouer l'espace nécessaire");

    if (!is_random_seeded) {
        PRINT_DEBUG("RAND_bytes n'était pas initialisé... initialisation");
        is_random_seeded = RAND_poll();
    }

    if (0 >= RAND_bytes(bytes, length))
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible de générer les octets aléatoire");

    return CRYPTO_RETURN_OK;
}

/**
 * @brief génère le session ID avec le random de OPENSSL
 * 
 * @param session_id tableau de 32 octets minimum
 * @return int -1 si erreur, 0 sinon
 */
inline CRYPTO_RETURN_TYPE generate_session_id(unsigned char *session_id)
{
    PRINT_DEBUG("Génération aléatoire de l'ID de session");
    return generate_random_bytes(session_id, SESSION_ID_DIGITS);
}

inline CRYPTO_RETURN_TYPE generate_aes_iv(unsigned char *iv) {
    PRINT_DEBUG("Génération aléatoire du vecteur d'initialisation");
    return generate_random_bytes(iv, IV_SIZE);
}

void handle_errors(void) {
    printf("error\n");
}

CRYPTO_RETURN_TYPE encrypt(const unsigned char *plaintext, int plaintext_len,
                           const unsigned char *aad, int aad_len,
                           const unsigned char *iv, int iv_len,
                           const unsigned char *key,
                           unsigned char *ciphertext, size_t *ciphertext_len,
                           unsigned char *tag)
{
    EVP_CIPHER_CTX *ctx;
    int len;

    if(!(ctx = EVP_CIPHER_CTX_new()))
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible d'allouer l'espace pour le contexte cryptographique");

    PRINT_DEBUG("Initialise the encryption operation");
    if(1 != EVP_EncryptInit_ex(ctx, EVP_aes_256_gcm(), NULL, NULL, NULL))
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible d'initialiser le contexte");

    PRINT_DEBUG("Setting IV length");
    if(1 != EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_AEAD_SET_IVLEN, iv_len, NULL))
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible de configurer la taille du vecteur d'initialisation");

    /*
     * IMPORTANT - ensure you use a key and IV size appropriate for your cipher
     * In this example we are using 256 bit AES (i.e. a 256 bit key). The
     * IV size for *most* modes is the same as the block size. For AES this
     * is 128 bits
     */
    PRINT_DEBUG("Initialising key and iv");
    if(1 != EVP_EncryptInit_ex(ctx, NULL, NULL, key, iv))
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Initialisation du système de chiffrement");

    PRINT_DEBUG("Provide aad datas");
    if(1 != EVP_EncryptUpdate(ctx, NULL, &len, aad, aad_len))
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible de renseigner les AAD au système de chiffrement");
    
    PRINT_DEBUG("Provide the message to be encrypted");
    if(1 != EVP_EncryptUpdate(ctx, ciphertext, &len, plaintext, plaintext_len))
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible de renseigner le message au système de chiffrement");
    *ciphertext_len = len;

    PRINT_DEBUG("Finalise the encryption");
    if(1 != EVP_EncryptFinal_ex(ctx, ciphertext + len, &len))
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible de chiffrer le message");
    *ciphertext_len += len;
    
    PRINT_DEBUG("Get the tag");
    if(1 != EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_GET_TAG, 16, tag))
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible de récupérer le TAG pour authentifier le message");

    EVP_CIPHER_CTX_free(ctx);
    return CRYPTO_RETURN_OK;
}

CRYPTO_RETURN_TYPE decrypt(const unsigned char *ciphertext, int ciphertext_len, 
                           const unsigned char *aad, int aad_len, 
                           const unsigned char *tag,
                           const unsigned char *key,
                           const unsigned char *iv, int iv_len,
                           unsigned char *plaintext, size_t *plaintext_len)
{
    EVP_CIPHER_CTX *ctx;
    int len;

    PRINT_DEBUG("Create and initialise the context");
    if(!(ctx = EVP_CIPHER_CTX_new()))
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible d'allouer l'espace pour le contexte cryptographique");

    PRINT_DEBUG("Initialise the decryption operation");
    if(1 != EVP_DecryptInit_ex(ctx, EVP_aes_256_gcm(), NULL, NULL, NULL))
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible d'initialiser le contexte");

    PRINT_DEBUG("Set IV length");
    if(!EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_AEAD_SET_IVLEN, iv_len, NULL))
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible de configurer la taille du vecteur d'initialisation");

    PRINT_DEBUG("Initialise key and IV");
    if(!EVP_DecryptInit_ex(ctx, NULL, NULL, key, iv))
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Initialisation du système de déchiffrement");

    PRINT_DEBUG("Provide any AAD data");
    if(!EVP_DecryptUpdate(ctx, NULL, &len, aad, aad_len))
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible de renseigner les AAD au système de déchiffrement");

    PRINT_DEBUG("Provide the message to be decrypted");
    if(1 != EVP_DecryptUpdate(ctx, plaintext, &len, ciphertext, ciphertext_len))
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible de renseigner le message au système de déchiffrement");
    *plaintext_len = len;

    /* Set expected tag value. Works in OpenSSL 1.0.1d and later */
    PRINT_DEBUG("Settings expected tag value");
    if(!EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_TAG, 16, (unsigned char*) tag))
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible de renseigner le TAG attendu pour l'authentification du message");

    PRINT_DEBUG("Finalise the decryption");
    if (1 != EVP_DecryptFinal_ex(ctx, plaintext + len, &len))
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible de déchiffrer le message");
    *plaintext_len += len;

    EVP_CIPHER_CTX_free(ctx);
    return CRYPTO_RETURN_OK;
}

CRYPTO_RETURN_TYPE hash(const unsigned char *text, int text_len, unsigned char *hash, unsigned int *hash_len)
{
    // QUESTION: Elle sera utilisée un jour cette fonction ??
    EVP_MD_CTX* context = NULL;

    context = EVP_MD_CTX_new();
    if(context == NULL)
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible d'allouer le contexte pour le hash");
    
    if(1 != EVP_DigestInit_ex(context, EVP_sha256(), NULL)) {
        EVP_MD_CTX_free(context);
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible d'initialiser le contexte pour le hash");
    }

    if(1 != EVP_DigestUpdate(context, text, text_len)) {
        EVP_MD_CTX_free(context);
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible d'ajouter le text à hasher au contexte");
    }

    if(1 != EVP_DigestFinal_ex(context, hash, hash_len)) {
        EVP_MD_CTX_free(context);
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible de finaliser le hashage");
    }

    EVP_MD_CTX_free(context);
    return CRYPTO_RETURN_OK;
}

static CRYPTO_RETURN_TYPE create_RSA(const unsigned char *key, RSA **rsa, bool is_private) {
    PRINT_DEBUG_ENTER_FCT();
    BIO *keybio = NULL;
    *rsa = NULL;

    if (NULL == key)
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Vous devez passer une clé valide");

    keybio = BIO_new_mem_buf((void*) key, -1);
    if (NULL == keybio)
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible de créer le buffer BIO");

    if (is_private) {
        PRINT_DEBUG("Converting private key with PEM module");
        PEM_read_bio_RSAPrivateKey(keybio, rsa, NULL, NULL);
    } else {
        PRINT_DEBUG("Converting public key with PEM module");
        PEM_read_bio_RSA_PUBKEY(keybio, rsa, NULL, NULL);
    }

    if (NULL == *rsa)
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "La convertion en clé RSA à échoué");

    PRINT_DEBUG("Key successfully converted !");
    PRINT_DEBUG_EXIT_FCT();
    return CRYPTO_RETURN_OK;
}

#define FREE_AND_QUIT_SIGN()                           \
    do {                                               \
        if (rsa_priv_key) RSA_free(rsa_priv_key);      \
        if (priv_key) EVP_PKEY_free(priv_key);         \
        if (mdctx) EVP_MD_CTX_free(mdctx);             \
        /* free(signature) -> pas de fuite mémoire */  \
        if (*signature) OPENSSL_free(*signature);      \
        return CRYPTO_RETURN_ERROR;                    \
    } while (0)

/* IMPORTANT: Vous devez appeler OPENSSL_free pour 'signature' */
CRYPTO_RETURN_TYPE sign(const unsigned char* message, size_t message_len, unsigned char **signature, size_t *signed_len, const unsigned char* private_key)
{
    PRINT_DEBUG("Entering sign function");
    EVP_MD_CTX *mdctx = EVP_MD_CTX_new();
    // NOTE: Pas besoin de EVP_PKEY_free, ça sera fait par EVP_MD_CTX_free
    // car priv_key est installé dans le context lors du sign_init.
    EVP_PKEY* priv_key  = EVP_PKEY_new();
    RSA* rsa_priv_key = NULL;

    *signature = NULL;
 
    PRINT_DEBUG("Create the Message Digest Context");
    if(!mdctx || !priv_key)
        FREE_AND_QUIT_SIGN();
    
    // TODO: A changer pour ECC ou à rendre générique.
    PRINT_DEBUG("Creating RSA structure to sign the message");
    if (CRYPTO_RETURN_ERROR == create_RSA(private_key, &rsa_priv_key, true))
        FREE_AND_QUIT_SIGN();

    PRINT_DEBUG("Assigning the previously created structure to sign the message");
    if (1 != EVP_PKEY_assign_RSA(priv_key, rsa_priv_key))
        FREE_AND_QUIT_SIGN();

    PRINT_DEBUG("Initialise the DigestSign operation with SHA256");
    if(1 != EVP_DigestSignInit(mdctx, NULL, EVP_sha256(), NULL, priv_key))
        FREE_AND_QUIT_SIGN();

    PRINT_DEBUG("Call update with the message");
    if(1 != EVP_DigestSignUpdate(mdctx, message, message_len))
        FREE_AND_QUIT_SIGN();
    
    PRINT_DEBUG("Finalise the DigestSign operation");
    /* First call EVP_DigestSignFinal with a NULL sig parameter to obtain the length of the
       signature. Length is returned in signed_len */
    if(1 != EVP_DigestSignFinal(mdctx, NULL, signed_len))
        FREE_AND_QUIT_SIGN();

    PRINT_DEBUG("Allocate memory for the signature based on size in slen");
    if(!(*signature = OPENSSL_malloc(sizeof(unsigned char) * (*signed_len))))
        FREE_AND_QUIT_SIGN();

    PRINT_DEBUG("Obtain the signature");
    if(1 != EVP_DigestSignFinal(mdctx, *signature, signed_len))
        FREE_AND_QUIT_SIGN();

    PRINT_DEBUG("Clean up");
    EVP_MD_CTX_free(mdctx);
    RSA_free(rsa_priv_key);
    PRINT_DEBUG("Exiting sign function");
    return CRYPTO_RETURN_OK;
}

#define FREE_AND_QUIT_VERIFY()                  \
    do {                                        \
        /* if (rsa_pub_key) RSA_free(rsa_pub_key); */\
        if (pub_key) EVP_PKEY_free(pub_key);    \
        if (mdctx) EVP_MD_CTX_free(mdctx);      \
        return CRYPTO_RETURN_ERROR;             \
    } while (0)

CRYPTO_RETURN_TYPE verify(const unsigned char* message, size_t message_len, const unsigned char *signature, size_t signed_len, const unsigned char* public_key)
{
    EVP_MD_CTX *mdctx = EVP_MD_CTX_new();
    // NOTE: Pas besoin de EVP_PKEY_free, ça sera fait par EVP_MD_CTX_free
    // car pub_key est installé dans le context lors du sign_init.
    EVP_PKEY *pub_key  = EVP_PKEY_new();
    RSA *rsa_pub_key = NULL;

    if(!mdctx || !pub_key)
        FREE_AND_QUIT_VERIFY();

    // TODO: A changer pour ECC ou à rendre générique.
    PRINT_DEBUG("Creating RSA structure to sign the message");
    if (CRYPTO_RETURN_ERROR == create_RSA(public_key, &rsa_pub_key, false))
        FREE_AND_QUIT_VERIFY();

    PRINT_DEBUG("Assigning the previously created structure to sign the message");
    if (1 != EVP_PKEY_assign_RSA(pub_key, rsa_pub_key))
        FREE_AND_QUIT_VERIFY();

    PRINT_DEBUG("Initialize `key` with a public key");
    if(1 != EVP_DigestVerifyInit(mdctx, NULL, EVP_sha256(), NULL, pub_key))
        FREE_AND_QUIT_VERIFY();

    PRINT_DEBUG("Initialize `key` with a public key");
    if(1 != EVP_DigestVerifyUpdate(mdctx, message, message_len))
        FREE_AND_QUIT_VERIFY();

    PRINT_DEBUG("Final check");
    if(1 == EVP_DigestVerifyFinal(mdctx, signature, signed_len)) {
        PRINT_DEBUG("Cleanup");
        EVP_MD_CTX_free(mdctx);
        RSA_free(rsa_pub_key);
        return CRYPTO_RETURN_OK;
    } else {
        // TODO: Changer PRINT_DEBUG en PRINT_DEBUG_MSG et PRINT_DEBUG pour qu'il fasse un simple printf avec VA_ARG
#ifdef DEBUG
        printf("%s\n", ERR_error_string(ERR_get_error(), NULL));
#endif
        FREE_AND_QUIT_VERIFY();
    }
}

// Vous devrez utiliser EVP_PKEY_free pour libérer la clé, mais elle doit être libéré plus tard !
CRYPTO_RETURN_TYPE generate_dh_parameters(EVP_PKEY **params)
{
    EVP_PKEY_CTX *pctx;

    if (NULL != *params) {
        PRINT_DEBUG("Le paramètres params n'était pas vide, on libère pour éviter les fuites");
        EVP_PKEY_free(*params);
    }

    PRINT_DEBUG("Create the context for generating the parameters");
    pctx = EVP_PKEY_CTX_new_id(EVP_PKEY_DH, NULL);
    if(NULL == pctx)
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible de créer le contexte pour la génération des paramètres DH");

    PRINT_DEBUG("Initializing the paramgen context");
    if(1 != EVP_PKEY_paramgen_init(pctx)) {
        EVP_PKEY_CTX_free(pctx);
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible d'initialiser le contexte pour la génération des paramètres DH");
    }

    PRINT_DEBUG("Setting the length of RSA prime number to ASYM_KEY_BITS_LEN");
    if(0 >= EVP_PKEY_CTX_set_dh_paramgen_prime_len(pctx, ASYM_KEY_BITS_LEN)) {
        EVP_PKEY_CTX_free(pctx);
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible de fixer la taille de la clé RSA");
    }

    PRINT_DEBUG("Generating parameters");
    if (1 != EVP_PKEY_paramgen(pctx, params)) {
        EVP_PKEY_CTX_free(pctx);
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible de générer les paramètres Diffie Hellman");
    }

    EVP_PKEY_CTX_free(pctx);
    return CRYPTO_RETURN_OK;
}

// Pas besoin d'allouer pkey au préalable, mais il doit être libéré plus tard !
CRYPTO_RETURN_TYPE generate_keys(EVP_PKEY *params, EVP_PKEY **pkey)
{
    EVP_PKEY_CTX *kctx;

    if (NULL == params)
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Vous devez d'abord générer les paramètres DH");

    PRINT_DEBUG("Create the context for generating the keys");
    kctx = EVP_PKEY_CTX_new(params, NULL);
    if(NULL == kctx)
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible d'allouer le contexte");

    PRINT_DEBUG("Initializing the key generation context");
    if(1 != EVP_PKEY_keygen_init(kctx)) {
        EVP_PKEY_CTX_free(kctx);
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible d'initialiser le contexte");
    }

    // FIXME: Voir pourquoi ça retourne -1
    // PRINT_DEBUG("Setting the length of RSA keys to ASYM_KEY_BITS_LEN");
    // if(0 >= EVP_PKEY_CTX_set_rsa_keygen_bits(kctx, 4096)) {
    //     printf("%d\n", EVP_PKEY_CTX_set_rsa_keygen_bits(kctx, ASYM_KEY_BITS_LEN));
    //     printf("%s\n", ERR_error_string(ERR_get_error(), NULL));
    //     EVP_PKEY_CTX_free(kctx);
    //     RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible de fixer la taille de la clé RSA");
    // }

    PRINT_DEBUG("Generating keys");
    if (1 != EVP_PKEY_keygen(kctx, pkey)){
        EVP_PKEY_CTX_free(kctx);
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible de générer la clé RSA");
    }

    EVP_PKEY_CTX_free(kctx);
    return CRYPTO_RETURN_OK;
}

/*
 * NOTE: inspiratin : https://github.com/rustymagnet3000/OpenSSLKeyAgreementECDH/blob/master/magicPointers/keyGeneration.c
 * secret sera alloué par cette fonction, il faudra donc le free avec OPENSSL_free
 */
CRYPTO_RETURN_TYPE generate_shared_secret(EVP_PKEY* local_key, EVP_PKEY* peer_key, unsigned char **secret, size_t* secret_len) {
    EVP_PKEY_CTX* ctx;

    PRINT_DEBUG("Create the context for the shared secret derivation");
    ctx = EVP_PKEY_CTX_new(local_key, NULL);
    if(NULL == ctx)
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible d'allouer le contexte");
    
    PRINT_DEBUG("Initialise the derivation context");
    if(1 != EVP_PKEY_derive_init(ctx)) {
        EVP_PKEY_CTX_free(ctx);
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible de créer le contexte");
    }
    
    PRINT_DEBUG("Provide the peer public key");
    if(1 != EVP_PKEY_derive_set_peer(ctx, peer_key)) {
        EVP_PKEY_CTX_free(ctx);
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible d'ajouter la clé distante dans le contexte");
    }
    
    PRINT_DEBUG("Determine buffer length for shared secret");
    if(1 != EVP_PKEY_derive(ctx, NULL, secret_len)) {
        EVP_PKEY_CTX_free(ctx);
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible de déterminer la taille du buffer pour la clé partagée");
    }
    
    PRINT_DEBUG("Create the secret buffer");
    *secret = OPENSSL_malloc(sizeof(unsigned char) * (*secret_len));
    if (NULL == *secret) {
        EVP_PKEY_CTX_free(ctx);
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible d'allouer l'espace nécessaire au retour de la fonction");
    }
    
    PRINT_DEBUG("Derive the shared secret");
    if(1 != (EVP_PKEY_derive(ctx, *secret, secret_len))) {
        EVP_PKEY_CTX_free(ctx);
        OPENSSL_free(*secret);
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible de dériver la clé partagée");
    }

    EVP_PKEY_CTX_free(ctx);
    return CRYPTO_RETURN_OK;
}

CRYPTO_RETURN_TYPE write_dhparams_to_file(const char* filename, EVP_PKEY* dhparam)
{
    FILE* fp = NULL;
    DH *params = NULL;

    if (NULL == filename)
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Vous devez spécifier le chemin vers un fichier");

    if (NULL == dhparam)
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "La structure EVP_PKEY ne doit pas être NULL");

    fp = fopen(filename, "w");
    if (NULL == fp)
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible de lire le fichier demandé");

    params = EVP_PKEY_get0_DH(dhparam);
    if (NULL == params) {
        fclose(fp);
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible de convertir EVP_PKEY* en DH*");
    }

    if (1 != PEM_write_DHparams(fp, params)) {
        DH_free(params);
        fclose(fp);
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible d'écrire les paramètres DH dans le fichier");
    }
    
    DH_free(params);
    fclose(fp);

    return CRYPTO_RETURN_OK;
}

// NOTE: Il faudra free dhparams avec EVP_PKEY_free
CRYPTO_RETURN_TYPE read_dhparams_from_file(const char* filename, EVP_PKEY **dhparams)
{
    FILE* fp = NULL;
    DH *params = NULL;

    if (NULL == filename)
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Vous devez spécifier le chemin vers un fichier");

    fp = fopen(filename, "r");
    if (NULL == fp)
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible de lire le fichier demandé");

    params = PEM_read_DHparams(fp, NULL, 0, NULL);
    if (NULL == params) {
        fclose(fp);
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible de lire les paramètres depuis le fichier");
    }

    *dhparams = EVP_PKEY_new();
    if (NULL == *dhparams) {
        fclose(fp);
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible d'allouer la structure EVP_PKEY");
    }

    if (1 != EVP_PKEY_set1_DH(*dhparams, params)) {
        fclose(fp);
        DH_free(params);
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible de convertir la structure DH en EVP_PKEY");
    }

    DH_free(params);
    fclose(fp);

    return CRYPTO_RETURN_OK;
}

CRYPTO_RETURN_TYPE convert_from_evp_pkey(EVP_PKEY* pkey, unsigned char* converted, size_t* max_len) {
    BIO* bio_buffer = NULL;
    int bio_written = 0;
    size_t bio_len = 0;
    int bio_read_len = 0;

    // merci https://stackoverflow.com/a/56798741

    if (NULL == converted)
        PRINT_DEBUG("'converted' est à NULL, on place la taille de la pkey dans 'max_len'");
  
    bio_buffer = BIO_new(BIO_s_mem());
    if (NULL == bio_buffer)
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Error while allocating creating the BIO Buffer");

    PRINT_DEBUG("converting pubkey to BIO structure");
    bio_written = PEM_write_bio_PUBKEY(bio_buffer, pkey);
    if (0 == bio_written) {
        BIO_free(bio_buffer);
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "problem while converting pubkey to BIO structure");
    }

    /*
     * BIO_pending est censé retourner le nombre exacte de caractères qu'il reste à lire
     * c'est utile pour pouvoir allouer un buffer dynamiquement
     */
    bio_len = BIO_pending(bio_buffer);
    if (0 == bio_len) {
        BIO_free(bio_buffer);
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible de récupérer la taille de la structure BIO");
    }

    if(NULL == converted) {
        PRINT_DEBUG("on place la taille du buffer BIO dans le paramètres 'max_len'");
        *max_len = bio_len;
    } else {
        if (*max_len < bio_len) {
            BIO_free(bio_buffer);
            RAISE_ERROR(CRYPTO_RETURN_ERROR, "La taille du buffer passé en paramètre est trop faible");
        }

        bio_read_len = BIO_read(bio_buffer, converted, bio_len);
        if (-1 == bio_read_len || 0 == bio_read_len) {
            BIO_free(bio_buffer);
            RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible d'allouer l'espace pour transformer la structure BIO");
        } else if (-2 == bio_read_len) {
            BIO_free(bio_buffer);
            RAISE_ERROR(CRYPTO_RETURN_ERROR, "BIO_read n'est pas implémenté pour la structure BIO demandée");
        }
    }

    BIO_free(bio_buffer);
    return CRYPTO_RETURN_OK;
}

// NOTE: dest n'a pas besoin d'etre alloué, mais devra etre libéré avec EVP_PKEY_free
CRYPTO_RETURN_TYPE convert_to_evp_pkey(EVP_PKEY **dest, unsigned char *source, size_t key_size) {
    BIO* bio_buffer = NULL;
    int bio_written = 0;

    if (NULL == source)
        PRINT_DEBUG("'converted' est à NULL, aucune informations à prendre");
  
    bio_buffer = BIO_new(BIO_s_mem());
    if (NULL == bio_buffer)
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible d'allouer l'espace pour la structure BIO");

    bio_written = BIO_write(bio_buffer, source, key_size);
    if (0 >= bio_written) {
        BIO_free(bio_buffer);
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible de transformer la chaine de charactère source en structure BIO");
    }

    PRINT_DEBUG("Convertion du format BIO vers le format EVP_PKEY");
    if (NULL == PEM_read_bio_PUBKEY(bio_buffer, dest, NULL, NULL)) {
        BIO_free(bio_buffer);
        RAISE_ERROR(CRYPTO_RETURN_ERROR, "Impossible de convertir la structure BIO en EVP_PKEY");
    }

    BIO_free(bio_buffer);
    return CRYPTO_RETURN_OK;
}