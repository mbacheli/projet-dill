#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/resource.h>
#include <sys/select.h>
#include <sys/sysinfo.h>

#include "utils.h"
#include "benchmark.h"
#include "crypto_primitives.h"
#include "state_machine.h"
#include "protocol.h"

// Partie lié au calcul des différents temps moyens

#define TIME(code)                         \
    clock_t start, end;                    \
    start = clock();                       \
    for (size_t i = 0; i < NB_TEST; i++) { \
        code;                              \
    }                                      \
    end = clock()                          \

#define GET_ELAPSED_TIME() (((end - start) / (CLOCKS_PER_SEC * 1.0)) * 1000)

float time_aes_encrypt(const unsigned char* base_plaintext) {
    unsigned char tag[TAG_SIZE] = {'\0'};
    size_t ciphertext_len = 0;

    unsigned char *ciphertext = calloc(strlen((char*)   base_plaintext), sizeof(unsigned char));
    if (NULL == ciphertext) {
        printf("cannot allocate memory\n");
        return 0;
    }

    TIME(encrypt(
        base_plaintext,
        strlen((char*)   base_plaintext),
        (unsigned char*) "HELLO15002",
        strlen((char*)   "HELLO15002"),
        (unsigned char*) "_initvector_",
        strlen((char*)   "_initvector_"),
        (unsigned char*) "01234567890123456789012345678901",
        ciphertext, &ciphertext_len, tag
    ));

    return GET_ELAPSED_TIME();
}


float time_aes_decrypt(const unsigned char *base_plaintext) {
    unsigned char tag[TAG_SIZE] = {'\0'};
    size_t ciphertext_len = 0, plaintext_len = 0;

    unsigned char *plaintext = calloc(strlen((char *) base_plaintext), sizeof(unsigned char));
    if (NULL == plaintext) {
        printf("cannot allocate memory\n");
        return 0;
    }

    unsigned char *ciphertext = calloc(strlen((char *) base_plaintext), sizeof(unsigned char));
    if (NULL == ciphertext) {
        free(plaintext);
        printf("cannot allocate memory\n");
        return 0;
    }

    encrypt(
        base_plaintext,
        strlen((char*)   base_plaintext),
        (unsigned char*) "HELLO15002",
        strlen((char*)   "HELLO15002"),
        (unsigned char*) "_initvector_",
        strlen((char*)   "_initvector_"),
        (unsigned char*) "01234567890123456789012345678901",
        ciphertext, &ciphertext_len, tag
    );

    TIME(decrypt(
        ciphertext, ciphertext_len, 
        (unsigned char*) "HELLO15002",
        strlen((char*)   "HELLO15002"), tag, 
        (unsigned char*) "01234567890123456789012345678901",
        (unsigned char*) "_initvector_",
        strlen((char*)   "_initvector_"),
        plaintext, &plaintext_len
    ));

    free(plaintext);
    free(ciphertext);
    return GET_ELAPSED_TIME();
}

unsigned char* generate_test_secret(void) {
    EVP_PKEY* parameters = NULL;
    EVP_PKEY* local_key = NULL;
    EVP_PKEY* peer_key = NULL;
    unsigned char* secret = NULL;
    size_t secret_len = 0;
    
    if (CRYPTO_RETURN_OK != generate_dh_parameters(&parameters))
        return NULL;

    if (CRYPTO_RETURN_OK != generate_keys(parameters, &local_key))
        return NULL;

    if (CRYPTO_RETURN_OK != generate_keys(parameters, &peer_key))
        return NULL;

    if(CRYPTO_RETURN_OK != generate_shared_secret(local_key, peer_key, &secret, &secret_len))
        return NULL;

    return secret;
}

float time_data_packet_crafting(const unsigned char* datas) {
    packet_t message;
    unsigned char *secret = generate_test_secret();

    if (NULL == secret) {
        printf("Erreur lors de la génération du secret de test\n");
        return  0;
    }

    TIME(craft_data(&message, secret, datas));

    if (secret) OPENSSL_free(secret);
    return GET_ELAPSED_TIME();
}

float time_parse_data(const unsigned char *datas) {
    packet_t message;
    packet_t parsed_message;
    unsigned char *secret = generate_test_secret();
    unsigned char *serialized_message = NULL;

    if (NULL == secret) {
        printf("Erreur lors de la génération du secret de test\n");
        return  0;
    }

    if (PROTOCOL_OK != craft_data(&message, secret, datas)) {
        printf("Erreur lors de la création du paquet DATA\n");
        return  0;
    }

    serialized_message = calloc(message_full_size(&message), sizeof(unsigned char));
    if (NULL == serialized_message) {
        printf("Impossible d'allouer l'espace nécessaire\n");
        return 0;
    }

    if (PROTOCOL_OK != serialize(&message, serialized_message)) {
        printf("Erreur lors de la sérialisation du paquet");
        return 0;
    }

    TIME(
        parse_header(&parsed_message.header, serialized_message);
        parse_body_data(&parsed_message, secret, serialized_message)
    );

    return GET_ELAPSED_TIME();
}

// Partie liés aux tests fonctionnels

transition_code_t workload_surge_client_connected_state(state_attr_t *sm_attr)
{
    static bool need_init = true;
    static uint16_t sleep_time = 0;


    if (need_init) {
        // On triche en récupérant le temps de sommeil entre chaque
        // paquet depuis la premiere string passé à la SM.
        // NOTE: ce temps est en millisecondes
        sleep_time = get_short((unsigned char *) sm_attr->stdin_datas, 0);
        strncpy(sm_attr->stdin_datas, sm_attr->stdin_datas + sizeof(uint16_t), STDIN_DATAS_BUFFER_LEGTH - sizeof(uint16_t));
        printf("l'intervalle de temps entre chaque paquet est de %d\n", sleep_time);
        need_init = false;
    }

    usleep(sleep_time);

    // if (send_counter == NB_TEST) return STOP;

    return SEND;
}

transition_code_t workload_surge_server_connected_state(state_attr_t *sm_attr) {
    PRINT_DEBUG_ENTER_FCT();
    int activity;
    fd_set read_fds;


    FD_ZERO(&read_fds);
    FD_SET(STDIN_FILENO, &read_fds);
    FD_SET(sm_attr->serial_fd, &read_fds);

    PRINT_DEBUG("En attente d'acitivité au niveau du select");
    activity = select(sm_attr->serial_fd + 1, &read_fds, NULL, NULL, NULL);
    if (-1 == activity) {
        PRINT_DEBUG("Erreur lors du select");
        perror("select");
        return REPEAT;
    }

    if (FD_ISSET(sm_attr->serial_fd, &read_fds)) {
        PRINT_DEBUG("On a reçu quelque chose, on le traite");
        PRINT_DEBUG_EXIT_FCT();
        return RECEIVE;
    }

    PRINT_DEBUG_EXIT_FCT();
    return REPEAT;
}

void run_state_machine(state_machine_type_t sm_type, state_attr_t *sm_attributes) {
    state_t current_state = ENTRY_STATE;
    // Peut etre changer de terminologie, return_code_t serait surement mieux
    transition_code_t return_code;
    state_function_t state_function;

    while (true) {
        // printf("Etat courant : %s\n", state_names[current_state]);
        if (SERVER_SM == sm_type)
            state_function = server_states[current_state];
        else
            state_function = client_states[current_state];
        return_code = state_function(sm_attributes);
        current_state = lookup_transitions(sm_type, current_state, return_code);

        if (current_state == DISCONNECTED)
            break;
    }
}

void workload_surge_client(const char* device_name, const unsigned char *message, uint16_t sleep_time) {
    state_attr_t sm_attributes;
    init_state_attributes(&sm_attributes);

    strncpy(sm_attributes.device_name, device_name, DEVICE_NAME_LENGTH);
    memcpy(sm_attributes.stdin_datas, &sleep_time, sizeof(uint16_t));
    strncpy(sm_attributes.stdin_datas + sizeof(uint16_t), (char *) message, STDIN_DATAS_BUFFER_LEGTH - sizeof(uint16_t));
    client_states[CONNECTED] = workload_surge_client_connected_state;

    run_state_machine(CLIENT_SM, &sm_attributes);
}

void workload_surge_server(const char* device_name) {
    state_attr_t sm_attributes;
    init_state_attributes(&sm_attributes);

    strncpy(sm_attributes.device_name, device_name, DEVICE_NAME_LENGTH);
    server_states[CONNECTED] = workload_surge_server_connected_state;

    run_state_machine(SERVER_SM, &sm_attributes);
}