#include <openssl/bio.h>
#include <unistd.h>
#include <termios.h>
#include <errno.h>
#include <string.h>

#include "utils.h"

void print_hex(const unsigned char *s, size_t size)
{
  for (size_t i = 0; i < size; i++)
    printf("%02x", (int) s[i]);
  printf("\n");
}

void print_bio(BIO* bio_buffer) {
  char buffer[1024] = {'\0'};

  while (BIO_read (bio_buffer, buffer, 1024) > 0)
    printf("%s", buffer);
  
  printf("\n");
}

uint16_t get_short(unsigned char* array, int offset)
{
  return (uint16_t)(((uint16_t)array[offset + 1]) << 8) | array[offset];
}

int read_from_stdin(char *read_buffer, size_t max_len)
{
  // IMPORTANT: Il faut que stdin soit non bloquant pour que cette fonction marche
  PRINT_DEBUG_ENTER_FCT();
  memset(read_buffer, 0, max_len);
  
  ssize_t read_count = 0;
  ssize_t total_read = 0;
  
  do {
    PRINT_DEBUG("En attente sur STDIN");
    read_count = read(STDIN_FILENO, read_buffer, max_len);
    if (read_count < 0 && errno != EAGAIN && errno != EWOULDBLOCK) {
      perror("read()");
      PRINT_DEBUG_EXIT_FCT();
      return -1;
    }
    else if (read_count < 0 && (errno == EAGAIN || errno == EWOULDBLOCK)) {
      break;
    }
    else if (read_count > 0) {
      total_read += read_count;
      PRINT_DEBUG("On a bien lu STDIN");
      if ((size_t) total_read > max_len) {
        PRINT_DEBUG("Le message est trop long");
        printf("Veuillez taper un message plus court !\n");
        fflush(STDIN_FILENO);
        break;
      }
    }
  } while (read_count > 0);
  
  size_t len = strlen(read_buffer);
  if (len > 0 && read_buffer[len - 1] == '\n')
    read_buffer[len - 1] = '\0';
  
  // printf("Read from stdin %zu bytes\n", strlen(read_buffer));

  PRINT_DEBUG_EXIT_FCT();
  return 0;
}