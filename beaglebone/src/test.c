#include <stdio.h>
#include <string.h>
#include <openssl/crypto.h>
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>

#include "crypto_primitives.h"
#include "protocol.h"
#include "state_machine.h"
#include "utils.h"


void test_aes_gcm(void)
{
    /*
     * Set up the key and iv. Do I need to say to not hard code these in a
     * real application? :-)
     */

    /* A 256 bit key */
    unsigned char *key = (unsigned char *) "01234567890123456789012345678901";

    /* A 128 bit IV */
    unsigned char *iv = (unsigned char *) "0123456789012345";

    /* Message to be encrypted */
    unsigned char *plaintext =
        (unsigned char *)"The quick brown fox jumps over the lazy dog";
    unsigned char *aad =
        (unsigned char *)"The quick brown fox jumps over the lazy dog";

    /*
     * Buffer for ciphertext. Ensure the buffer is long enough for the
     * ciphertext which may be longer than the plaintext, depending on the
     * algorithm and mode.
     */
    unsigned char ciphertext[128 * 4];
    unsigned char tag[TAG_SIZE];
    /* Buffer for the decrypted text */
    unsigned char decryptedtext[128 * 4];
    size_t decryptedtext_len, ciphertext_len;

    /* Encrypt the plaintext */
    if (CRYPTO_RETURN_ERROR == encrypt(plaintext, strlen((char*) plaintext),
                                       aad, strlen((char*) aad),
                                       iv, strlen((char*) iv),
                                       key,
                                       ciphertext, &ciphertext_len, tag))
    {
        printf("erreur lors du déchiffrement\n");
    }
    
    printf("encrypted : %zu bytes\n", ciphertext_len);

    /* Do something useful with the ciphertext here */
    printf("Ciphertext is:\n");
    BIO_dump_fp (stdout, (const char *)ciphertext, ciphertext_len);
    
    /* Decrypt the ciphertext */
    if (CRYPTO_RETURN_ERROR == decrypt(ciphertext, ciphertext_len,
                                aad, strlen((char *) aad),
                                tag,
                                key, iv, strlen((char *) iv),
                                decryptedtext, &decryptedtext_len))
        printf("erreur lors du déchiffrement");
    printf("decrypted %zu bytes\n", decryptedtext_len);
    /* Add a NULL terminator. We are expecting printable text */
    decryptedtext[decryptedtext_len] = '\0';

    /* Show the decrypted text */
    printf("Decrypted text is:\n");
    printf("%s\n", decryptedtext);
}

void test_state_machine(void)
{
    state_t current_state = ENTRY_STATE;
    // Peut etre changer de terminologie, return_code_t serait surement mieux
    transition_code_t return_code;
    state_function_t state_function;
    state_attr_t sm_attributes;
    
    init_state_attributes(&sm_attributes);

    while (1) {
        printf("current state number : %d\n", current_state);
        state_function = client_states[current_state];
        return_code = state_function(&sm_attributes);
        current_state = lookup_transitions(CLIENT_SM, current_state, return_code);

        if (current_state == DISCONNECTED)
            break;
    }
    
}

void test_sha256(void) {
    unsigned char hashed_word[33];
    unsigned int hash_len = 0;

    hash((unsigned char*) "bonjour", strlen("bonjour"), hashed_word, &hash_len);
    printf("-----\nreceived : ");
    print_hex(hashed_word, strlen((char *) hashed_word));
    printf("should be: 2cb4b1431b84ec15d35ed83bb927e27e8967d75f4bcd9cc4b25c8d879ae23e18\n");
    printf("-----\n");
}

void test_signature(void) {
    size_t is_valid;
    const unsigned char *message = (unsigned char *) "toto";
    const unsigned char *private_key = (unsigned char *) "-----BEGIN RSA PRIVATE KEY-----\n"\
    "MIIEowIBAAKCAQEAy8Dbv8prpJ/0kKhlGeJYozo2t60EG8L0561g13R29LvMR5hy\n"\
    "vGZlGJpmn65+A4xHXInJYiPuKzrKUnApeLZ+vw1HocOAZtWK0z3r26uA8kQYOKX9\n"\
    "Qt/DbCdvsF9wF8gRK0ptx9M6R13NvBxvVQApfc9jB9nTzphOgM4JiEYvlV8FLhg9\n"\
    "yZovMYd6Wwf3aoXK891VQxTr/kQYoq1Yp+68i6T4nNq7NWC+UNVjQHxNQMQMzU6l\n"\
    "WCX8zyg3yH88OAQkUXIXKfQ+NkvYQ1cxaMoVPpY72+eVthKzpMeyHkBn7ciumk5q\n"\
    "gLTEJAfWZpe4f4eFZj/Rc8Y8Jj2IS5kVPjUywQIDAQABAoIBADhg1u1Mv1hAAlX8\n"\
    "omz1Gn2f4AAW2aos2cM5UDCNw1SYmj+9SRIkaxjRsE/C4o9sw1oxrg1/z6kajV0e\n"\
    "N/t008FdlVKHXAIYWF93JMoVvIpMmT8jft6AN/y3NMpivgt2inmmEJZYNioFJKZG\n"\
    "X+/vKYvsVISZm2fw8NfnKvAQK55yu+GRWBZGOeS9K+LbYvOwcrjKhHz66m4bedKd\n"\
    "gVAix6NE5iwmjNXktSQlJMCjbtdNXg/xo1/G4kG2p/MO1HLcKfe1N5FgBiXj3Qjl\n"\
    "vgvjJZkh1as2KTgaPOBqZaP03738VnYg23ISyvfT/teArVGtxrmFP7939EvJFKpF\n"\
    "1wTxuDkCgYEA7t0DR37zt+dEJy+5vm7zSmN97VenwQJFWMiulkHGa0yU3lLasxxu\n"\
    "m0oUtndIjenIvSx6t3Y+agK2F3EPbb0AZ5wZ1p1IXs4vktgeQwSSBdqcM8LZFDvZ\n"\
    "uPboQnJoRdIkd62XnP5ekIEIBAfOp8v2wFpSfE7nNH2u4CpAXNSF9HsCgYEA2l8D\n"\
    "JrDE5m9Kkn+J4l+AdGfeBL1igPF3DnuPoV67BpgiaAgI4h25UJzXiDKKoa706S0D\n"\
    "4XB74zOLX11MaGPMIdhlG+SgeQfNoC5lE4ZWXNyESJH1SVgRGT9nBC2vtL6bxCVV\n"\
    "WBkTeC5D6c/QXcai6yw6OYyNNdp0uznKURe1xvMCgYBVYYcEjWqMuAvyferFGV+5\n"\
    "nWqr5gM+yJMFM2bEqupD/HHSLoeiMm2O8KIKvwSeRYzNohKTdZ7FwgZYxr8fGMoG\n"\
    "PxQ1VK9DxCvZL4tRpVaU5Rmknud9hg9DQG6xIbgIDR+f79sb8QjYWmcFGc1SyWOA\n"\
    "SkjlykZ2yt4xnqi3BfiD9QKBgGqLgRYXmXp1QoVIBRaWUi55nzHg1XbkWZqPXvz1\n"\
    "I3uMLv1jLjJlHk3euKqTPmC05HoApKwSHeA0/gOBmg404xyAYJTDcCidTg6hlF96\n"\
    "ZBja3xApZuxqM62F6dV4FQqzFX0WWhWp5n301N33r0qR6FumMKJzmVJ1TA8tmzEF\n"\
    "yINRAoGBAJqioYs8rK6eXzA8ywYLjqTLu/yQSLBn/4ta36K8DyCoLNlNxSuox+A5\n"\
    "w6z2vEfRVQDq4Hm4vBzjdi3QfYLNkTiTqLcvgWZ+eX44ogXtdTDO7c+GeMKWz4XX\n"\
    "uJSUVL5+CVjKLjZEJ6Qc2WZLl94xSwL71E41H4YciVnSCQxVc4Jw\n"\
    "-----END RSA PRIVATE KEY-----\n\0";

    const unsigned char *public_key = (unsigned char *) "-----BEGIN PUBLIC KEY-----\n"\
    "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAy8Dbv8prpJ/0kKhlGeJY\n"\
    "ozo2t60EG8L0561g13R29LvMR5hyvGZlGJpmn65+A4xHXInJYiPuKzrKUnApeLZ+\n"\
    "vw1HocOAZtWK0z3r26uA8kQYOKX9Qt/DbCdvsF9wF8gRK0ptx9M6R13NvBxvVQAp\n"\
    "fc9jB9nTzphOgM4JiEYvlV8FLhg9yZovMYd6Wwf3aoXK891VQxTr/kQYoq1Yp+68\n"\
    "i6T4nNq7NWC+UNVjQHxNQMQMzU6lWCX8zyg3yH88OAQkUXIXKfQ+NkvYQ1cxaMoV\n"\
    "PpY72+eVthKzpMeyHkBn7ciumk5qgLTEJAfWZpe4f4eFZj/Rc8Y8Jj2IS5kVPjUy\n"\
    "wQIDAQAB\n"\
    "-----END PUBLIC KEY-----\n";
    size_t signed_length = 0;
    unsigned char* signature = NULL;

    sign(message, strlen((char *) message), &signature, &signed_length, private_key);
    is_valid = verify(message, strlen((char *) message), signature, signed_length, public_key);
    printf("la signature est %s\n", is_valid == CRYPTO_RETURN_ERROR ? "bonne" : "mauvaise");

    OPENSSL_free(signature);
}

void test_serial(void) {
    int fd;

    // Ouverture d'un port série
    fd = open("/dev/pts/4", O_RDWR | O_NOCTTY | O_NDELAY);
    if (fd == -1)
        perror("open_port: Unable to open /dev/ttyf1 - ");
    else {
        printf("port successfully opened\n");
        fcntl(fd, F_SETFL, 0);
    }

    // Ecriture
    int n = write(fd, "ATZ\r", 4);
    if (n < 0)
        fputs("write() of 4 bytes failed!\n", stderr);

    // Lecture (l'appel est bloquant)
    char buffer[255] = {'\0'};
    n = read(fd, buffer, 255);
    if (n < 0) {
        fputs("read() failed!\n", stderr);
    } else {
        printf("read %d bytes\n", n);
        buffer[n] = '\0';
        printf("%s", buffer);
    }

    // fermure du port série
    close(fd);

    return;
}

void test_messate_type_exist(void) {
    if (messate_type_exist(6))
        printf("error while trying messate_type_exist function\n");
    else
        printf("OK\n");

    if(!messate_type_exist(CLIENT_HELLO))
        printf("error while trying messate_type_exist function\n");
    else
        printf("OK\n");
}

void test_parse_header(void) {
    header_t source_header, final_header;
    unsigned char buffer[255] = {'\0'};

    source_header.length = 10;
    source_header.message_type = CLIENT_HELLO;
    source_header.sequence_number = 2;

    printf("sizeof(header_t) = %zu (should be 5)\n", sizeof(header_t));
    printf("sizeof(message_type_t) = %zu (should be 1)\n", sizeof(message_type_t));
    serialize_header(&source_header, buffer);
    print_hex(buffer, sizeof(header_t));
    parse_header(&final_header, buffer);

    printf("%d, %d, %d\n", final_header.length, final_header.message_type, final_header.sequence_number);
}

void test_diffie_hellman(void) {
    EVP_PKEY* parameters = NULL;
    EVP_PKEY* local_key = NULL;
    EVP_PKEY* peer_key = NULL;
    unsigned char* secret = NULL;
    size_t secret_len = 0;

    printf("Generating diffie hellman parameters\n");
    
    if (CRYPTO_RETURN_ERROR == generate_dh_parameters(&parameters))
        printf("error while generating df_parameters\n");

    printf("Generating the local key\n");
    if (CRYPTO_RETURN_ERROR == generate_keys(parameters, &local_key)) {
        printf("error while generating the local asymetric key\n");
        return;
    }

    printf("Generating the peer key\n");
    if (CRYPTO_RETURN_ERROR == generate_keys(parameters, &peer_key)) {
        printf("error while generating the peer asymetric key\n");
        return;
    }

    printf("Generating the shared secret\n");
    if(CRYPTO_RETURN_ERROR == generate_shared_secret(local_key, peer_key, &secret, &secret_len))
        printf("error while generating the shared secret\n");

    printf("THE SHARED SECRET WAS SUCCESSFULLY GENERATED -->\n");
    // QUESTION: Est-ce que secret_len est la taille en bit ou en octets ?
    print_hex(secret, secret_len);
    // XXX: Si c'est la taille en bit alors il faudrait faire l'appel suivant :
    // print_hex(secret, secret_len / 8);


    if (secret) OPENSSL_free(secret);
}

void test_session_id_generator(void) {
    unsigned char buffer[SESSION_ID_DIGITS] = {'\0'};

    generate_session_id(buffer);
    print_hex(buffer, SESSION_ID_DIGITS);
    generate_session_id(buffer);
    print_hex(buffer, SESSION_ID_DIGITS);
    generate_session_id(buffer);
    print_hex(buffer, SESSION_ID_DIGITS);
}

void test_packet_craft_client_hello(void) {
    packet_t message;
    packet_t reception;
    unsigned char serialized_message[1024] = {'\0'};
    unsigned char *secret = (unsigned char *) "01234567890123456789012345678901";

    const unsigned char *private_key = (unsigned char *) "-----BEGIN RSA PRIVATE KEY-----\n" \
    "MIIEowIBAAKCAQEA4hjuuZ3BXO6xBJ0qRwDqtycZV38xvAbtvhRbYpHXFQBVEr+H\n" \
    "EwirGyvBGKjeobvpwdTkdk45f5kn83ODpZ+3TZ8sqoRNaRtun3wH10vUoaShVy1e\n" \
    "jyEqyFmqyrTe7B/au+USuTuueGKbTbavKt9yelWy+ceUguTuJKQl4w23sprSYbHm\n" \
    "inVRFpABfXN/9cIhLfVszjMj+cJi8nQqBHEeonh4qVhtszMEPz/9i1RnMALSbQy0\n" \
    "Mo6Sxq5szMJA2ZUdFpGTbtjvYwJbTz4VtyCO+hfPA1EosZ5M4x715DcHpbMrjgIe\n" \
    "ciapERK8xkNdZe/B870g6ZXJ61r6IxyaYMX7ewIDAQABAoIBABhN/9OaPsADtKm/\n" \
    "T10knxajMFk0hh8lS4dEXvKApPNP/tUuG5ITO3mHQZGqDYyrS5yXixugmxO5Xg8Q\n" \
    "mtVCDw1cEAwgfsXOuhyQ07L0Jt5deS6K5QBT22qAIRj7Sn9tiTDrlhCtdrrgNruq\n" \
    "6LeeH4tBDSVGrBNKEsODlcMLpXSgdkCcuddS6NDdRTKGLqc8zKwrivlualVk5qD5\n" \
    "vWYQHhgflx/f5NhhenVMHrlJ4nWFRLvEpZko3hf7IeN28hnO5FNAptjgin+Wo59o\n" \
    "9zk3P8HFVgU79uIo1+oQGGovGJf3JQ3MJn5AKZELbqBxRIP9EW9+IMyb31NRGb9c\n" \
    "bDY5W5kCgYEA90Y462w2ZH92SJyk8+APgmaHNWcOWc6z6472QbtwE5Wj/4svifFq\n" \
    "RJYayr9MShlG4ET43GcFJ1a40xlX1gpK1tF2wiUn6S1UwqPsLGXq0Q3Hp9x6fgQy\n" \
    "dmym+kPRJ4tnM5xJPYA5Dpm4+rnhK8W1skgju+8VHhbWg5xU05MFgl8CgYEA6hNo\n" \
    "B/GPMHo+v4kS0c1CjsFRZTCDh+5mcH0rVY9odHUYO/HBFSqbZ6idlbrk++8ZeGMc\n" \
    "12cCalpLN5Ent33SLFHJmU7yRfpVXEYiUnjo9gcp0vj7Eqv6R4NeRlO04cWjnVsq\n" \
    "yYdl7LDyNQacFN+UKeLGn9r5K3piVuVTQbYJ9GUCgYAL29NI/xSQt0gt7U0/4OP9\n" \
    "hftABWQ7zFz2lBGdT2btmYSW8c7i17AX//bi+E+pUhMDGGuy7kHiBBezEAkVW8MB\n" \
    "EGSYpP2IAkOJXiEqc+zR/84ub7V0K7Fb2pQbdnsmYVoesIUWrPqPh7HGXHm/BqlT\n" \
    "FoCYN7wUiFXbU/kBeJCQEwKBgGjaapZsWZUAnY0U7O44QSFLbZejZLgGTbMEDt5T\n" \
    "0BmivklT4QcHP/fuKzEAOES2dTVdSHUg+o8DhYANsLncTcJ2nJTlLVe1bHlaHVPO\n" \
    "P6S9zrlnuKhDi2hva1KbeoJMEx+Q0BjYYwrktIqWPGKJSIh+wjSnVUhpmRWNwn7D\n" \
    "+a+VAoGBAKSGi+HNv4NsbBD9oJk1OvFS0KWxrjwlOFjvno56TN0gWH/IypZMuPsx\n" \
    "13+0HB1e7DO0zZgzC+oIg0xW/I1ylBemoTFld3QnVycLUo0+OGejaDcH7Ps9H/Sr\n" \
    "A+lhJDL6MLRX9F1T6X9eddmsvHdUzwGMGsZ37XTLWqYSJSnOkLqk\n" \
    "-----END RSA PRIVATE KEY-----\n\0";

    const unsigned char *peer_sign_pubkey = (unsigned char *) "-----BEGIN PUBLIC KEY-----\n"\
    "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4hjuuZ3BXO6xBJ0qRwDq\n"\
    "tycZV38xvAbtvhRbYpHXFQBVEr+HEwirGyvBGKjeobvpwdTkdk45f5kn83ODpZ+3\n"\
    "TZ8sqoRNaRtun3wH10vUoaShVy1ejyEqyFmqyrTe7B/au+USuTuueGKbTbavKt9y\n"\
    "elWy+ceUguTuJKQl4w23sprSYbHminVRFpABfXN/9cIhLfVszjMj+cJi8nQqBHEe\n"\
    "onh4qVhtszMEPz/9i1RnMALSbQy0Mo6Sxq5szMJA2ZUdFpGTbtjvYwJbTz4VtyCO\n"\
    "+hfPA1EosZ5M4x715DcHpbMrjgIeciapERK8xkNdZe/B870g6ZXJ61r6IxyaYMX7\n"\
    "ewIDAQAB\n"\
    "-----END PUBLIC KEY-----\n\0";

    if (PROTOCOL_ERROR == craft_client_hello(&message, private_key)) {
        printf("Erreur lors du craft du paquet CLIENT_HELLO\n");
        return;
    }

    if (PROTOCOL_ERROR == serialize(&message, serialized_message)) {
        printf("Erreur lors de la serialisation du paquet CLIENT_HELLO\n");
        return;
    }

    print_hex(serialized_message, message.header.length + sizeof(header_t));

    if (PROTOCOL_ERROR == parse_header(&reception.header, serialized_message)) {
        printf("Erreur lors du parsing du header\n");
    }
    if (PROTOCOL_ERROR == parse_body(&reception, secret, peer_sign_pubkey, serialized_message + sizeof(header_t))) {
        printf("Erreur lors du parsing du body\n");
    }
}

void test_packet_craft_public_key(void) {
    EVP_PKEY* parameters = NULL;
    EVP_PKEY* local_key = NULL;
    packet_t message;


    const unsigned char *private_key = (unsigned char *) "-----BEGIN RSA PRIVATE KEY-----\n" \
    "MIIEowIBAAKCAQEA4hjuuZ3BXO6xBJ0qRwDqtycZV38xvAbtvhRbYpHXFQBVEr+H\n" \
    "EwirGyvBGKjeobvpwdTkdk45f5kn83ODpZ+3TZ8sqoRNaRtun3wH10vUoaShVy1e\n" \
    "jyEqyFmqyrTe7B/au+USuTuueGKbTbavKt9yelWy+ceUguTuJKQl4w23sprSYbHm\n" \
    "inVRFpABfXN/9cIhLfVszjMj+cJi8nQqBHEeonh4qVhtszMEPz/9i1RnMALSbQy0\n" \
    "Mo6Sxq5szMJA2ZUdFpGTbtjvYwJbTz4VtyCO+hfPA1EosZ5M4x715DcHpbMrjgIe\n" \
    "ciapERK8xkNdZe/B870g6ZXJ61r6IxyaYMX7ewIDAQABAoIBABhN/9OaPsADtKm/\n" \
    "T10knxajMFk0hh8lS4dEXvKApPNP/tUuG5ITO3mHQZGqDYyrS5yXixugmxO5Xg8Q\n" \
    "mtVCDw1cEAwgfsXOuhyQ07L0Jt5deS6K5QBT22qAIRj7Sn9tiTDrlhCtdrrgNruq\n" \
    "6LeeH4tBDSVGrBNKEsODlcMLpXSgdkCcuddS6NDdRTKGLqc8zKwrivlualVk5qD5\n" \
    "vWYQHhgflx/f5NhhenVMHrlJ4nWFRLvEpZko3hf7IeN28hnO5FNAptjgin+Wo59o\n" \
    "9zk3P8HFVgU79uIo1+oQGGovGJf3JQ3MJn5AKZELbqBxRIP9EW9+IMyb31NRGb9c\n" \
    "bDY5W5kCgYEA90Y462w2ZH92SJyk8+APgmaHNWcOWc6z6472QbtwE5Wj/4svifFq\n" \
    "RJYayr9MShlG4ET43GcFJ1a40xlX1gpK1tF2wiUn6S1UwqPsLGXq0Q3Hp9x6fgQy\n" \
    "dmym+kPRJ4tnM5xJPYA5Dpm4+rnhK8W1skgju+8VHhbWg5xU05MFgl8CgYEA6hNo\n" \
    "B/GPMHo+v4kS0c1CjsFRZTCDh+5mcH0rVY9odHUYO/HBFSqbZ6idlbrk++8ZeGMc\n" \
    "12cCalpLN5Ent33SLFHJmU7yRfpVXEYiUnjo9gcp0vj7Eqv6R4NeRlO04cWjnVsq\n" \
    "yYdl7LDyNQacFN+UKeLGn9r5K3piVuVTQbYJ9GUCgYAL29NI/xSQt0gt7U0/4OP9\n" \
    "hftABWQ7zFz2lBGdT2btmYSW8c7i17AX//bi+E+pUhMDGGuy7kHiBBezEAkVW8MB\n" \
    "EGSYpP2IAkOJXiEqc+zR/84ub7V0K7Fb2pQbdnsmYVoesIUWrPqPh7HGXHm/BqlT\n" \
    "FoCYN7wUiFXbU/kBeJCQEwKBgGjaapZsWZUAnY0U7O44QSFLbZejZLgGTbMEDt5T\n" \
    "0BmivklT4QcHP/fuKzEAOES2dTVdSHUg+o8DhYANsLncTcJ2nJTlLVe1bHlaHVPO\n" \
    "P6S9zrlnuKhDi2hva1KbeoJMEx+Q0BjYYwrktIqWPGKJSIh+wjSnVUhpmRWNwn7D\n" \
    "+a+VAoGBAKSGi+HNv4NsbBD9oJk1OvFS0KWxrjwlOFjvno56TN0gWH/IypZMuPsx\n" \
    "13+0HB1e7DO0zZgzC+oIg0xW/I1ylBemoTFld3QnVycLUo0+OGejaDcH7Ps9H/Sr\n" \
    "A+lhJDL6MLRX9F1T6X9eddmsvHdUzwGMGsZ37XTLWqYSJSnOkLqk\n" \
    "-----END RSA PRIVATE KEY-----\n\0";

    printf("Generating diffie hellman parameters\n");
    if (CRYPTO_RETURN_ERROR == generate_dh_parameters(&parameters))
        printf("error while generating df_parameters\n");

    printf("Generating the local key\n");
    if (CRYPTO_RETURN_ERROR == generate_keys(parameters, &local_key)) {
        printf("error while generating the local asymetric key\n");
        return;
    }

    craft_public_key_dh(&message, local_key, private_key);
}

void test_packet_craft_data(void) {
    EVP_PKEY* parameters = NULL;
    EVP_PKEY* local_key = NULL;
    EVP_PKEY* peer_key = NULL;
    unsigned char *secret = NULL;
    size_t secret_len = 0;
    packet_t message;

    printf("Generating diffie hellman parameters\n");
    if (CRYPTO_RETURN_ERROR == generate_dh_parameters(&parameters))
        printf("error while generating df_parameters\n");

    printf("Generating the local key\n");
    if (CRYPTO_RETURN_ERROR == generate_keys(parameters, &local_key)) {
        printf("error while generating the local asymetric key\n");
        return;
    }

    printf("Generating the peer key\n");
    if (CRYPTO_RETURN_ERROR == generate_keys(parameters, &peer_key)) {
        printf("error while generating the peer asymetric key\n");
        return;
    }

    printf("Generating the secret\n");
    if (CRYPTO_RETURN_ERROR == generate_shared_secret(local_key, peer_key, &secret, &secret_len)) {
        printf("error while generating the shared secret\n");
        return;
    }

    unsigned char *raw_datas = (unsigned char *) "totoleplusbo";
    if (PROTOCOL_ERROR == craft_data(&message, secret, raw_datas)) {
        printf("error while generating the DATA packet\n");
        return;
    }

    printf("length:        %d\n", message.header.length);
    printf("datas_length: %zu\n", message.body.datas.datas_length);
    printf("enc_datas:\n");
    print_hex(message.body.datas.enc_datas, message.body.datas.datas_length);
    printf("iv:\n");
    print_hex(message.body.datas.iv, IV_SIZE);
    printf("tag:\n");
    print_hex(message.body.datas.tag, TAG_SIZE);
}

void test_get_short(void) {
    uint16_t value1 = 10;
    uint16_t value2 = 640;
    unsigned char array[4] = {'\0'};

    memcpy (array, &value1, 2);
    memcpy (array + 2, &value2, 2);

    printf("value1 = %d\n", get_short(array, 0));
    printf("value2 = %d\n", get_short(array, 2));
}