#ifndef __PROTOCOL_H__
#define __PROTOCOL_H__

// On compile en C99
#include <stdint.h>
#include <stdbool.h>

#include "crypto_primitives.h"

/*
 * NOTE: On utilise des define pour pouvoir décider la taille du type
 * message_type_t. Elle doit etre précise pour aller dans le header_t
 * qui lui doit faire une taille 5.
 */ 
#define CLIENT_HELLO        0
#define PUBLIC_KEY_DH       1  // Alias PKEY_DH
#define DATA                2
#define END_COMMUNICATION   3

#define MESSAGE_TYPE_AMOUNT  4

// TODO: A changer en un enum
#define PROTOCOL_RETURN_TYPE     size_t
#define PROTOCOL_ERROR           0
#define PROTOCOL_OK              1
#define PROTOCOL_ATTACK_DETECTED 2


typedef uint8_t message_type_t;

extern const char *message_type_names[];

// NOTE: Seul le header doit avoir une taille prédéterminé, le body on s'en fou
#pragma pack(push, 1)
typedef struct header_t
{
  message_type_t message_type;
  uint16_t length;
  uint16_t sequence_number;
} header_t;
#pragma pack(pop)


// QUESTION: Mettre un pointeur pour alléger la structure ?
typedef struct client_hello_body_t {
  unsigned char session_id[SESSION_ID_DIGITS];
  unsigned char *signature;
  unsigned long signature_length; // NOTE: Un peu useless
} client_hello_body_t;

typedef struct public_key_dh_body_t {
  uint16_t public_key_len;
  unsigned char *public_key;
  unsigned char *signature;
  unsigned long signature_length; // NOTE: Un peu useless
} public_key_dh_body_t;

// QUESTION: Mettre un pointeur pour alléger la structure ?
typedef struct data_body_t {
  unsigned char iv[IV_SIZE];
  unsigned char tag[TAG_SIZE];
  unsigned char *enc_datas;
  unsigned char *datas;
  size_t        datas_length; // dans le cas de AES-GCM enc_len == data_len
} data_body_t;

typedef client_hello_body_t end_communication_body_t;

typedef union packet_body_t {
  client_hello_body_t      client_hello;
  public_key_dh_body_t     pkey_dh;
  data_body_t              datas;
  end_communication_body_t end_communication;
} packet_body_t;


typedef struct packet_t {
  header_t header;
  packet_body_t body;
} packet_t;


bool messate_type_exist(message_type_t message_type);
size_t message_full_size(const packet_t *message);

PROTOCOL_RETURN_TYPE receive_packet(int serial_fd, const unsigned char *secret, const unsigned char *peer_key, packet_t *message);
PROTOCOL_RETURN_TYPE receive_packet_header(int serial_fd, packet_t *message);
PROTOCOL_RETURN_TYPE receive_packet_body(int serial_fd, bool drop_packet, const unsigned char *secret, const unsigned char *peer_key, packet_t *message);

PROTOCOL_RETURN_TYPE send_packet(int serial_fd, const packet_t *message);

PROTOCOL_RETURN_TYPE serialize(const packet_t *packet, char unsigned *buffer);
PROTOCOL_RETURN_TYPE serialize_header(const header_t *header, char unsigned *buffer);
PROTOCOL_RETURN_TYPE serialize_body(const packet_t *message, unsigned char *buffer);

PROTOCOL_RETURN_TYPE parse_header(header_t *packet, const unsigned char* message);
PROTOCOL_RETURN_TYPE parse_body(packet_t *packet_t, const unsigned char *secret, const unsigned char *peer_key, const unsigned char* message);
PROTOCOL_RETURN_TYPE parse_body_client_hello(packet_t *packet, const unsigned char *peer_key, const unsigned char *body_buffer);
PROTOCOL_RETURN_TYPE parse_body_public_key_dh(packet_t *packet, const unsigned char *peer_key, const unsigned char *body_buffer);
PROTOCOL_RETURN_TYPE parse_body_data(packet_t *packet, const unsigned char *secret, const unsigned char *body_buffer);
PROTOCOL_RETURN_TYPE parse_body_end_communication(packet_t *packet, const unsigned char *peer_key, const unsigned char *body_buffer);

PROTOCOL_RETURN_TYPE craft_client_hello(packet_t *message, const unsigned char *private_key);
PROTOCOL_RETURN_TYPE craft_public_key_dh(packet_t *message, EVP_PKEY *diffie_key, const unsigned char *private_key);
PROTOCOL_RETURN_TYPE craft_data(packet_t *message, const unsigned char *secret, const unsigned char *datas);
PROTOCOL_RETURN_TYPE craft_end_communication(packet_t *message, unsigned char *session_id, const unsigned char *private_key);

#endif // __PROTOCOL_H__