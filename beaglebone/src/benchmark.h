#ifndef __BENCHMARK_H__
#define __BENCHMARK_H__

#include <time.h>

#define NB_TEST 1000000

#define MILLI_TO_MICRO_SECONDS(ms_time) ((ms_time * 1000.0) / NB_TEST)

float time_aes_encrypt(const unsigned char *base_plaintext);
float time_aes_decrypt(const unsigned char *base_plaintext);
float time_data_packet_crafting(const unsigned char* datas);
float time_parse_data(const unsigned char *datas);
void workload_surge_client(const char* device_name, const unsigned char *message, uint16_t sleep_time);
void workload_surge_server(const char* device_name);

#endif //__BENCHMARK_H__