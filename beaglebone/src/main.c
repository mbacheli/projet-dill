#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <libgen.h>

#include "protocol.h"
#include "state_machine.h"
#include "utils.h"

#define CLIENT_PROGRAM_NAME "client"
#define SERVER_PROGRAM_NAME "server"

void run_state_machine(state_machine_type_t sm_type, state_attr_t *sm_attributes) {
    state_t current_state = ENTRY_STATE;
    // Peut etre changer de terminologie, return_code_t serait surement mieux
    transition_code_t return_code;
    state_function_t state_function;

    while (true) {
        printf("Etat courant : %s\n", state_names[current_state]);
        if (SERVER_SM == sm_type)
            state_function = server_states[current_state];
        else
            state_function = client_states[current_state];
        return_code = state_function(sm_attributes);
        current_state = lookup_transitions(sm_type, current_state, return_code);

        if (current_state == DISCONNECTED)
            break;
    }
}

int main(int argc, char** argv) {
    char* program_name = basename(argv[0]);
    state_attr_t sm_attributes;

    init_state_attributes(&sm_attributes);

    if (argc == 1)
        SHOW_USAGE("<serial_dev> [sign_private_key] [remote_sign_public_key] [dh_parms]");

    if (argc > 1) {
        printf("Le nom du port série à été passé sur la ligne de commande %s\n", argv[1]);
        strncpy(sm_attributes.device_name, argv[1], DEVICE_NAME_LENGTH);
    }

    if (argc > 2) {
        printf("Le fichier de signature à été passé sur la ligne de commande: %s\n", argv[2]);
        strncpy(sm_attributes.private_key_filename, argv[2], PEM_FILENAME_LENGTH);
    }

    if (argc > 3) {
        printf("Le fichier de vérification des signatures à été passé sur la ligne de commande: %s\n", argv[3]);
        strncpy(sm_attributes.peer_sign_pubkey_filename, argv[3], PEM_FILENAME_LENGTH);
    }

    if (argc > 4) {
        printf("Le fichier des paramètres Diffie Hellman à été passé sur la ligne de commande: %s\n", argv[4]);
        strncpy(sm_attributes.peer_sign_pubkey_filename, argv[4], PEM_FILENAME_LENGTH);
    }

    if (!strncmp(program_name, CLIENT_PROGRAM_NAME, sizeof(CLIENT_PROGRAM_NAME))) {
        printf("Entering client mode\n");
        run_state_machine(CLIENT_SM, &sm_attributes);
    } else if (!strncmp(program_name, SERVER_PROGRAM_NAME, sizeof(SERVER_PROGRAM_NAME))) {
        printf("Entering server mode\n");
        run_state_machine(SERVER_SM, &sm_attributes);
    } else {
        printf("No mode used, please either call 'server' or 'client' program\n");
        printf("called program was: %s\n", program_name);
        return 1;
    }

    return 0;
}
