#ifndef __TEST_H__
#define __TEST_H__

void test_aes_gcm(void);
void test_state_machine(void);
void test_sha256(void);
void test_signature(void);
void test_serial(void);
void test_messate_type_exist(void);
void test_parse_header(void);
void test_diffie_hellman(void);
void test_session_id_generator(void);
void test_packet_craft_client_hello(void);
void test_packet_craft_public_key(void);
void test_packet_craft_data(void);
void test_get_short(void);

#endif // __TEST_H__