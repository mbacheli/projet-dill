#include <stdio.h>
#include <string.h>
#include <openssl/crypto.h>
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <libgen.h>

#include "crypto_primitives.h"
#include "protocol.h"
#include "state_machine.h"
#include "utils.h"
#include "test.h"
#include "attack.h"
#include "benchmark.h"

#define LAUNCH_TEST_FROM_BINARY_NAME(TEST_NAME)                        \
    if (!strncmp(basename(argv[0]), #TEST_NAME, sizeof(#TEST_NAME))) { \
        TEST_NAME();                                                   \
        return EXIT_SUCCESS;                                           \
    }

#define LAUNCH_SUB_PROGRAM(SUBPROG_NAME)                                     \
    if (!strncmp(basename(argv[0]), #SUBPROG_NAME, sizeof(#SUBPROG_NAME))) { \
        return SUBPROG_NAME(argc, argv);                                     \
    }

int benchmark(int argc, char **argv) {
    const unsigned char* messages[] = {
        (const unsigned char *) "fylvRrKJ",
        (const unsigned char *) "hhl@HEHqij9KKuKk",
        (const unsigned char *) "yggJ-4qQqJ22CAroSSO@4SvDkoN7m0H0",
        (const unsigned char *) "w6GYqP%GzQk~ak7io*AtluxPBUMUMgKg28R4pgkcFF2cruBFQ$#HHo9vNvVx~lon",
        (const unsigned char *) "YjzUuCpf$h15~0LCYniFki0J0W^uni!ecN*gkhNcIe#pt0g1#~sL35jUm58L^8KGO!k#X3FgrxnZOCa5!M&q5t80IfPxtaftQU6FpzNdyfJ1*6Ho!tiTWj5M$eqky9qg",
        (const unsigned char *) "u89OS2dD2G*si9C99xHpGNSVp-trduEmiZ4M_5lBcTQhdFrv@fNI_hevQ66O8sP&&3*#_HLK9x22edJ_Ms44Odq3qcdhJWKwZZ6qAg2de*&*V1Jm1vImVoEIFkQiboZTvcN7NDq7s3Aqo5Tm8gGzZ#vGdI~f~#A#edSJX9VrLWJL&vbcurwBTG~2fs4G1_t#*cZ481Bys*rgBaRkVkvhQRNgV6nq_6LI0e_@u2d0tLV*Wk^yav*s=9J4wSjE9Z!x",
        (const unsigned char *) "^ypgD@Ds7SG-06CZ0ak-MGvXE1ai0xa7&3GhT@MooEi_4nX@b_EbB2kwfLJqQ^qHbDje~RAEHZ^AfI1V$2fr$tvEWlUQavg$xwk-lSv1WE@Neghh4a0PA11gFS12DN9~wNnsS--MbWyg0G7vHQbJP89FGSa~C1t6=kOUzOH1Q5BEz1DoE5vBkhYqa&%OkGUwNZ6StSgq4jB-zmIVThEJwW7FNKw2yxuxZo4tE_-p~sy4uy4iuGmr2NBJMV&JiAVItve_BAT!k5dev~Bat!oNg#UGFIXVRIue#iHW4#^$0oxt~ZyIOE5xZTzrJ9FaSdF*l!cpFsmPZruS!4R0W-zZZS!epuxkdI#J1QvDoclB@xgu7pLh^JlozdmyiFqzYAYII1GicDKZy1Y3-2uHE~fERL8-KidU0!$bDOgIjTcQ9yppWMESOG31*iu#5ywcN*f9N~QnyQzLA$Z@4j9YVw-E_ic17r2gIntU$aeTSXGnnf9NYPhUxOPlC^dlBn#K6Y$N",
        (const unsigned char *) "ON7z1dMepd4#0M2dnaHI4tMZJyrS#eXU1aqJeYlvaOj-!E6mNsWbJt_fv*gZyo7vkcx1a_dOmSifHm49Klb!6dEse#gOvpZn8OjJirw8bI!Kg3PrnXMOM_5As#L5ZxSfZt!xncL6SnC!P0OOpqAB_mFf&LQ-CexiCKMeog*k*feG7~o$MpfruMRd$I2z3Iu-Wn**kYaRwppemwbIPp$&17_au$rKRkAGX3AayWpSMT6c_0#qXE3bhxz4-C3zPsgq8G3Zon3za17cIkUEGmIV_Hu$DHiA6M$5rdzP*7#P#DD$dFlJpiM^6eUxG5oWBToqsQCiTl70gUb513nJfk@JpOtj3ef2v~9#kc3dQIqJ5!tTcq@sxBjacVmRuu@fF0U1ju1T1*Hs-~XZQ0pcaI4saNmH~9tKyky_rL@ikS@l@Z4h0x7Q$_j*$8pEYwsymU&m9Ga4y~ll4!G_xiUbWJXQeVqmJ_x8c45^Qztdk~P#F#hIqIWR!2l^O4wJpzP8m05avXwh~9Emrn&zZul-V0!XPS-e2z9tcZ^zghLdYPdi!5amOnyu$@Yq-gV@yyiuJm3m-bs7tz7nBO#JWx#a@Mzh~7cA@B6&NhYhUB@$p!D!9jgk*&8HV^p~*$4!1BY7dLX^r1&w&T!W4&c5a21RJuS8W88VhFJOJRD5aL*NYa34Us*LCjCPPldtb5mTgYVbDFSkBmW_gi_jkTsN~dmFvVU_bEemsbtHOtDhXM$~jN79GTtKkxlET#!SgWVy9BVsVk-_de9Lxqm&bSue_svrwZ^q_9eQTmix6xKo^mJ12*RgRz3jG-P-*PaCFiJwUB3CBAcCp8N$K~66kRVtik$sjBJqKpA0&h4Dt3MPaCLxx1rMY3Fj^I3YqER2jPYOVS#Bi27WecXhUNQe$$S@5g$60WjE0EuhmLF-^yz!LgfpPoKUYiOtn7PFOQ#_ZUmuPX_uInKb4zevnZxesW2ir6AG~uzV#qI1qrHhuype9wgTP5KA&a-EvqSCkDYynp!jUEsnH5rW"
    };
    size_t message_amount = sizeof(messages) / sizeof(const unsigned char*);

    if (argc < 2)
        SHOW_USAGE("<benchmark_program>");

    if (!strcmp(argv[1], "aes")) {
        for (size_t i = 0; i < message_amount; i++) {
            // Pour l'instant la division par NB_TEST rend les chiffres trop imprécis
            printf(
                "temps moyen de chiffrement de %zu octets de données avec AES-GCM :   %f (µs)\n",
                strlen((char *) messages[i]), MILLI_TO_MICRO_SECONDS(time_aes_encrypt(messages[i]))
            );
        }
        printf("\n");
        for (size_t i = 0; i < message_amount; i++) {
            printf(
                "temps moyen de déchiffrement de %zu octets de données avec AES-GCM : %f (µs)\n",
                strlen((char *) messages[i]), MILLI_TO_MICRO_SECONDS(time_aes_decrypt(messages[i]))
            );
        }
    } else if (!strcmp(argv[1], "craft_data")) {
        for (size_t i = 0; i < message_amount; i++) {
            printf(
                "temps moyen de création d'un paquet DATA avec %zu octets données : %f (µs)\n", 
                strlen((char *) messages[i]), MILLI_TO_MICRO_SECONDS(time_data_packet_crafting(messages[i]))
            );
        }
        for (size_t i = 0; i < message_amount; i++) {
            printf(
                "temps moyen de parsing d'un paquet DATA avec %zu octets données : %f (µs)\n", 
                strlen((char *) messages[i]), MILLI_TO_MICRO_SECONDS(time_parse_data(messages[i]))
            );
        }
    } else if (!strcmp(argv[1], "workload_surge_client")) {
        if (argc < 3)
            SHOW_USAGE("<client_serial_dev>");

        workload_surge_client(argv[2], (const unsigned char*) "toto", 1);
    } else if (!strcmp(argv[1], "workload_surge_server")) {
        if (argc < 3)
            SHOW_USAGE("<client_serial_dev>");

        workload_surge_server(argv[2]);
    }

    return EXIT_SUCCESS;
}

int attack(int argc, char **argv) {
    if (argc < 2)
        SHOW_USAGE("<attack_type> [attack_options]...");

    if (!strcmp("replay_attack", argv[1])) {
        if (argc < 4)
            SHOW_USAGE("replay_attack <client_serial_dev> <server_serial_dev>");
        printf("lancement du proxy pour l'attaque par rejeu\n");
        printf("on se branchera sur le client via : %s\n", argv[2]);
        printf("on se branchera sur le serveur via : %s\n", argv[3]);

        simple_replay_attack(argv[2], argv[3]);
    } else if (!strcmp("spam_replay_attack", argv[1])) {
        if (argc < 4)
            SHOW_USAGE("spam_replay_attack <client_serial_dev> <server_serial_dev>");
        printf("lancement du proxy pour l'attaque par rejeu\n");
        printf("on se branchera sur le client via : %s\n", argv[2]);
        printf("on se branchera sur le serveur via : %s\n", argv[3]);

        spam_replay_attack(argv[2], argv[3]);
    } else if (!strcmp("smart_replay_attack", argv[1])) {
        if (argc < 4)
            SHOW_USAGE("smart_replay_attack <client_serial_dev> <server_serial_dev>");
        printf("lancement du proxy pour l'attaque par rejeu\n");
        printf("on se branchera sur le client via : %s\n", argv[2]);
        printf("on se branchera sur le serveur via : %s\n", argv[3]);

        smart_replay_attack(argv[2], argv[3]);
    } else if (!strcmp("unwanted_end_communication", argv[1])) {
        if (argc < 4)
            SHOW_USAGE("unwanted_end_communication <client_serial_dev> <server_serial_dev>");
        printf("lancement du proxy pour l'attaque par rejeu\n");
        printf("on se branchera sur le client via : %s\n", argv[2]);
        printf("on se branchera sur le serveur via : %s\n", argv[3]);

        unwanted_end_communication(argv[2], argv[3]);
    } else if (!strcmp("evil_server", argv[1])) {
        if (argc < 3)
            SHOW_USAGE("evil_server <server_serial_dev>");
        printf("lancement du proxy pour l'attaque par rejeu\n");
        printf("on se branchera sur le client via : %s\n", argv[2]);

        evil_server(argv[2]);
    }

    return EXIT_SUCCESS;
}

int main (int argc, char **argv)
{
    LAUNCH_TEST_FROM_BINARY_NAME(test_state_machine);
    LAUNCH_TEST_FROM_BINARY_NAME(test_signature);
    LAUNCH_TEST_FROM_BINARY_NAME(test_sha256);
    LAUNCH_TEST_FROM_BINARY_NAME(test_serial);
    LAUNCH_TEST_FROM_BINARY_NAME(test_aes_gcm);
    LAUNCH_TEST_FROM_BINARY_NAME(test_messate_type_exist);
    LAUNCH_TEST_FROM_BINARY_NAME(test_parse_header);
    LAUNCH_TEST_FROM_BINARY_NAME(test_session_id_generator);
    LAUNCH_TEST_FROM_BINARY_NAME(test_packet_craft_client_hello);
    LAUNCH_TEST_FROM_BINARY_NAME(test_packet_craft_public_key);
    LAUNCH_TEST_FROM_BINARY_NAME(test_get_short);
    LAUNCH_TEST_FROM_BINARY_NAME(test_packet_craft_data);
    LAUNCH_TEST_FROM_BINARY_NAME(test_diffie_hellman);

    LAUNCH_SUB_PROGRAM(benchmark);
    LAUNCH_SUB_PROGRAM(attack);

    printf("Aucun argument sur la ligne de commande\n");

    return EXIT_SUCCESS;
}
