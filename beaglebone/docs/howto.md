# Howto du projet

NOTE: Tous les chemins relatifs sont spécifiés depuis le dossier du projet c'est a dire le dossier *beaglebone*.

## Compilation du projet

Il existe 4 target Makefile pour le projet:

- `all`: [projet principale] compile le projet avec la fonction `main` du fichier *src/main.c*. Cela créer deux binaires : `server` et `client`.
- `debug`: compile le projet principale en activant la constante `DEBUG` qui active les affichage de debug.
- `test`: [projet tests] compile le projet avec la fonction `main` du fichier *src/test_platform.c*. Cela créer les binaires `test_*`, `attack` et `benchmark`.
- `test_debug`: comme la tache `debug`, mais pour le projet de tests.

Le Makefile est prévue pour être capable d'utiliser le GCC d'une toolchain de cross-compilation. Il suffit de modifier la variable `CC` en lui passant le chemin vers le gcc à utiliser.

Pour la cross-compilation, le fichier *config/beagle_config* est capable de construire la toolchain et le système de fichier associé au beaglebone nu (sans aucune cape ni cryptoshield). Cependant, cela fait longtemps qu'il n'a pas été retesté...

### Note sur les binaires générés

Il existe en fait deux vrai binaire dans le dossier *bin*. Les autres binaires sont simplement des liens symboliques vers ces deux fichiers. Les programmes principaux déterminent quel sous-programme lancer grâce au nom du lien symbolique (grâce à `argv[0]`).

### Liste des binaires

La liste des tests se trouvent dans le main du fichier *src/test_platform.c*. Les macros `LAUNCH_TEST_FROM_BINARY_NAME` et `LAUNCH_SUB_PROGRAM` permettent de lancer les fonctions passé en argument lorsque le binaire du meme nom est utilisé.

**IMPORTANT**: Pour que le binaire associé soit créer lors de la compilation il faut le renseigner dans la variable `TEST_CASES_EXEC_NAMES` du Makefile.

## Environnement de développement

NOTE: Comme chacun utilise l'environnement de développement qu'il maitrise, cette partie est présente à titre d'information.

### Beaglebone

- éditeur de code : VSCode avec l'extension `C/C++` de Microsoft.
- compilateur pour la machine hote: gcc
- compilateur pour la cross-compilation: toolchain gcc construite avec buildroot
- tester les connexions série sans beaglebone: comande `socat` -> `socat -d -d pty,raw,echo=0 pty,raw,echo=0`
- environnement TMUX disponible via le script *scripts/replay_attack.tmux.sh*

### EFM

Nous avons utilisé l'IDE de Silicon Labs: Simplicity Studio

## Mise en place pour les tests

Vous pouvez mettre en place rapidement un ensemble de terminaux pour tester grâce à la commande *scripts/replay_attack.tmux.sh* (`tmux` et `socat` doivent être installés).

Vous devrez générer un ensemble de paramètres cryptographiques nécessaires pour que les programmes fonctionnent :

- Clé privée pour signer les paquets d'authentification à envoyer
- Clé publique de l'équipement distant pour vérifier la signature des paquets d'authentification entrants
- Les paramètres Diffie Hellman dans le fichier *dhparams.txt* au format PEM d'OpenSSL

Cet ensemble d'informations peut être crée grâce aux script *scripts/generate_crypto_params.sh*.

## Fonctionnement nomiale

Deux terminaux sont nécessaires. Le premier lancera le serveur et le second le client. On considère que l'environnement utilisé est celui décrit via le script *scripts/replay_attack.tmux.sh*

Pour lancer le serveur tapez la commande suivante

```sh
./bin/server /dev/pts/7 server-private.pem client-public.pem dhparams.pem
```

Pour lancer le client tapez la commande suivante

```sh
./bin/server /dev/pts/8 client-private.pem server-public.pem dhparams.pem
```

## Fonctionnement des tests

Chaque binaire commençant par `test_` est fait pour tester une fonctionnalité du code (signature, state_machine, chiffrement, ...). Ces tests ne sont pas maintenus et certains sont devenus erronés, ils sont laissés à titre d'exemples et devraient être transformés en tests unitaires et fonctionnels.

Deux programmes maintenus servent aux tests : *bin/attack* et *bin/benchmark*. Le premier permet notamment la mise en place d'une sorte de proxy pour tester rapidement les propriétés de non rejeu du protocole. Le second permet de lancer les tests de performance. Chacun de ces programme est composés de commandes qui sont voués à évoluer en fonction des tests et attack implémentés.

Ces deux derniers binaires sont à utiliser de la manière suivante:

```sh
./bin/benchmark <command> [parametres]...
```

Lancer la commande comme montrer ci dessous vous permettra d'avoir plus d'informations sur les paramètres que la commandes utilise.

```sh
./bin/benchmark <command>
```

### Binaire benchmark

- `aes`: permet de calculer le temps moyen mis pour chiffrer différentes taille données avec l'implémentation du chiffrement AES-GCM.
- `craft_data`: permet de calculer le temps moyen mis pour créer différentes taille de paquet de données avec l'implémentation du protocole.
- `workload_surge_client`: Permet de lancer un client avec une state machine faite pour laner le test de performance sur un serveur.
- `workload_surge_server`: Permet de lancer un serveur avec une state machine faite pour visualiser les performance de ce dernier en tess de charge

### Binaire attack

- `replay_attack`: mets en place un proxy qui ne fait que transmettre les transactions d'authentifications, détecte les paquets de type `DATA` et permet de les rejouer.
- `smart_replay_attack`: tente de changer le sequence number pour forcer le rejeu
- `spam_replay_attack`: tente de rejouer à l'infini un meme paquet (sans changer le sequence number)
- `unwanted_end_communication`: permet de mettre en évidence le problème sur le paquet de fin de communication
- `evil_server`: plus utilisé, permet de mettre en place un serveur utilisant un jeu de clés de signature non reconnu par le client.
