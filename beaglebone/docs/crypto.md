# Éléments de compréhension pour la cryptographie

[Taille des différents éléments cryptographique](https://crypto.stackexchange.com/a/26787)

## Initialization Vector

Le vecteur d'initialisation est expliqué [ici pour le cas d'AES en mode CBC](https://stackoverflow.com/questions/39412760/what-is-an-openssl-iv-and-why-do-i-need-a-key-and-an-iv).

Il peut (et doit) être envoyé avec un message [et c'est sécure](https://stackoverflow.com/questions/3436864/sending-iv-along-with-cipher-text-safe).

### Besoin de l'envoyé

Globalement, si on veux économiser de la bande passante il faudra ne pas envoyer l'IV à chaque fois.

[Envoyer l'IV à chaque fois ou non](https://crypto.stackexchange.com/questions/15907/why-must-iv-be-sent-with-each-packet)

## Authentication TAG

[Thread Stack overflow](https://crypto.stackexchange.com/questions/51537/delayed-tag-checks-in-aes-gcm-for-streaming-data) sur le choix du flux de données en rapport avec la vérification du TAG de ce flux.

## Paire de clé RSA

pour créer une paire de clé RSA il vous faut la ligne de commande OpenSSL installé sur votre machine. Vous pourrez ensuite taper les commandes suivantes:

```sh
export RSA_KEY_SIZE=2048
export PRIVATE_KEY_FILE=private.pem
export PUBLIC_KEY_FILE=public.pem
/usr/bin/openssl genrsa -out $PRIVATE_KEY_FILE $RSA_KEY_SIZE
/usr/bin/openssl rsa -in $PRIVATE_KEY_FILE -outform PEM -pubout -out $PUBLIC_KEY_FILE
```
