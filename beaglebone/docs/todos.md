# Liste des TODOs

## Beaglebone

### Cryptographie

Concerne les fichiers *src/crypto_primitives.c* et *src/crypto_primitives.h*.

- passer de Diffie Hellman classique à ECDH pour l'échange de clés (permet de réduire la taille des clés et donc des paquets)
- passer de RSA à ECC pour les signatures de paquet d'authentification (permet de réduire la taille des clés et donc des paquets)

### Protocole

Concerne les fichiers *src/protocol.c* et *src/protocol.h*.

### Machine à états

Concerne les fichiers *src/state_machine.c* et *src/state_machine.h*.
