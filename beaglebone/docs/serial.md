# Communication série

## Tests

Pour tester les programmes, il est possible de lancer une communication série virtuel via un linux grâce à un outil appelé `socat`. Ce [post stackoverflow](https://stackoverflow.com/a/19733677) explique la procédure.

## Lire depuis un port série

Avec linux on peut utiliser les appels systèmes standard, moyennant une petite configuration. Par exemple, voici [la procédure pour lire en mode "non canonical"](http://tldp.org/HOWTO/Serial-Programming-HOWTO/x115.html). Ce mode permet de préciser combien de charactère on veux lire à chaque fois.

## Documentation ARF53-pro

[branchements](https://www.pei-france.com/uploads/tx_etim/Adeunis_18010.pdf)
