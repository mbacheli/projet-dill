# Tests

## Récupération de message Random

Les tests de chiffrement et déchiffrement utilisent des chaines d'octets générés de la manière suivante.

```sh
export MESSAGE_SIZE=128
head -c 5000 /dev/urandom | tr -dc 'a-zA-Z0-9~!@#$%^&*_-' | fold -w $MESSAGE_SIZE | head -n 1
```
