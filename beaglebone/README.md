# Code pour le beaglebone

## Environnement de développement

### Compilation en cross-compilation

#### Installation de la toolchain

TODO: Intégrer la documentation de la toolchain buildroot

#### Compilation

Le makefile est fait pour fonctionner par défaut avec un cross compilateur pour armhf. Vous pouvez donc simplement taper la commande suivante :

```shell
make
```

Pour compiler en mode debug:

```shell
make debug
```

NOTE: Vous pouvez modifier la variable CC pour changer le compilateur (utile pour utiliser un gcc local ou une toolchain spéciale)

### Compilation sur le beaglebone

### Exportation sur le beaglebone

TODO

## Utilisation des programmes

### Génération des clés de signatures

TODO: (voir crypto.md dans la doc)

### Lancement des attaques

#### Attaque de rejeu simple

Il vous faudra ouvrir 2x2 ports série avec socat. Un premier couple de port pour connecter le client au proxy de l'attaquant et un second pour connecter le serveur au proxy de l'attaquant.

Pour lancer l'attaque il faut lancer le proxy, puis le serveur, puis le client. Le client va effectuer une connexion vers le serveur en passant par le proxy qui ne fera que retransmettre tous les paquets vers le serveur.

Le proxy ne fait qu'attendre un paquet de type `DATA` afin de le stocker et le re-émettre vers le serveur (pour que ce dernier reçoive deux fois le meme paquet).
