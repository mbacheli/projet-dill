#!/bin/bash

# Documentation utilisée
# - https://github.com/buildroot/buildroot/tree/master/board/beaglebone
# - https://stackoverflow.com/questions/45449264/buildroot-toolchain-with-openssl
# - https://stackoverflow.com/questions/45449264/buildroot-toolchain-with-openssl
# - https://bootlin.com/doc/training/buildroot/buildroot-labs.pdf

BUILDROOT_VERSION=2019.02.8

## Utilitaires
show_error() {
  echo -e "\e[91mERREUR\e[0m: $1"
}

show_note() {
  echo -e "\e[96mNOTE\e[0m: $1" 
}

check_env_vars() {
  if [ -z "$REPOSITORY_PATH" ]; then
    show_error "Vous devez setup la variable REPOSITORY_PATH pour qu'elle contienne le chemin ABSOLU du repository git du projet."
    exit 1
  fi
}

## Buildroot construction
get_buildroot() {
  wget https://buildroot.org/downloads/buildroot-$BUILDROOT_VERSION.tar.gz
  tar xzf buildroot-$BUILDROOT_VERSION.tar.gz
}

get_configuration() {
  # configuration faite en suivant les instructions à l'adresse :
  # https://github.com/buildroot/buildroot/tree/master/board/beaglebone
  cp $1 buildroot-$BUILDROOT_VERSION/.config
}

construct_buildroot() {
  get_buildroot
  get_configuration $REPOSITORY_PATH/configs/beagle_config
  make
  # TODO: Mettre à jour les variables etc.. pour installer l'environnement
}

# IMPORTANT: A faire avant tout = récupérer le dossier de travail de l'utilisateur
previous_dir=$(pwd)
check_env_vars

if [ -z "$RECOMPIL_TOOLCHAIN" ]; then
  show_error "La non recompilation de la toolchain n'est pas encore implémentée"
  exit 1
elif
  temp_dir=$(mktemp)
  cd $temp_dir

  show_note "Construction de l'OS embarqué et de sa toolchain de compilation"
  construct_buildroot

  show_note "Retour au dossier de travail et suppression du dossier temporaire"
  cd $previous_dir
  rm -rf $temp_dir
fi

show_note "On retourne dans le dossier de départ"
cd $previous_dir