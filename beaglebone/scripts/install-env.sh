#!/bin/bash

# Espace temporaire
mkdir -p /tmp/cross-compiler
cd /tmp/cross-compiler

# Téléchargement des binaires de GCC
wget https://releases.linaro.org/components/toolchain/binaries/7.5-2019.12/arm-linux-gnueabihf/gcc-linaro-7.5.0-2019.12-x86_64_arm-linux-gnueabihf.tar.xz
tar xJf gcc-linaro-7.5.0-2019.12-x86_64_arm-linux-gnueabihf.tar.xz

# Installation dans l'environnement
mv gcc-linaro-7.5.0-2019.12-x86_64_arm-linux-gnueabihf /usr/bin/gcc-linaro

echo "\n\n\n"
echo "Pour supprimer le cross-compilateur tapez la commande suivante"
echo "    rm -rf /usr/bin/gcc-linaro"