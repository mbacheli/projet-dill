#!/bin/bash
SESSION=${USER}_replay_attack

tmux -2 new-session -d -s $SESSION

tmux new-window -t $SESSION:1 -n 'Basic test environment'

# Setup a window for simple replay attack demo 
tmux split-window -h

tmux select-pane -t 0
tmux split-window -v
tmux split-window -v

tmux select-pane -t 0
tmux resize-pane -D 5 

tmux select-pane -t 1 
tmux send-keys "socat -d -d pty,raw,echo=0 pty,raw,echo=0" C-m
tmux select-pane -t 2 
tmux send-keys "socat -d -d pty,raw,echo=0 pty,raw,echo=0" C-m

tmux select-pane -t 3
tmux split-window -v

# Attach to session
tmux -2 attach-session -t $SESSION
