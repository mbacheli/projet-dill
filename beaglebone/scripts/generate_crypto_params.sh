#!/bin/bash

OPENSSL=/usr/bin/openssl
RSA_KEY_SIZE=2048

generate_signature_keys() {
  MODE=$1

  PRIVATE_KEY_FILE=$MODE-private.pem
  PUBLIC_KEY_FILE=$MODE-public.pem

  $OPENSSL genrsa -out $PRIVATE_KEY_FILE $RSA_KEY_SIZE
  $OPENSSL rsa -in $PRIVATE_KEY_FILE -outform PEM -pubout -out $PUBLIC_KEY_FILE
}

generate_dh_parameters() {
  DH_PARAMS_FILE=dhparams.pem

  $OPENSSL dhparam -outform PEM -out $DH_PARAMS_FILE 2048
}

generate_dh_parameters

generate_signature_keys client
generate_signature_keys server